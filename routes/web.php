<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Clear Cache facade value:
Route::get('/clear-cache', function() {
     $exitCode = Artisan::call('cache:clear');
	 $exitCode = Artisan::call('route:clear');
	 $exitCode = Artisan::call('view:clear');
	 $exitCode = Artisan::call('config:cache');
    return '<h1>Clear all type of cache.</h1>';
});


//Route::get('/','UserController@dashboard')->middleware('auth');
Route::match(['get','post'],'/',  ['as' => 'user.agentDashboard', 'uses' => 'UserController@dashboard'])->middleware('auth');
Route::match(['get','post'],'/user/list/{sUserType}',  ['as' => 'user.list', 'uses' => 'UserController@callUserList']);
Route::match(['get','post'],'/login',  ['as' => 'login', 'uses' => 'UserController@callUserLogin']);
Route::match(['get','post'],'/logout',  ['as' => 'logout', 'uses' => 'UserController@callUserLogout']);
Route::post('user/username-exists', 'UserController@checkUserNameExists');
Route::match(['get','post'],'/user/create-licensee',  ['as' => 'user.create-licensee', 'uses' => 'UserController@callCreateUserLicensee']);
Route::match(['get','post'],'/user/create-agent/{user_id?}',  ['as' => 'user.create-agent', 'uses' => 'UserController@callCreateUserAgent']);
Route::match(['get','post'],'/user/create-customer/{user_id?}',  ['as' => 'user.create-customer', 'uses' => 'UserController@callCreateUserCustomer']);
Route::match(['get','post'],'/user/create-product/{user_id?}',  ['as' => 'user.create-product', 'uses' => 'UserController@callCreateUserProduct']);
Route::match(['get','post'],'/user/create-brand/{user_id?}',  ['as' => 'user.create-brand', 'uses' => 'UserController@callCreateUserBrand']);
Route::match(['get','post'],'/user/setting',  ['as' => 'user.setting', 'uses' => 'UserController@userSetting']);
Route::match(['get','post'],'/user/active/{userId?}',  ['as' => 'user.active', 'uses' => 'UserController@callUserActive']);

Route::post('/booking-count','UserController@getBookingCount'); 
Route::post('/total-sales','UserController@getTotalSales'); 
Route::post('/total-commission','UserController@getTotalCommission'); 

Route::match(['get','post'],'/common/label-list',  ['as' => 'common.label-list', 'uses' => 'CommonController@callLabelList']);
Route::match(['get','post'],'/common/create-label/{nLabelId?}',  ['as' => 'common.create-label', 'uses' => 'CommonController@callLabelCreate']);
Route::match(['get','post'],'/common/delete-label/{nLabelId}',  ['as' => 'common.delete-label', 'uses' => 'CommonController@callLabelDelete']);
Route::match(['get','post'],'/common/region-list',  ['as' => 'common.region-list', 'uses' => 'CommonController@callRegionLists']);
Route::match(['get','post'],'/common/region-switch/{nRegionId?}',  ['as' => 'common.region-switch', 'uses' => 'CommonController@callRegionSwitch']);
Route::match(['get','post'],'/common/country-list',  ['as' => 'common.country-list', 'uses' => 'CommonController@callCountryList']);
Route::match(['get','post'],'/common/country-list/update-region',  ['as' => 'common.update-region', 'uses' => 'CommonController@callCountryUpadetRegion']);

Route::match(['get','post'],'/common/country-switch/{nRegionId?}',  ['as' => 'common.country-switch', 'uses' => 'CommonController@callCountrySwitch']);
Route::match(['get','post'],'/common/city-list',  ['as' => 'common.city-list', 'uses' => 'CommonController@callCityList']);
Route::match(['get','post'],'/common/create-city/{nCityId?}',  ['as' => 'common.create-city', 'uses' => 'CommonController@callCityCreate']);
Route::match(['get','post'],'/common/delete-city/{nCityId}',  ['as' => 'common.delete-city', 'uses' => 'CommonController@callCityDelete']);
Route::match(['get','post'],'/common/city-images',  ['as' => 'common.city-images', 'uses' => 'CommonController@callCityImageUpload']);
Route::match(['get','post'],'/common/delete-city-images/{nCityId}',  ['as' => 'common.delete-city-images', 'uses' => 'CommonController@callCityImageDelete']);
Route::match(['get','post'],'/common/primary-city-images/{nCityId}',  ['as' => 'common.primary-city-images', 'uses' => 'CommonController@callSetCityImagePrimary']);
Route::get('common/aot-location/{sType?}',  ['as' => 'common.aot-location', 'uses' =>  'CommonController@getAotLocations']);
Route::match(['get','post'],'/common/coupon-list',  ['as' => 'common.coupon-list', 'uses' => 'CommonController@callCouponList']);
Route::match(['get','post'],'/common/create-coupon/{nCouponId?}',  ['as' => 'common.create-coupon', 'uses' => 'CommonController@callCouponCreate']);
Route::match(['get','post'],'/common/coupon-manage-action',  ['as' => 'common.manage-action', 'uses' => 'CommonController@callManageAction']);
Route::match(['get','post'],'/common/get-cities-by-country',['as' => 'common.get-cities-by-country' ,'uses'=> 'CommonController@getCityCountryWise']);
Route::match(['get','post'],'/common/get-domain-by-licensee',['as' => 'common.get-domain-by-licensee' ,'uses'=> 'CommonController@getDomainLicenseeWise']);
Route::post('/common/get-domain-by-licensee','CommonController@getDomainsByLicenseeId')->name('common.get-domains-by-licensee_id');

Route::match(['get','post'],'/common/get-hbzone-by-cityid',['as' => 'common.get-hbzone-by-cityid' ,'uses'=> 'CommonController@getHbZoneCityWise']);
Route::match(['get','post'],'/common/get-countries-by-region',['as' => 'common.get-countries-by-region' ,'uses'=> 'CommonController@getCountryRegionWise']);
Route::match(['get','post'],'/common/currency-list',  ['as' => 'common.currency-list', 'uses' => 'CommonController@callCurrencyList']);
Route::match(['get','post'],'/common/create-currency/{nIdCurrency?}',  ['as' => 'common.create-currency', 'uses' => 'CommonController@callCurrencyCreate']);
Route::match(['get','post'],'/common/currency-delete/{nIdCurrency?}',  ['as' => 'common.currency-delete', 'uses' => 'CommonController@callCurrencyDelete']);
Route::match(['get','post'],'/common/suburb-list',  ['as' => 'common.suburb-list', 'uses' => 'CommonController@callSuburbList']);
Route::match(['get','post'],'/common/create-suburb/{nIdSuburb?}',  ['as' => 'common.create-suburb', 'uses' => 'CommonController@callSuburbCreate']);
Route::match(['get','post'],'/common/delete-hotel-images/{himgId}', ['as' => 'common.hotel-images-delete' ,'uses'=>'AcomodationController@ImageUploadDelete']);
Route::match(['get','post'],'/common/download-aws-images', ['as' => 'common.download-aws-images' ,'uses'=>'CommonController@downloadAwsImage']);

/* Maan Routes */
Route::get('region-geo','CommonController@callUserRegion')->name('user-region-geodata');
Route::post('region-geo','CommonController@addUserRegion')->name('add-user-region-geodata');

Route::get('country-geo','CommonController@callUserCountry')->name('user-country-geodata');
Route::post('country-geo','CommonController@addUserCountry')->name('add-user-country-geodata');

Route::get('city-geo','CommonController@callUserCity')->name('user-city-geodata');
Route::post('city-geo','CommonController@addUserCity')->name('add-user-city-geodata');


//AJAX
Route::post('/set-domain-geolink', 'CommonController@setDomainGeoLink');
Route::get('geo-data', 'CommonController@callRegionList')->name('common.manage_user_geo_data');
Route::post('/check-domain-geodata', 'CommonController@getGioDataUserWise');
Route::post('/logout_page', 'UserController@logoutEroam')->name('logout_page');

//acomodation
Route::group(['prefix'=>'acomodation'],function(){

    Route::get( 'get-hotel-info', 'AcomodationController@getHotelInfo' );
    Route::get( 'get-season-info', 'AcomodationController@getSeasonBySupplierId' );
    Route::match(['get','post'],'/hotel-list',  ['as' => 'acomodation.hotel-list', 'uses' => 'AcomodationController@callHotelList']);
    Route::match(['get','post'],'/hotel-create/{nIdHotel?}',  ['as' => 'acomodation.hotel-create', 'uses' => 'AcomodationController@callHotelCreate']);
    Route::match(['get','post'],'/hotel-supplier-list',  ['as' => 'acomodation.hotel-supplier-list', 'uses' => 'AcomodationController@callHotelSupplierList']);
    Route::match(['get','post'],'/hotel-label-list',  ['as' => 'acomodation.hotel-label-list', 'uses' => 'AcomodationController@callHotelLabelList']);
    Route::match(['get','post'],'/hotel-season-list',  ['as' => 'acomodation.hotel-season-list', 'uses' => 'AcomodationController@callHotelSeasonList']);
    Route::match(['get','post'],'/hotel-price-list',  ['as' => 'acomodation.hotel-price-list', 'uses' => 'AcomodationController@callHotelPriceList']);
    Route::match(['get','post'],'/hotel-markup-list',  ['as' => 'acomodation.hotel-markup-list', 'uses' => 'AcomodationController@callHotelMarkupList']);
    Route::match(['get','post'],'/hotel-room-list',  ['as' => 'acomodation.hotel-room-list', 'uses' => 'AcomodationController@callHotelRoomTypeList']);
    Route::match(['get','post'],'/hotel-aotlocation-list',  ['as' => 'acomodation.hotel-aotlocation-list', 'uses' => 'AcomodationController@callAOTLocationList']);
    Route::match(['get','post'],'/hotel-aotsupplier-list',  ['as' => 'acomodation.hotel-aotsupplier-list', 'uses' => 'AcomodationController@callAOTSupplierList']);
    Route::match(['get','post'],'/aotsupplier-update/{nIdSupplier?}',  ['as' => 'acomodation.aotsupplier-update', 'uses' => 'AcomodationController@callAOTSupplierUpdate']);
    Route::match(['get','post'],'/hotel-label-create/{nIdLabel?}',  ['as' => 'acomodation.hotel-label-create', 'uses' => 'AcomodationController@callHotelLabelCreate']);
    Route::match(['get','post'],'/label-delete/{nIdLabel?}',  ['as' => 'acomodation.label-delete', 'uses' => 'AcomodationController@callLabelDelete']);
    Route::match(['get','post'],'/hotel-images', ['as' => 'acomodation.hotel-images' ,'uses'=>'AcomodationController@ImageUpload']);
    
    Route::match(['get','post'],'/hotel-season-create/{nId?}/{nFrom?}',  ['as' => 'acomodation.hotel-season-create', 'uses' => 'AcomodationController@callHotelSeasonCreate'])
    ;
    Route::match(['get','post'],'/hotel-season-delete/{nIdSeason}/',  ['as' => 'acomodation.hotel-season-delete', 'uses' => 'AcomodationController@callHotelSeasonDelete']);

    Route::match(['get','post'],'/hotel-price-create/{nId?}/{nFrom?}',  ['as' => 'acomodation.hotel-price-create', 'uses' => 'AcomodationController@callHotelPriceCreate']);
    
    Route::match(['get','post'],'/hotel-supplier-create/{nId?}',  ['as' => 'acomodation.hotel-supplier-create', 'uses' => 'AcomodationController@callHotelSupplierCreate']);
    Route::match(['get','post'],'/hotel-supplier-delete/{nId}',  ['as' => 'acomodation.hotel-supplier-delete', 'uses' => 'AcomodationController@callHotelSupplierdelete']);

    Route::match(['get','post'],'/hotel-markup-create/{nId?}',  ['as' => 'acomodation.hotel-markup-create', 'uses' => 'AcomodationController@callHotelMarkupCreate']);
    Route::match(['get','post'],'/hotel-price-delete/{nIdPrice?}',  ['as' => 'acomodation.hotel-price-delete', 'uses' => 'AcomodationController@callPriceDelete']);
    Route::match(['get','post'],'/hotel-roomtype-create/{nIdPrice?}',  ['as' => 'acomodation.hotel-roomtype-create', 'uses' => 'AcomodationController@callRoomTypeCreate']);
    Route::match(['get','post'],'/room-delete/{nIdPrice?}',  ['as' => 'acomodation.room-delete', 'uses' => 'AcomodationController@callRoomTypeDelete']);
    Route::match(['get','post'],'/all-hotel-list', ['as' => 'acomodation.all-hotel-list', 'uses' =>'AcomodationController@getAllHotel', ['before' => 'csrf']]);
    Route::match(['get','post'],'/hotel-mapping',  ['as' => 'acomodation.expedia-hotel-mapping', 'uses' => 'HotelMappingController@callExpediaHotelMapping']);
    Route::match(['get','post'],'/hotel/mapping/add',  ['as' => 'acomodation.hotel-mapping-add', 'uses' => 'HotelMappingController@addHotelMapping']);
    Route::match(['get','post'],'/hotel-publish/{HotelId?}',  ['as' => 'acomodation.hotel-publish', 'uses' => 'AcomodationController@callHotelPublish']);
    //Route::post('hotel/mapping/add', 'HotelMappingController@addHotelMapping');

});
//tour
Route::match(['get','post'],'/tour/tour-list',  ['as' => 'tour.tour-list', 'uses' => 'TourController@callTourList']);
Route::match(['get','post'],'/tour/tour-create/{nTourId?}',  ['as' => 'tour.tour-create', 'uses' => 'TourController@callTourCreate']);
Route::match(['get','post'],'/tour/tour-images', ['as' => 'tour.tour-images' ,'uses'=>'TourController@ImageUpload']);
Route::match(['get','post'],'/tour/provider-data', ['as' => 'tour.provider-data' ,'uses'=>'TourController@getProviderData']);
Route::match(['get','post'],'/tour/delete-tour-images/{nCityId}',  ['as' => 'tour.delete-tour-images', 'uses' => 'TourController@callTourImageDelete']);
Route::match(['get','post'],'tour/sort-tour-images', ['as' => 'tour.sort-tour-images', 'uses'=>'TourController@sortImages']);
Route::match(['get','post'],'tour/change-status', ['as' => 'tour.change-status', 'uses'=>'TourController@callChangeStatus']);
Route::post('tour/manage-view','TourController@callManageView');
Route::match(['get','post'],'tour/manage-dates/{nTourId?}',['as'=>'tour.manage-dates','uses'=>'TourController@callManageTourDates']);
Route::post('tour/remove-season','TourController@callRemoveSeason');
Route::match(['get','post'],'/tour/manage-flight-payment',['as'=>'tour.manage-flight-payment','uses'=>'TourController@callManageFlightPayment']);
Route::match(['get','post'],'/tour/manage-season',['as'=>'tour.manage-season','uses'=>'TourController@callManageSeason']);
Route::match(['get','post'],'/tour/manage-payment',['as'=>'tour.manage-payment','uses'=>'TourController@callManagePayment']);
Route::match(['get','post'],'tour/update/dates/{nTourId}',['as'=>'tour.update-dates','uses'=>'TourController@callUpdateSeasonDates']);
Route::match(['get','post'],'/tour/tour-type-logo-list',  ['as' => 'tour.tour-type-logo-list', 'uses' => 'TourController@callTourTypeLogoList']);
Route::match(['get','post'],'/tour/tour-logo-create/{nIdTourLogo?}',  ['as' => 'tour.tour-logo-create', 'uses' => 'TourController@callTourTypeLogoCreate']);
Route::match(['get','post'],'/tour/tour-logo-delete/{nIdTourLogo}',  ['as' => 'tour.tour-logo-delete', 'uses' => 'TourController@callTourTypeLogoDelete']);
Route::match(['get','post'],'/tour/get-cities-by-country-id',  ['as' => 'tour.get-cities-by-country-id', 'uses' => 'TourController@get_cities_by_country_id']);

//route module
Route::match(['get','post'],'/route/route-list',  ['as' => 'route.route-list', 'uses' => 'RoutePlanController@getRouteList']);
Route::match(['get','post'],'/route/route-create',  ['as' => 'route.route-create', 'uses' => 'RoutePlanController@callRouteCreate']);
Route::match(['get'],'route/route-view/{nRouteId}', ['as' => 'route.route-view', 'uses' => 'RoutePlanController@callRouteView']);
Route::match(['get','post'],'route/get-transport-by-city', ['as' => 'route.get-transport-by-city', 'uses' => 'RoutePlanController@getTransportByCity']);
Route::post('route/route-delete', ['as' => 'route.route-delete','before' => 'csrf', 'uses' => 'RoutePlanController@callDeleteRoute']);
Route::post('route/route-default', ['as' => 'route.route-default','before' => 'csrf', 'uses' => 'RoutePlanController@callSetDefaultRoute']);

//activity module
Route::match(['get','post'],'/activity/activity-list',  ['as' => 'activity.activity-list', 'uses' => 'ActivityController@callActivityList']);
Route::match(['get','post'],'/activity/activity-delete/{nIdActivity?}',  ['as' => 'activity.activity-delete', 'uses' => 'ActivityController@callActivityDelete']);
Route::match(['get','post'],'/activity/activity-publish/{nIdActivity?}',  ['as' => 'activity.activity-publish', 'uses' => 'ActivityController@callActivityPublish']);
Route::match(['get','post'],'/activity/activity-create/{nIdActivity?}',  ['as' => 'activity.activity-create', 'uses' => 'ActivityController@callActivityCreate']);
Route::match(['get','post'],'/activity/activity-images', ['as' => 'activity.activity-images' ,'uses'=>'ActivityController@ImageUpload']);
Route::match(['get','post'],'activity/sort-activity-images', ['as' => 'activity.sort-activity-images', 'uses'=>'ActivityController@sortImages']);

Route::match(['get','post'],'/activity/activity-season-list',  ['as' => 'activity.activity-season-list', 'uses' => 'ActivityController@callActivitySeasonList']);
Route::match(['get','post'],'/activity/activity-season-create/{nId?}/{nFrom?}',  ['as' => 'activity.activity-season-create', 'uses' => 'ActivityController@callActivitySeasonCreate']);
Route::match(['get','post'],'/activity/activity-season-delete/{nIdActivity?}',  ['as' => 'activity.activity-season-delete', 'uses' => 'ActivityController@callActivitySeasonDelete']);
Route::match(['get','post'],'/activity/activity-supplier-list',  ['as' => 'activity.activity-supplier-list', 'uses' => 'ActivityController@callActivitySupplierList']);
Route::match(['get','post'],'/activity/activity-operator-list',  ['as' => 'activity.activity-operator-list', 'uses' => 'ActivityController@callActivityOperatorList']);
Route::match(['get','post'],'/activity/activity-supplier-delete/{nIdActivitySupplier?}',  ['as' => 'activity.activity-supplier-delete', 'uses' => 'ActivityController@callActivitySupplierDelete']);
Route::match(['get','post'],'/activity/create-activity-supplier/{nIdActivitySupplier?}',  ['as' => 'activity.create-activity-supplier', 'uses' => 'ActivityController@callActivitySupplierCreate']);
Route::match(['get','post'],'/activity/activity-markup-list',  ['as' => 'activity.activity-markup-list', 'uses' => 'ActivityController@callActivityMarkupList']);
Route::match(['get','post'],'/activity/create-activity-markup/{nIdActivityMarkup?}',  ['as' => 'activity.create-activity-markup', 'uses' => 'ActivityController@callActivityMarkupCreate']);
Route::match(['get','post'],'/activity/all-activity-list', ['as' => 'activity.all-activity-list', 'uses' =>'ActivityController@getAllActivity', ['before' => 'csrf']]);
Route::match(['get','post'],'/activity/activity-operator-delete/{nIdOperator}', ['as' => 'activity.activity-operator-delete', 'uses' =>'ActivityController@getActivityOperatorDelete' ]);
Route::match(['get','post'],'/activity/create-activity-operator/{nIdActivitySupplier?}',  ['as' => 'activity.create-activity-operator', 'uses' => 'ActivityController@callActivityOperatorCreate']);
Route::match(['get','post'],'/common/all-city-list', ['as' => 'activity.all-city-list', 'uses' =>'CommonController@getAllCity', ['before' => 'csrf']]);
Route::match(['get','post'],'/common/all-country-list', ['as' => 'activity.all-country-list', 'uses' =>'CommonController@getAllCountry', ['before' => 'csrf']]);
Route::match(['get','post'],'/activity/delete-activity-images/{nActivityId}',  ['as' => 'tour.delete-tour-images', 'uses' => 'ActivityController@callActivityImageDelete']);

Route::group(['prefix'=>'transport'],function(){
    //transport  old routing
    Route::match(['get','post'],'/transport-list',  ['as' => 'transport.transport-list', 'uses' => 'TransportController@callTransportList']);
    Route::match(['get','post'],'/transport-delete/{nId}',  ['as' => 'transport.transport-delete', 'uses' => 'TransportController@callTransportDelete']);
    Route::match(['get','post'],'/transport-create/{nId?}',  ['as' => 'transport.transport-create', 'uses' => 'TransportController@callTransportCreate']);
    Route::match(['get','post'],'/transport-supplier-list',  ['as' => 'transport.transport-supplier-list', 'uses' => 'TransportController@callTransportSupplierList']);
    Route::match(['get','post'],'/transport-supplier-delete/{nId}',  ['as' => 'transport.transport-supplier-delete', 'uses' => 'TransportController@callTransportSupplierDelete']);
    Route::match(['get','post'],'/transport/transport-publish/{nId?}',  ['as' => 'transport.transport-publish', 'uses' => 'TransportController@callTransportPublish']);
    Route::match(['get','post'],'/transport-supplier-create/{nId?}',  ['as' => 'transport.transport-supplier-create', 'uses' => 'TransportController@callTransportSupplierCreate']);
    Route::match(['get','post'],'/transport-season-list',  ['as' => 'transport.transport-season-list', 'uses' => 'TransportController@callTransportSeasonList']);
    Route::match(['get','post'],'/transport-season-delete/{nId}',  ['as' => 'transport.transport-season-delete', 'uses' => 'TransportController@callTransportSeasonDelete']);
    Route::match(['get','post'],'/transport-season-create/{nId?}/{nIdFrom?}',  ['as' => 'transport.transport-season-create', 'uses' => 'TransportController@callTransportSeasonCreate']);
    Route::match(['get','post'],'/transport-operator-list',  ['as' => 'transport.transport-operator-list', 'uses' => 'TransportController@callTransportOperatorList']);
    Route::match(['get','post'],'/transport-operator-delete/{nId}',  ['as' => 'transport.transport-operator-delete', 'uses' => 'TransportController@callTransportOperatorDelete']);
    Route::match(['get','post'],'/transport-operator-create/{nId?}',  ['as' => 'transport.transport-operator-create', 'uses' => 'TransportController@callTransportOperatorCreate']);
    Route::match(['get','post'],'/transport-markup-list',  ['as' => 'transport.transport-markup-list', 'uses' => 'TransportController@callTransportMarkupList']);
    Route::match(['get','post'],'/transport-markup-create/{nId?}',  ['as' => 'transport.transport-markup-create', 'uses' => 'TransportController@callTransportMarkupCreate']);
    Route::match(['get','post'],'/transport-type-list',  ['as' => 'transport.transport-type-list', 'uses' => 'TransportController@callTransportTypeList']);
    Route::match(['get','post'],'/trasnport-type-switch/{nId?}', ['as'=>'transport.trasnport-type-switch','before' => 'csrf', 'uses' => 'TransportController@callTransportTypeSwitch']);
    Route::match(['get','post'],'/transport-type-create',  ['as' => 'transport.transport-type-create', 'uses' => 'TransportController@callTransportTypeCreate']);
    Route::match(['get','post'],'/all-transport-list',  ['as' => 'transport.all-transport-list', 'uses' => 'TransportController@callAllTransport']);
    


    Route::match(['get','post'],'airlines',  ['as' => 'transport.airlines', 'uses' => 'TransportController@callTransportAirlinesList']);
    Route::match(['get','post'],'/airlines-delete/{nId}',  ['as' => 'transport.airlines-delete', 'uses' => 'TransportController@delete_airline']);
    
    Route::match(['get','post'],'/airline-create/{nId?}',  ['as' => 'transport.airline-create', 'uses' => 'TransportController@callTransportAirlineCreate']);




    /*Route::post('airlines-add', ['as' => 'transport.airlines-add','before' => 'csrf', 'uses' => 'TransportController@add_airline']);
    Route::post('airlines/{id}/{action}', ['as' => 'transport.airlines','before' => 'csrf', 'uses' => 'TransportController@update_airline']);*/

});

//booking
Route::match(['get','post'],'/booking/itenary-list',  ['as' => 'booking.itenary-list', 'uses' => 'BookingController@callItenaryList']);
Route::match(['get','post'],'/booking/show-itenary/{nItenaryId}',  ['as' => 'booking.show-itenary', 'uses' => 'BookingController@showItenary']);
Route::get('booking/itenary-pdf/{id}',['as' => 'booking.itenary-pdf','uses' => 'BookingController@viewItenaryPdf', 'before' => 'csrf']);
Route::post('booking/update-status', ['before' => 'csrf', 'uses' => 'BookingController@callUpdateStatus']);
Route::post('booking/update-voucher', ['before' => 'csrf', 'uses' => 'BookingController@callUpdateVoucher']);
Route::match(['get','post'],'booking/set-booking-id',['as' => 'booking.set-booking-id','uses' => 'BookingController@setBookingId']);
Route::match(['get','post'],'/booking/booked-tour-list',  ['as' => 'booking.booked-tour-list', 'uses' => 'BookingController@callBookedTourList']);
Route::match(['get','post'],'/booking/booked-tour-detail/{nBookId}',  ['as' => 'booking.booked-tour-detail', 'uses' => 'BookingController@callTourBookingShow']);
Route::post('booking/tour-update-status', ['before' => 'csrf', 'uses' => 'BookingController@callTourUpdateStatus']);
Route::match(['get','post'],'/booking/viator-list',  ['as' => 'booking.viator-booking-list', 'uses' => 'BookingController@callViatorList']);
Route::match(['get','post'],'/booking/view-viator-detail/{booking_id}',  ['as' => 'booking.show-viator-booking', 'uses' => 'BookingController@callViatorList']);

// booking created on 13-09-18

Route::match(['get','post'],'/booking/pending',  ['as' => 'booking.pending', 'uses' => 'AgentBookingController@pendingBooking']);
Route::match(['get','post'],'/booking/active',  ['as' => 'booking.active', 'uses' => 'AgentBookingController@activeBooking']);
Route::match(['get','post'],'/booking/archived',  ['as' => 'booking.archived', 'uses' => 'AgentBookingController@archivedBooking']);

Route::match(['get','post'],'/booking/booking-listing/{status}',  ['as' => 'booking.booking-listing', 'uses' => 'AgentBookingController@statusWiseBooking']);
Route::match(['get','post'],'/booking/booking-listing-detail/{nBookId}',  ['as' => 'booking.booking-listing-detail', 'uses' => 'AgentBookingController@reviewBooking']);
Route::match(['get','post'],'/booking/booking-location-detail/{nBookId}',  ['as' => 'booking.booking-location-detail', 'uses' => 'AgentBookingController@locationDetails']);
Route::match(['get','post'],'/booking/booking-product-detail/{nBookId}',  ['as' => 'booking.booking-product-detail', 'uses' => 'AgentBookingController@productDetails']);
Route::match(['get','post'],'/booking/booking-supplier-detail/{nBookId}',  ['as' => 'booking.booking-supplier-detail', 'uses' => 'AgentBookingController@supplierDetails']);
Route::match(['get','post'],'/booking/booking-itenary-detail/{nBookId}',  ['as' => 'booking.booking-itenary-detail', 'uses' => 'AgentController@itenaryDetails']);
Route::match(['get','post'],'/booking/booking-itenary-detail-email/{nBookId}',  ['as' => 'booking.booking-itenary-detail-email', 'uses' => 'AgentController@revisedItenaryEmail']);
Route::match(['get','post'],'/booking/booking-voucher-detail/{nBookId}',  ['as' => 'booking.booking-voucher-detail', 'uses' => 'AgentBookingController@voucherDetails']);
Route::match(['get','post'],'/booking/payment/{nBookId}',  ['as' => 'booking.payment', 'uses' => 'AgentBookingController@bookingPayment']);

Route::any('update-own-arrangement',  'AgentController@getItenaryUpdateView');
Route::any('update-revised-itenary',  'AgentController@updateRevisedItenary');
Route::match(['get','post'],'/booking/update-voucher-pdf/{legDetailId}',  ['as' => 'booking.update-voucher-pdf', 'uses' => 'AgentBookingController@updateVoucherDetails']);

Route::match(['get','post'],'/booking/tour-booking-listing',  ['as' => 'booking.tour-booking-listing', 'uses' => 'AgentBookingController@tourBooking']);
Route::match(['get','post'],'/booking/tour-pax-detail/{request_id}',  ['as' => 'booking.tour-pax-detail', 'uses' => 'AgentBookingController@tourPaxDetail']);
Route::match(['get','post'],'/booking/tour-supplier-detail/{request_id}',  ['as' => 'booking.tour-supplier-detail', 'uses' => 'AgentBookingController@tourSupplierDetail']);
Route::match(['get','post'],'/booking/tour-product-detail/{request_id}',  ['as' => 'booking.tour-product-detail', 'uses' => 'AgentBookingController@tourProductDetail']);
//test route
Route::match(['get','post'],'/user/test',  ['as' => 'user.test', 'uses' => 'UserController@test1']);


//Special Offer - OCTAL

Route::match(['get','post'],'/special-offer/list',  ['as' => 'special-offer.list', 'uses' => 'SpecialOfferController@OfferList']);
Route::post('special-offer/products','SpecialOfferController@getInventoryProducts')->name('products');
Route::post('special-offer/change-status','SpecialOfferController@changeStatus')->name('change-status');
Route::post('special-offer/price','SpecialOfferController@getPrice')->name('price');
Route::resource('special-offer','SpecialOfferController');


//test image upload toute
Route::get('s3-image-upload','SpecialOfferController@imageUpload');
Route::post('s3-image-upload','SpecialOfferController@imageUploadPost');
//API - OCTAL

Route::get('apis','ApiManagerController@get');

Route::group(array('middleware' => 'App\Http\Middleware\VerifyCsrfToken'), function() {

        ##General Routing Start Here
        Route::post('api/ip-detail', 'ApiController@addUserIpDetail');
    }); 

/*
    Status : Start
    Routes for APIS call from @frontend

*/
Route::prefix('map')->group(function () {
    Route::match(['get','post'],'related/cities/{country_id}','MapApiController@getAllRelatedCities');
    Route::match(['get','post'],'get/all/countries','MapApiController@getAllCountries');
    Route::match(['get','post'],'get/all/countriesBookingPro','MapApiController@getAllCountries');

    Route::match(['get','post'],'city/{id}','MapApiController@getCityByCityId');
    Route::match(['get','post'],'nearby-cities','MapApiController@getNearbyCities');
    Route::match(['get','post'],'get-transport-by-city-ids','MapApiController@getTransportByCitieIDs');
    Route::match(['get','post'],'auto-routes','MapApiController@getFixedCities');
    Route::match(['get','post'],'city','MapApiController@getAllCities');
    Route::match(['get','post'],'city-defaults','MapApiController@findAllDefault');
    Route::match(['get','post'],'get-hotels','MapApiController@get_hotels_by_city_id_v2');
    Route::match(['get','post'],'get-hotel-details','MapApiController@get_hotel_details');
    Route::match(['get','post'],'get-hotel-info','MapApiController@get_hotel_info');
    Route::match(['get','post'],'hotel-price-check','MapApiController@hotel_price_check');
    Route::match(['get','post'],'hotel-booking','MapApiController@hotel_booking');
    Route::match(['get','post'],'get-all-activity','MapApiController@get_all_activity_by_city_id_v2');
    Route::match(['get','post'],'hotel-by-city-id','MapApiController@hotel_by_city_id');
    Route::match(['get','post'],'city/hotel','MapApiController@hotel_by_city_id');
    Route::any('city/{id}',  'MapApiController@getCityByCityId');
    Route::any('city',  'MapApiController@getAllCities');
    Route::get('hotel-categories','MapApiController@get_all_hotel_categories');
    
});

/* 
    Calls Api Calls from frontend
*/
Route::post('activity',  'MapApiController@getActivity');
Route::get('transport-type',  'MapApiController@get_transport_types');
Route::get('room-type',  'MapApiController@get_room_types');
Route::get('eroam/hotel/{id}',  'MapApiController@viewEroamHotel');
Route::get('eroam/activity/{id}',  'MapApiController@viewEroamActivity');
Route::get('aot/hotel/{SupplierCode}','MapApiController@viewAOTHotel');
Route::match(['get','post'],'viator/category','MapApiController@get_all_label');
Route::match(['get','post'],'suburb-by-city/{city_id}','MapApiController@get_all_suburb_by_city');
Route::get('get_currencies', 'MapApiController@get_currency_values');
Route::get('all-currencies', 'MapApiController@get_all');
Route::get('regions-countries-cities-suburbs', 'MapApiController@getRegions');
Route::get('traveler-options',  'MapApiController@get_traveler_profile_options');
Route::match(['get','post'],'city/hotel','MapApiController@hotel_by_city_id');
/* 
    Calls related Viator
*/
Route::match(['get','post'],'service/search/products', 'ViatorApiController@activities');
Route::match(['get','post'],'service/taxonomy/locations', 'ViatorApiController@location');
Route::match(['get','post'],'service/booking/hotels', 'ViatorApiController@hotels');
Route::match(['get','post'],'service/location', 'ViatorApiController@search_location'); 
Route::match(['get','post'],'search/product', 'ViatorApiController@view_activity');
Route::match(['get','post'],'booking/calculateprice', 'ViatorApiController@calculate_price');
Route::match(['get','post'],'save/viator/cagetory', 'ViatorApiController@save_category');
Route::match(['get','post'],'book-activities', 'ViatorApiController@bookActivities');   
Route::match(['get','post'],'get-tour-code', 'ViatorApiController@getTourCode');                
Route::match(['get','post'],'service/tours', 'ViatorApiController@create_tours');   
Route::match(['get','post'],'tour/dates', 'ViatorApiController@get_tour_dates');
Route::match(['get','post'],'labels','MapApiController@get_all_label');

/* 
    Calls related BusBud
*/

Route::match(['get','post'],'busbud/transportList', 'BusBudApiController@getAllTransport'); 
Route::match(['get','post'],'bookBusBud', 'BusBudApiController@bookingProcess');
Route::match(['get','post'],'createBusBudCart','BusBudApiController@createBusBudCart');

/* 
    Calls related Itenary Save
*/

Route::resource('saveOrderDetail','ItenaryOrderController@saveOrderDetail');
Route::get('getItineraryDetail/{orderId}','ItenaryOrderController@getItineraryDetail');

/* 
    Calls related Mystifly
*/

Route::get('mystifly/modal-data',     'MystiflyController@get_modal_data');
Route::post('mystifly/flight/search', 'MystiflyController@airLowFareSearchController');
Route::get('mystifly/flight/search1', 'MystiflyController@airLowFareSearchController1');
Route::get('mystifly/fare-rules',     'MystiflyController@fareRules1_1Controller');
Route::get('mystifly/revalidate',     'MystiflyController@airRevalidateController');
Route::post('mystifly/flight/book',   'MystiflyController@bookFlightController');
Route::post('mystifly/ticket/order',  'MystiflyController@ticketOrderController');
Route::get('mystifly/trip-details',   'MystiflyController@tripDetailsController');
Route::post('testMystifly',   'MystiflyController@test');


/* 
    Calls related Mystifly
*/
Route::prefix('user')->group(function () {
    Route::any('check-customer','UserController@check_customer');
    Route::any('create_customer','UserController@customer_registeration');
    Route::any('get-by-id/{id}', 'UserController@user_get_by_id');
    Route::post('update-profile/step1','UserController@update_user_profile_step1');
    Route::post('update-profile/step2','UserController@update_user_profile_step2');
    Route::post('update-profile/step3','UserController@update_user_profile_step3');
    Route::post('update-profile/step4','UserController@update_user_profile_step4');
    Route::any('update-preferences', 'UserController@update_user_preferences');
    Route::any('get-itinerary', 'UserController@get_user_itinerary');
    Route::any('request-password-reset', 'UserController@request_change_password');
    Route::any('check-token', 'UserController@check_token');
    Route::any('reset-password', 'UserController@reset_user_password_from_app');
    Route::any('confirm-registration', 'UserController@confirm_registration');
    Route::any('get-itineraries', 'UserController@get_user_itineraries');
    Route::any('get_trips/{user_id}','UserController@get_user_trips');
});

/*routes add by dhara start*/
Route::get('store/viatorTours', 'TourController@storeViatorTours');
/*routes add by dhara end*/


// Client Onboarding - OCTAL
Route::prefix('onboarding')->group(function () {
    
    Route::get('general/{licensee_id?}',  ['as' => 'onboarding.general', 'uses' => 'OnboardingController@general']);
    Route::post('general',['as'=> 'general.add','uses' => 'OnboardingController@generalAdd']);
    Route::put('general/{id}',['as'=> 'general.update','uses' => 'OnboardingController@generalUpdate']);
    
    Route::get('frontend/{licensee_id}',['as' => 'onboarding.frontend','uses' => 'OnboardingController@frontend']);
    Route::post('frontend',['as' => 'frontend.add','uses' => 'OnboardingController@frontendAdd']);
    Route::post('upload-logo',['as' => 'onboarding.upload-logo','uses' => 'OnboardingController@uploadLogo']);
    Route::post('upload-favicon',['as' => 'onboarding.upload-favicon','uses' => 'OnboardingController@uploadFavicon']);
    Route::put('frontend/{domain_id}',['as' => 'frontend.update','uses' => 'OnboardingController@frontendUpdate']);
    Route::post('addDomain',['as' => 'frontend.addDomain','uses' => 'OnboardingController@addDomain']);
    Route::post('deleteDomain',['as' => 'frontend.deleteDomain','uses' => 'OnboardingController@deleteDomain']);
    // Route::get('backend/{licensee_id}/{domain_id}',['as'=>'onboarding.backend','uses' => 'OnboardingController@backend']);
    Route::get('backend/{licensee_id}',['as'=>'onboarding.backend','uses' => 'OnboardingController@backend']);
    // Route::put('backend/{licensee_id}/{domain_id}',['as'=> 'backend.update','uses' => 'OnboardingController@backendUpdate']);
    Route::put('backend/{licensee_id}',['as'=> 'backend.update','uses' => 'OnboardingController@backendUpdate']);
    Route::post('backend',['as'=>'backend.add','uses' => 'OnboardingController@backendAdd']);

    Route::get('geodata/{licensee_id}',['as' => 'onboarding.geodata','uses' => 'OnboardingController@geoData']);
    Route::post('geodata',['as'=> 'geodata.add','uses' => 'OnboardingController@geoDataSave']);
    Route::post('geodata/{licensee_id}',['as'=> 'geodata.update','uses' => 'OnboardingController@geoDataUpdate']);
    Route::post('getGeodata',['as'=> 'geodata.getGeodata','uses' => 'OnboardingController@getGeodata']);
    

    Route::get('inventory/{licensee_id}',['as'=>'onboarding.inventory','uses' => 'OnboardingController@inventory']);
    Route::post('inventory',['as' => 'inventory.add','uses' => 'OnboardingController@inventoryAdd']);
    Route::put('inventory/{licensee_id}/{domain_id}',['as'=>'inventory.update','uses'=>'OnboardingController@inventoryUpdate']);
    

    Route::get('account/{licensee_id}',['as'=>'onboarding.account','uses' => 'OnboardingController@account']);
    Route::post('account',['as'=>'account.add','uses' => 'OnboardingController@accountSave']);
    Route::put('account/{licensee_id}',['as'=>'account.update','uses' => 'OnboardingController@accountUpdate']);

    Route::get('usermanagement/{licensee_id}',['as'=>'onboarding.usermanagement','uses' => 'OnboardingController@usermanagement']);
    Route::post('usermanagement',['as'=>'usermanagement.add','uses' => 'OnboardingController@userAdd']);
    Route::put('usermanagement/{licensee_id}',['as'=>'usermanagement.update','uses'=>'OnboardingController@userUpdate']);
    Route::get('usermanagement/update/{user_id}/{licensee_id}',['as'=>'usermanagement.userupdate','uses'=>'OnboardingController@userdetails']);

    Route::get('onboarding',['as'=>'onboarding','uses'=>'OnboardingController@index']);
    Route::get('view/{licensee_id}',['as'=>'onboarding.view','uses'=>'OnboardingController@view']);

    Route::get('pricing/{licensee_id}',['as'=>'onboarding.pricing','uses'=>'OnboardingController@pricing']);
    Route::post('pricing',['as'=>'pricing.add','uses'=>'OnboardingController@pricingAdd']);
    Route::put('pricing/{licensee_id}',['as'=>'pricing.update','uses'=>'OnboardingController@pricingUpdate']);

    Route::get('create-css',['as'=>'onboarding.createCss','uses'=>'OnboardingController@createCss']);
});


/* Image Script*/
Route::any('/image',  'ImageScriptController@index');
Route::any('/hotel-image',  'ImageScriptController@imageScriptHotel'); 
/* Image Script end*/

Route::prefix('yalago')->group(function () {
    Route::match(['get','post'],'store-countries','YalagoApiController@StoreCountries');
    Route::match(['get','post'],'store-province','YalagoApiController@StoreProvince');
    Route::match(['get','post'],'store-locations','YalagoApiController@StoreLocations');
    Route::match(['get','post'],'store-facilities','YalagoApiController@StoreFacilities');    
});

/* Agent Portal */

Route::match(['get','post'],'/users/user-management',  ['as' => 'user.index', 'uses' => 'AgentController@index']);
Route::match(['get'],'/users/passenger-details/{id}',  ['as' => 'user.paxDetails', 'uses' => 'AgentController@paxDetails']);

Route::match(['get','post'],'/booking/booking-location-detail-agent/{nBookId}',  ['as' => 'booking.booking-location-detail', 'uses' => 'AgentNewController@locationDetails']);


Route::get('agent/settings',['as'=>'agent.settings','uses'=>'AgentNewController@settings']);
Route::put('agent/settings',['as'=>'agent.settings','uses'=>'AgentNewController@addSettings']);

Route::get('agent/switch-domains',['as'=>'agent.switch-domains','uses'=>'AgentNewController@switchDomains']);
Route::post('agent/switch-domain',['as'=>'agent.switch-domain','uses'=>'AgentNewController@switchAgentDomain']);

Route::get('agent/create-customer',['as'=>'agent.create-customer','uses'=>'AgentBookingController@createCustomer']);
Route::post('agent/store-customer',['as'=>'agent.store-customer','uses'=>'AgentBookingController@storeCustomer']);
Route::get('agent/redirect-page/{user_id}/{trip_id?}',['as'=>'agent.redirect-page','uses'=>'AgentBookingController@redirectPage']);
Route::get('agent/list-customer',['as'=>'agent.list-customer','uses'=>'AgentBookingController@listCustomers']);
Route::get('agent/list',['as'=>'agent.list','uses'=>'AgentBookingController@list']);
Route::get('users/edit-customer/{id}',['as'=>'agent.edit-customer','uses'=>'AgentBookingController@editCustomer']);
Route::post('users/update-customer',['as'=>'agent.update-customer','uses'=>'AgentBookingController@updateCustomer']);

Route::get('users/saved-trips/{user_id}',['as'=>'user.saved-trips','uses'=>'AgentBookingController@savedTrips']);