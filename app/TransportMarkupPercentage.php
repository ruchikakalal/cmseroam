<?php
namespace App;
use Eloquent;

class TransportMarkupPercentage extends Eloquent {

	protected $table = 'ztransportmarkuppercentages';

    protected $guarded = array('id');
    
    protected $dates = ['deleted_at'];   

    public function markup(){
    	return $this->belongsTo('TransportMarkup','transport_markup_id','id');
    }    
}
 
?>