@extends('layout/mainlayout')
@section('content')
<div class="content-container">
    <h1 class="page-title">General Settings</h1>

    <div class="alert" role="alert" id="error_msg"></div>
    <form action="{{ route('agent.settings') }}" class="add-form" method="post">
    {{ csrf_field() }}
    @method('PUT')
    <div class="box-wrapper">
    	  <p>General Details</p>
    	  <div class="form-group">
    	  	<label class="label-control">Title</label>
    	  	<select class="form-control" id="title" name="title">
    	  		<option value="">Select Title</option>
    	  		<option value="Mr" @if(isset($user['title']) && $user['title'] == 'Mr'){{ 'selected' }}@endif>Mr</option>
    	  		<option value="Ms" @if(isset($user['title']) && $user['title'] == 'Ms'){{ 'selected' }}@endif>Ms</option>
    	  		<option value="Mrs" @if(isset($user['title']) && $user['title'] == 'Mrs'){{ 'selected' }}@endif>Mrs</option>
    	  		<option value="Miss" @if(isset($user['title']) && $user['title'] == 'Miss'){{ 'selected' }}@endif>Miss</option>
    	  		<option value="Dr" @if(isset($user['title']) && $user['title'] == 'Dr'){{ 'selected' }}@endif>Dr</option>
    	  		<option value="Prof" @if(isset($user['title']) && $user['title'] == 'Prof'){{ 'selected' }}@endif>Prof</option>
    	  		<option value="Rev" @if(isset($user['title']) && $user['title'] == 'Rev'){{ 'selected' }}@endif>Rev</option>
    	  		<option value="Sir" @if(isset($user['title']) && $user['title'] == 'Sir'){{ 'selected' }}@endif>Sir</option>
    	  		<option value="Sister" @if(isset($user['title']) && $user['title'] == 'Sister'){{ 'selected' }}@endif>Sister</option>
    	  		<option value="Father" @if(isset($user['title']) && $user['title'] == 'Father'){{ 'selected' }}@endif>Father</option>
    	  		<option value="Lady" @if(isset($user['title']) && $user['title'] == 'Lady'){{ 'selected' }}@endif>Lady</option>
    	  		<option value="Lord" @if(isset($user['title']) && $user['title'] == 'Lord'){{ 'selected' }}@endif>Lord</option>
    	  	</select>
    	  </div>
    	  @php
    	  	if(isset($user['name'])) {
	    	  	$name = explode(' ',$user['name']);
	    	  	$fname = $name[0];
	    	  	$lname = $name[1];
    	  	}
    	  @endphp
    	  <div class="row">
    	  	<div class="col-sm-6">
    	  		<div class="form-group">
    	  			<label class="label-control">Name</label>
    	  			<input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ $fname }}">
    	  		</div>
    	  	</div>
    	  	<div class="col-sm-6">
    	  		<div class="form-group">
    	  			<label class="label-control">Family Name</label>
    	  			<input type="text" name="fname" class="form-control" id="fname" placeholder="Family Name" value="{{ $lname }}">
    	  		</div>
    	  	</div>
    	  </div>
    	  <div class="form-group">
    	  	<label class="label-control">Gender</label>
    	  	<select class="form-control" id="gender" name="gender">
    	  		<option value="">Select Gender</option>
    	  		<option value="Male" @if(isset($user['gender']) && $user['gender'] == 'Male'){{ 'selected' }}@endif>Male</option>
    	  		<option value="Female" @if(isset($user['gender']) && $user['gender'] == 'Female'){{ 'selected' }}@endif>Female</option>
    	  		<option value="Other" @if(isset($user['gender']) && $user['gender'] == 'Other'){{ 'selected' }}@endif>Other</option>
    	  	</select>
    	  </div>
    </div>
    <div class="box-wrapper">
    	<p>Contact Details</p>
    	<div class="row">
    		<div class="col-sm-6">
    			<div class="form-group">
    				<label class="label-control">Phone</label>
    				<input type="text" name="contact_no" id="contact_no" class="form-control" placeholder="Phone" value="@if(isset($user['contact_no'])){{ $user['contact_no'] }}@endif">
    			</div>
    		</div>
    		<div class="col-sm-6">
    			<div class="form-group">
    				<label class="label-control">Email</label>
    				<input type="text" name="email" id="email" class="form-control" placeholder="Email" value="@if(isset($user['username'])){{ $user['username'] }}@endif">
    			</div>
    		</div>
    	</div>
    </div>
    <div class="box-wrapper">
    	<p>Account Details</p>
    	<div class="form-group">
    		<label class="label-control">Consultant ID</label>
    		<input type="text" name="consultant_id" id="consultant_id" class="form-control" placeholder="Consultant ID" value="@if(isset($user['consultant_id'])){{ $user['consultant_id'] }}@endif">
    	</div>
    	<div class="form-group">
    		<label class="label-control">Password</label>
    		<input type="password" name="password" id="password" class="form-control" placeholder="Password">
    	</div>
    	<div class="form-group">
    		<label class="label-control">Confirm Password</label>
    		<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password">
    	</div>
    	
    </div>
    <div class="box-wrapper">
    	<p>Domains</p>
    	<ul>
    	@foreach($user_domains as $domain)
    		<li>{{ $domain['domains']['name'] }}</li>
    	@endforeach
    	</ul>
    </div>
    <div class="row">
    	<div class="col-md-12 text-center">
    		<button type="submit" name="submit" class="btn btn-primary">Save</button>
    	</div>
    </div>
    </form>
</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{ asset('assets/js/agent.js') }}"></script>
@endpush