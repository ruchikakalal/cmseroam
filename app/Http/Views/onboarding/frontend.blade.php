@extends( 'layout/mainlayout' )
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap-colorpicker.css') }}">
@endpush
@section('content')
<div class="content-container" id="accordion">
    <div class="m-t-10">
        <?php
        if(isset($licensee_id) && $licensee_id!=''){ $licensee_id = $licensee_id; } else { $licensee_id = ''; } 
        ?>
        @include('WebView::onboarding.includes.onboarding-steps', array('id' => $licensee_id))
    </div>
    
    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 2: Front-End Configuration</h1>
    <div class="box-wrapper">
        <div class="form-group">
            <label class="label-control">Instances</label><br>
            <label class="radio-checkbox label_radio m-r-10" for="single-domain"><input type="radio" id="single-domain" value="single" name="instance" @if(count($frontends) <= 1) checked @endif>Single Instance (Single Domain)</label>
            <label class="radio-checkbox label_radio tooltip-wrapper" for="multiple-domain"><input type="radio" id="multiple-domain" value="multiple" name="instance" @if(count($frontends) > 1) checked @endif>Multiple Instances (Multiple Domains)</label>
        </div>
    </div>
    
    <div class="formbox">
        @if(count($frontends) < 1)
        <div class="box-wrapper">
            <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain"><p>Consumer Front-End</p></a>
            <div class="collapse" id="firstdomain">
                <form class="add-form" method="post" id="addform" action="{{route('frontend.add')}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="licensee_id" id="licensee_id" value="{{ $licensee_id }}">

                    <div class="box-wrapper m-t-20">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="label-control">Domain Name *</label>
                                <input type="text" name="domain_name" id="domain_name" class="form-control" placeholder="Domain Name" value="">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    
                        <label class="label-control">Products *</label>                  
                        <div class="row">
                            @foreach($products as $product)
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label class="radio-checkbox label_check m-t-10 @if($product['status'] == 0) disabled @endif" for="products-{{ $product['id'] }}">
                                        <input type="checkbox" class="checkbox" id="products-{{ $product['id'] }}" value="{{ $product['id'] }}" name="products[]" @if($product['status'] == 0) disabled data-toggle="tooltip" data-placement="top" title="Not available in Pilot" @endif>{{ $product['name'] }} 
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="box-wrapper m-t-20">
                        <p>Front-End CSS Stylng</p><br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="label-control">Template </label>
                                    <select class="form-control disabled" disabled name="template_id" id="template_id" data-toggle="tooltip" data-placement="top" title="Not available in Pilot">
                                        <option value="0">Select Template</option>
                                    </select>
                                </div>
                            </div>
                        </div><br>
                    
                        <label class="label-control">Logo *</label>
                        <div class="row image_upload_div">
                            <div class="col-sm-12">
                                <div class="file-upload1">
                                    <input type="hidden" id="logo" name="logo" value=""/>
                                    <input type="file" class="file-input fileupload" id="themeLogo" name="themeLogo"/>
                                </div>
                                <span class="input-filename"></span>
                                <span id="image-submit-load"></span>
                                <input type="hidden" name="image_name" id="image_name" class="image_name" value="">
                            </div>
                            <div class="small-centered columns error_message_image error"></div>
                        </div><br>
                    
                        <label class="label-control">Favicon *</label>
                        <div class="row image_upload_div">
                            <div class="col-sm-12">
                                <div class="file-upload1">
                                    <input type="hidden" id="favicon" name="favicon" value=""/>
                                    <input type="file" class="file-input faviconupload form-control" id="themeFavicon" name="themeFavicon"/>
                                </div>
                                <span class="input-filename"></span>
                                <span id="image-submit-load-favicon"></span>
                                <input type="hidden" name="favicon_name" id="favicon_name" class="favicon_name" value="">
                                
                            </div>
                            <div class="small-centered columns error_message_image_favicon error"></div>
                        </div><br>
                    
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="label-control">Browser Page title *</label>
                                    <input type="text" name="page_title" id="page_title" class="form-control" placeholder="Page Title" value="@isset($frontend){{ $frontend['domain']['page_title'] }}@endisset">
                                </div>
                            </div>
                        </div><br>
                    
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="label-control">Select Websafe Font *</label>
                                    <select class="form-control" name="webfont_id" id="webfont_id">
                                        @foreach($webfonts as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div><br>
                    
                        <p>Select Colour Scheme</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Theme Color *</label>
                                    <input autocomplete="off" id="theme_color" name="theme_color" type="text"  class="form-control colorpic" value="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Font Color *</label>
                                    <input autocomplete="off" id="font_color" name="font_color" type="text" class="form-control colorpic" value="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Header Bg Color *</label>
                                    <input autocomplete="off" id="header_color" name="header_color" type="text" class="form-control colorpic" value="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Footer Bg Color *</label>
                                    <input autocomplete="off" id="footer_color" name="footer_color" type="text" class="form-control colorpic" value="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Search Filter Bg Color *</label>
                                    <input autocomplete="off" id="search_color" name="search_color" type="text" class="form-control colorpic" value="" />
                                </div>
                            </div>
                        </div><br>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="radio-checkbox label_check m-t-15 disabled" for="consultant"><input type="checkbox" id="consultant" name="consultant" value="1" disabled data-toggle="tooltip" data-placement="top" title="Not available in Pilot">Consultant Front-End</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-wrapper m-t-20">
                        <p>Social Media Settings</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Facebook page link </label>
                                    <input type="text" name="facebook_link" id="facebook_link" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Twitter page link </label>
                                    <input type="text" name="twitter_link" id="twitter_link" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">LinkedIn page link </label>
                                    <input type="text" name="linkedin_link" id="linkedin_link" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Instagram page link </label>
                                    <input type="text" name="instagram_link" id="instagram_link" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Pinterest page link </label>
                                    <input type="text" name="pinterest_link" id="pinterest_link" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Google Plus page link </label>
                                    <input type="text" name="google_plus_link" id="google_plus_link" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">YouTube page link </label>
                                    <input type="text" name="youtube_link" id="youtube_link" class="form-control"  >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-t-20 row">
                    </div>
                    <div class="loader countriesLoader" style="display: none;"><img src="{{asset('assets/images/ajax-loader.gif')}}"></div>
                    <div class="m-t-20 row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="row">
                                <div class="col-sm-6">
                                    <button type="sumbit" class="btn btn-primary btn-block">Save</button>
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn btn-primary btn-block domainRemove">Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
        @endif
        @foreach($frontends as $frontend)
        <div class="box-wrapper" id="domainbox{{$frontend['domain_id']}}">
            <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain{{$frontend['id']}}"><p>Consumer Front-End</p></a>
            <div class="collapse" id="firstdomain{{$frontend['id']}}">
                <form class="add-form" method="post" id="form{{$frontend['id']}}" action="@isset($frontend['id']){{ route('frontend.update',[$frontend['domain']['id']]) }}@else{{ route('frontend.add') }}@endisset">
                    {{ csrf_field() }}
                    @isset($frontend['id'])
                        @method('PUT')
                    @endisset
                    <input type="hidden" name="licensee_id" id="licensee_id" value="{{ $licensee_id }}">
                    <div class="box-wrapper m-t-20">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="label-control">Domain Name *</label>
                                <input type="text" name="domain_name" id="domain_name{{$frontend['id']}}" class="form-control" placeholder="Domain Name" value="@isset($frontend){{ $frontend['domain']['name'] }}@endisset">
                            </div>

                        </div>
                        <div class="clearfix"></div>

                        <label class="label-control">Products *</label>
                        <div class="row">
                            @foreach($products as $product)
                            <?php $spro = $spros[$frontend['domain_id']] ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="radio-checkbox label_check m-t-10 @if($product['status'] == 0) disabled @endif" for="products-{{ $product['id'] }}-{{$frontend['id']}}">
                                        <input type="checkbox" class="checkbox" id="products-{{ $product['id'] }}-{{$frontend['id']}}" value="{{ $product['id'] }}" name="products[]" @if($product['status'] == 0) disabled data-toggle="tooltip" data-placement="top" title="Not available in Pilot" @endif @if(in_array($product['id'], $spro)) checked @endif>{{ $product['name'] }} 
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="box-wrapper m-t-20">
                        <p>Front-End CSS Stylng</p><br>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="label-control">Template </label>
                                    <select class="form-control disabled" disabled name="template_id" id="template_id{{$frontend['id']}}" data-toggle="tooltip" data-placement="top" title="Not available in Pilot">
                                        <option value="0">Select Template</option>
                                    </select>
                                </div>
                            </div>
                        </div><br>
                    
                        <label class="label-control">Logo *</label>
                        <div class="row image_upload_div">
                            <div class="col-sm-12">
                                <div class="file-upload1">
                                    <input type="hidden" id="logo{{$frontend['id']}}" name="logo" value=""/>
                                    <input type="file" class="file-input fileupload" id="themeLogo{{$frontend['id']}}" name="themeLogo"/>
                                </div>
                                <span class="input-filename"></span>
                                <span id="image-submit-load{{$frontend['id']}}">
                                    @isset($frontend['logo'])
                                        <img src="{{ $S3LOGO.'/'.$frontend['logo'] }}" width="100px">
                                    @endisset
                                </span>
                                <input type="hidden" name="image_name" id="image_name{{$frontend['id']}}" class="image_name" value="@isset($frontend['logo']){{$frontend['logo']}}@endisset">
                                
                            </div>
                            <div class="small-centered columns error_message_image error"></div>
                        </div><br>
                    
                        <label class="label-control">Favicon *</label>
                        <div class="row image_upload_div">
                            <div class="col-sm-12">
                                <div class="file-upload1">
                                    <input type="hidden" id="favicon{{$frontend['id']}}" name="favicon" value=""/>
                                    <input type="file" class="file-input faviconupload form-control" id="themeFavicon{{$frontend['id']}}" name="themeFavicon"/>
                                </div>
                                <span class="input-filename"></span>
                                <span id="image-submit-load-favicon{{$frontend['id']}}"></span>
                                    @isset($frontend['favicon'])
                                        <img src="{{ $S3LOGO.'/'.$frontend['favicon'] }}" width="100px">
                                    @endisset
                                <input type="hidden" name="favicon_name" id="favicon_name{{$frontend['id']}}" class="favicon_name" value="@isset($frontend['favicon']){{$frontend['favicon']}}@endisset">

                            </div>
                            <div class="small-centered columns error_message_image_favicon error"></div>
                        </div><br>
                    
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="label-control">Browser Page title *</label>
                                    <input type="text" name="page_title" id="page_title{{$frontend['id']}}" class="form-control page_title" placeholder="Page Title" value="@isset($frontend){{ $frontend['page_title'] }}@endisset">
                                </div>
                            </div>
                        </div><br>
                    
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="label-control">Select Websafe Font *</label>
                                    <select class="form-control" name="webfont_id" id="webfont_id{{$frontend['id']}}">
                                        @foreach($webfonts as $id => $name)
                                        <option value="{{ $id }}" @if(isset($frontend['webfont_id']) && $id == $frontend['webfont_id'])  {{ 'selected' }}@endif>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div><br>
                    
                        <p>Select Colour Scheme</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Theme Color *</label>
                                    <input autocomplete="off" id="theme_color{{$frontend['id']}}" name="theme_color" type="text"  class="form-control colorpic" value="@isset($frontend['theme_color']){{ $frontend['theme_color'] }}@endisset" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Font Color *</label>
                                    <input autocomplete="off" id="font_color{{$frontend['id']}}" name="font_color" type="text" class="form-control colorpic" value="@isset($frontend['font_color']){{ $frontend['font_color'] }}@endisset" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Header Bg Color *</label>
                                    <input autocomplete="off" id="header_color{{$frontend['id']}}" name="header_color" type="text" class="form-control colorpic" value="@isset($frontend['header_color']){{ $frontend['header_color'] }}@endisset" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Footer Bg Color *</label>
                                    <input autocomplete="off" id="footer_color{{$frontend['id']}}" name="footer_color" type="text" class="form-control colorpic" value="@isset($frontend['footer_color']){{ $frontend['footer_color'] }}@endisset" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Select Search Filter Bg Color *</label>
                                    <input autocomplete="off" id="search_color{{$frontend['id']}}" name="search_color" type="text" class="form-control colorpic" value="@isset($frontend['search_color']){{ $frontend['search_color'] }}@endisset" />
                                </div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="radio-checkbox label_check m-t-15 disabled" for="consultant{{$frontend['id']}}"><input type="checkbox" id="consultant{{$frontend['id']}}" name="consultant" value="1" disabled data-toggle="tooltip" data-placement="top" title="Not available in Pilot">Consultant Front-End</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-wrapper m-t-20">
                        <p>Social Media Settings</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Facebook page link </label>
                                    <input type="text" name="facebook_link" id="facebook_link" class="form-control" value="@isset($frontend){{ $frontend['facebook_link'] }}@endisset" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Twitter page link </label>
                                    <input type="text" name="twitter_link" id="twitter_link" class="form-control" value="@isset($frontend){{ $frontend['twitter_link'] }}@endisset" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">LinkedIn page link </label>
                                    <input type="text" name="linkedin_link" id="linkedin_link" class="form-control" value="@isset($frontend){{ $frontend['linkedin_link'] }}@endisset" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Instagram page link </label>
                                    <input type="text" name="instagram_link" id="instagram_link" class="form-control" value="@isset($frontend){{ $frontend['instagram_link'] }}@endisset" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Pinterest page link </label>
                                    <input type="text" name="pinterest_link" id="pinterest_link" class="form-control" value="@isset($frontend){{ $frontend['pinterest_link'] }}@endisset" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">Google Plus page link </label>
                                    <input type="text" name="google_plus_link" id="google_plus_link" class="form-control" value="@isset($frontend){{ $frontend['google_plus_link'] }}@endisset" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label-control">YouTube page link </label>
                                    <input type="text" name="youtube_link" id="youtube_link" class="form-control" value="@isset($frontend){{ $frontend['youtube_link'] }}@endisset" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-t-20 row">
                    </div>
                    
                    <div class="loader countriesLoader" style="display: none;"><img src="{{asset('assets/images/ajax-loader.gif')}}"></div>
                    <div class="m-t-20 row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="row">
                                <div class="col-sm-6">
                                    <button type="sumbit" class="btn btn-primary btn-block">Save</button>
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn btn-primary btn-block domainRemove" data-id="{{$frontend['domain_id']}}" data-issaved="true">Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endforeach
    </div>

    <div class="m-t-20 row">
        <div class="col-sm-8 addmore col-sm-offset-4" style="display: none;">
            <div class="row">
                <div class="col-sm-6">
                    <a href="#" class="btn btn-primary btn-block" id="addDomain">Add More Domain</a>
                </div>
            </div>
        </div>
        <div class="col-sm-8 m-t-10 col-sm-offset-2">
            <div class="row">
                <div class="col-sm-6">
                    <a href="{{ route('onboarding.general',[$licensee_id]) }}" class="btn btn-primary btn-block">Previous</a>
                </div>
                <div class="col-sm-6">
                    <a href="{{ route('onboarding.backend',[$licensee_id]) }}" class="btn btn-primary btn-block">Next</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-colorpicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".colorpic").colorpicker();
        check_domains();
    });

    function check_domains(){
        if($('input[name="instance"]:checked').val() == 'single'){
            $('.addmore').hide();
        }else{
            $('.addmore').show();
        }
    }

    $('input[name="instance"]').change(function(){
        check_domains();
    });

    $("#addform").validate({
        ignore: [],
        rules: {
            domain_name: {
                required: true,
                url:true
            },
            page_title: {
                required: true
            },
            favicon_name: {
                required: true
            },
            facebook_link: {
                url:true
            },
            twitter_link: {
                url:true
            },
            linkedin_link: {
                url:true
            },
            instagram_link: {
                url:true
            },
            pinterest_link: {
                url:true
            },
            google_plus_link: {
                url:true
            },
            youtube_link: {
                url:true
            },
            'products[]': {
                required: true
            },
            webfont_id:{
                required: true,
            },
            image_name: {
                required: true,
            },
            theme_color: {
                required: true,
            },
            font_color: {
                required: true
            },
            header_color: {
                required: true
            },
            footer_color: {
                required: true,
            },
            search_color: {
                required: true
            }
        },

        errorPlacement: function (label, element) {
            label.addClass('error_c');
            if($(element).hasClass('checkbox')){
                label.insertAfter($(element).parents('.row').children('.col-sm-6:last-child'));
            }else if($(element).hasClass('image_name')){
                label.insertAfter($(element).parent('.col-sm-12'));
            }else if($(element).hasClass('favicon_name')){
                label.insertAfter($(element).parent('.col-sm-12'));
            }else{
                label.insertAfter($(element).parent('.form-group'));
            }
        },
        submitHandler: function (form) {
            $("#addform").sumbit();
        }
    });

    function bindfileupload() {
        $('.fileupload').fileuploader({
            changeInput:'<div class="fileuploader-input">' +
                            '<div class="fileuploader-input-inner">' +
                                '<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Logo here to upload.</span></h3>' +
                            '</div>' +
                        '</div>' + 
                        '<div class="fileupload-link"><span>Click here to upload logo from your computer</span> (Dimension 200 X 60. Supported file types, .jpeg, .jpg, .png).</div>',
            theme: 'dragdrop',
            upload: {
                url: siteUrl('onboarding/upload-logo'),
                data: {logo:$('#logo').val()},
                type: 'POST',
                enctype: 'multipart/form-data',
                start: true,
                synchron: true,
                beforeSend: null,
                onSuccess: function(result, item) {
                    var data = result;
                    if(data.success == true){
                        $('.fileuploader-items-list').find('li').remove();
                        item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                        setTimeout(function() {
                            item.html.find('.progress-bar2').fadeOut(400);
                        }, 400);
                        item.name = data.data[0].image_name;
                        var img_url = data.data[0].image_path+'/'+data.data[0].image_name;
    
                        var img_html = '<img src="'+img_url+'" class="img-responsive">';
                        $("#image-submit-load").html('');
                        $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('Your file is selected');
                        $("#image-submit-load").css('display','block').html(img_html);
                        $("#image_name").val(data.data[0].image_name);
                        $("input[name='image_name']").valid();
                        $('.error_message_image').html('');
                    }else if(data.success == false) {
                        $('.fileuploader-items-list').find('li').remove();
                        $('.error_message_image').html('<p>'+data.error.message+'</p>');
                        $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Logo here to upload.</span></h3>');
                    }else{
                        $('.fileuploader-items-list').find('li').remove();
                    }

                    // if warnings
                    if (data.hasWarnings) {
                        for (var warning in data.warnings) {
                            alert(data.warnings);
                        }
                        item.html.removeClass('upload-successful').addClass('upload-failed');
                        return this.onError ? this.onError(item) : null;
                    }
                },
                onError: function(result, item,response) {
                    $('.error_message_image').html('<p>The logo must be a file of type: jpeg, jpg, png.</p>');
                },
                onComplete: null,
            }
        });
    }

    function bindfileuploadfavicon() {
        $('.faviconupload').fileuploader({
            changeInput:'<div class="fileuploader-input">' +
                            '<div class="fileuploader-input-inner">' +
                                '<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Favicon here to upload.</span></h3>' +
                            '</div>' +
                        '</div>' + 
                        '<div class="fileupload-link"><span>Click here to upload favicon from your computer</span> (Dimension 16 X 16. Supported file types, .png, .ico).</div>',
            theme: 'dragdrop',
            upload: {
                url: siteUrl('onboarding/upload-favicon'),
                data: {logo:$('#favicon').val()},
                type: 'POST',
                enctype: 'multipart/form-data',
                start: true,
                synchron: true,
                beforeSend: null,
                onSuccess: function(result, item) {
                    var data = result;
                    if(data.success == true){
                        $('.fileuploader-items-list').find('li').remove();
                        item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                        setTimeout(function() {
                            item.html.find('.progress-bar2').fadeOut(400);
                        }, 400);
                        item.name = data.data[0].image_name;
                        var img_url = data.data[0].image_path+'/'+data.data[0].image_name;
                    
                        var img_html = '<img src="'+img_url+'" class="img-responsive">';
                        $("#image-submit-load-favicon").html('');
                        $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('Your file is selected');
                        $("#image-submit-load-favicon").css('display','block').html(img_html);
                        $("#favicon_name").val(data.data[0].image_name);
                        $("input[name='favicon_name']").valid();
                        $('.error_message_image_favicon').html('');
                    }else if(data.success == false) {
                        $('.fileuploader-items-list').find('li').remove();
                        $('.error_message_image_favicon').html('<p>'+data.error.message+'</p>');
                        $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Favicon here to upload.</span></h3>');
                    }else{
                        $('.fileuploader-items-list').find('li').remove();
                    }

                    // if warnings
                    if (data.hasWarnings) {
                        for (var warning in data.warnings) {
                            alert(data.warnings);
                        }
                        item.html.removeClass('upload-successful').addClass('upload-failed');
                        return this.onError ? this.onError(item) : null;
                    }
                },
                onError: function(result, item,response) {
                    $('.error_message_image_favicon').html('<p>The favicon must be a file of type: png, ico.</p>');
                },
                onComplete: null,
            }
        });
    }
    
    $(document).on('click','#single-domain',function(e) {

    });

    $(document).on('click','#multiple-domain',function() {
        $("#single-domain").prop('checked',false);
    });
	
	$(document).ready(function () {
		jQuery.validator.addMethod('url', function (value, element, param) {
			return this.optional(element)|| /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(value); 
		}, "Url pattern is not Valid");
        checkifsingelavailable();
	});

    function checkifsingelavailable() {
        var numForms = $('.add-form').length;
        if (numForms > 1) {
            $("#single-domain").prop('disabled',true);
            $("#single-domain").parent('label').addClass('disabled');
            $("#single-domain").addClass('disabled');
        }else{
            $("#single-domain").prop('disabled',false);
            $("#single-domain").parent('label').removeClass('disabled');
            $("#single-domain").removeClass('disabled');
        }
    }

    var addButton = $('#addDomain'); //Add button selector
    var wrapper = $('.formbox'); //Input field wrapper
    $(addButton).click(function(){
        var url = "{{route('frontend.addDomain')}}";
        var numForms = $('.add-form').length;
        onboard.showloader(); //Add field html
        $.ajax({
            type: "POST",
            url: url,
            data: {id:numForms,licensee_id:$('#licensee_id').val()},
            success: function(result){
                $(wrapper).append(result);
                $(".colorpic").colorpicker();
                onboard.hideloader();
                $("#addDomain").focusout();
                checkifsingelavailable();
            },
        });
    });


    //Once remove button is clicked
    $(document).on('click', '.domainRemove', function(e){
        e.preventDefault();
        onboard.showloader();
        var domain_id = $(this).data('id');
        var pleaseconfirm = confirm("The domain and its related data will be deleted, are you sure?");
        if (pleaseconfirm) {
            if($(this).data('issaved')){
                url = "{{route('frontend.deleteDomain')}}";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {domain_id:domain_id,licensee_id:$('#licensee_id').val()},
                    success: function(result){
                        $("#domainbox"+domain_id).remove();
                        onboard.hideloader();
                        checkifsingelavailable();
                    },
                });
            }else{
                onboard.hideloader();
                $("#domainbox"+domain_id).remove();
                checkifsingelavailable();
            }
        }
    });
</script>
@foreach($frontends as $frontend)
<script type="text/javascript">
    var id = "{{$frontend['id']}}";

    $("#form"+id).validate({
        ignore: [],
        rules: {
            domain_name: {
                required: true,
                url:true
            },
            page_title: {
                required: true
            },
            favicon_name: {
                required: true
            },
            facebook_link: {
                url:true
            },
            twitter_link: {
                url:true
            },
            linkedin_link: {
                url:true
            },
            instagram_link: {
                url:true
            },
            pinterest_link: {
                url:true
            },
            google_plus_link: {
                url:true
            },
            youtube_link: {
                url:true
            },
            'products[]': {
                required: true
            },
            webfont_id:{
                required: true,
            },
            image_name: {
                required: true,
            },
            theme_color: {
                required: true,
            },
            font_color: {
                required: true
            },
            header_color: {
                required: true
            },
            footer_color: {
                required: true,
            },
            search_color: {
                required: true
            }
        },

        errorPlacement: function (label, element) {
            label.addClass('error_c');
            if($(element).hasClass('checkbox')){
                label.insertAfter($(element).parents('.row').children('.col-sm-6:last-child'));
            }else if($(element).hasClass('image_name')){
                label.insertAfter($(element).parent('.col-sm-12'));
            }else if($(element).hasClass('favicon_name')){
                label.insertAfter($(element).parent('.col-sm-12'));
            }else{
                label.insertAfter($(element).parent('.form-group'));
            }
        },
        submitHandler: function (form) {
            $("#form"+id).sumbit();
        }
    });

    $('#themeLogo'+id).fileuploader({
        changeInput:'<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                            '<h3 class="fileuploader-input-caption"><span class="span{{$id}}"><i class="icon-add-photos"></i> Drag Theme Logo here to upload.</span></h3>' +
                        '</div>' +
                    '</div>' + 
                    '<div class="fileupload-link"><span>Click here to upload logo from your computer</span> (Dimension 200 X 60. Supported file types, .jpeg, .jpg, .png).</div>',
        theme: 'dragdrop',
        upload: {
            url: siteUrl('onboarding/upload-logo'),
            data: {logo:$('#logo'+id).val(),id:id},
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: null,
            onSuccess: function(result, item) {
                var data = result;
                if(data.success == true){
                    $('.fileuploader-items-list').find('li').remove();
                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                    item.name = data.data[0].image_name;
                    var img_url = data.data[0].image_path+'/'+data.data[0].image_name;

                    var img_html = '<img src="'+img_url+'" class="img-responsive">';
                    id = data.data[0].id;
                    $("#image-submit-load"+id).html('');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption .span"+id).html('1  files were chosen');
                    $("#image-submit-load"+id).css('display','block').html(img_html);
                    $("#image_name"+id).val(data.data[0].image_name);
                    // $("input[name='image_name']").valid();
                    $('.error_message_image').html('');
                }else if(data.success == false) {
                    $('.fileuploader-items-list').find('li').remove();
                    $('.error_message_image').html('<p>'+data.error.message+'</p>');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Logo here to upload.</span></h3>');
                }else{
                    $('.fileuploader-items-list').find('li').remove();
                }

                // if warnings
                if (data.hasWarnings) {
                    for (var warning in data.warnings) {
                        alert(data.warnings);
                    }
                    item.html.removeClass('upload-successful').addClass('upload-failed');
                    return this.onError ? this.onError(item) : null;
                }
                
                
            },
            onError: function(result, item,response) {
                $('.error_message_image').html('<p>The logo must be a file of type: jpeg, jpg, png.</p>')
            },
            onComplete: null,
        }
    });

    $('#themeFavicon'+id).fileuploader({
        changeInput:'<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                            '<h3 class="fileuploader-input-caption"><span class="span{{$id}}"><i class="icon-add-photos"></i> Drag Theme Favicon here to upload.</span></h3>' +
                        '</div>' +
                    '</div>' + 
                    '<div class="fileupload-link"><span>Click here to upload favicon from your computer</span> (Dimension 16 X 16. Supported file types, .png, .ico).</div>',
        theme: 'dragdrop',
        upload: {
            url: siteUrl('onboarding/upload-favicon'),
            data: {logo:$('#favicon'+id).val(),id:id},
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: null,
            onSuccess: function(result, item) {
                var data = result;
                if(data.success == true){
                    $('.fileuploader-items-list').find('li').remove();
                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                    item.name = data.data[0].image_name;
                    var img_url = data.data[0].image_path+'/'+data.data[0].image_name;

                    var img_html = '<img src="'+img_url+'" class="img-responsive">';
                    id = data.data[0].id;
                    $("#image-submit-load-favicon"+id).html('');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption .span"+id).html('1  files were chosen');
                    $("#image-submit-load-favicon"+id).css('display','block').html(img_html);
                    $("#favicon_name"+id).val(data.data[0].image_name);
                    // $("input[name='image_name']").valid();
                    $('.error_message_image_favicon').html('');
                }else if(data.success == false) {
                    $('.fileuploader-items-list').find('li').remove();
                    $('.error_message_image_favicon').html('<p>'+data.error.message+'</p>');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Favicon here to upload.</span></h3>');
                }else{
                    $('.fileuploader-items-list').find('li').remove();
                }

                // if warnings
                if (data.hasWarnings) {
                    for (var warning in data.warnings) {
                        alert(data.warnings);
                    }
                    item.html.removeClass('upload-successful').addClass('upload-failed');
                    return this.onError ? this.onError(item) : null;
                }
                
                
            },
            onError: function(result, item,response) {
                $('.error_message_image_favicon').html('<p>The favicon must be a file of type: png , ico.</p>')
            },
            onComplete: null,
        }
    });
</script>
@endforeach
@if(count($frontends) < 1)
<script type="text/javascript">
    bindfileupload();
    bindfileuploadfavicon();
</script>
@endif
@endpush