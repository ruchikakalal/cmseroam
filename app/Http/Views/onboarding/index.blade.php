@extends( 'layout/mainlayout' )

@section('content')
	<div class="content-container">
	  <h1 class="page-title">Client Onboarding</h1> 
	  <div class="box-wrapper">
	    <div class="table-responsive m-t-20">
	      <table class="table">
	        <thead>
	          <tr>
	          	<th>S.No.</th>
	            <th>Licensee Name</th>
	            <th>Business Name</th>
	            <th>Email</th>
	            <th>Phone No.</i></th>
	            <th>Domain</th>
	            <th>Action</th>
	          </tr>
	        </thead>
	        <tbody>
	        @foreach($licensees as $licensee)
	        @php $domains = array_column($licensee['domains']->toArray(),'name'); @endphp
	          <tr>
	            <td>{{ $loop->iteration }}</td>
	            <td>{{ $licensee['first_name'].' '.$licensee['last_name'] }}</td>
	            <td>{{ $licensee['business_name'] }}</td>
	            <td>{{ $licensee['email'] }}</td>
	            <td>{{ $licensee['phone_number'] }}</td>
	            <td>{{ implode(', ',$domains) }}</td>
	            <td class="text-center">
	            	<a href="{{ route('onboarding.view',$licensee['id']) }}" class="button success tiny btn-primary btn-sm">View</a>
	            	<a href="{{ route('onboarding.general',$licensee['id']) }}" class="button success tiny btn-primary btn-sm">Edit</a>
	            </td>
	          </tr>
	        @endforeach
	        </tbody>
	      </table>
	    </div>
	  </div>
	</div>
@endsection