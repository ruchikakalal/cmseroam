@extends( 'layout/mainlayout' )
<?php
if(isset($licensee['id']) && $licensee['id']!=''){ $idLicensee = $licensee['id']; } else { $idLicensee = ''; } 
//pr($idLicensee);
?>
@section('content')
<div class="content-container">
    <div class="m-t-10">
      @include('WebView::onboarding.includes.onboarding-steps', array('id' => $idLicensee))
    </div>
    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 1: General</h1>
    <form method="post" action="@isset($licensee['id']){{ route('general.update',$licensee['id']) }}@else{{ route('general.add') }} @endisset" class="add-form" id="general-form">
    	{{ csrf_field() }}
      @isset($licensee['id'])
        @method('PUT')
      @endisset
        <div class="box-wrapper">
          <div class="form-group m-t-10">
            <label class="label-control">Pre-Set Account Selection <span class="aestrik">*</span></label>
            <select class="form-control disabled" data-toggle="tooltip" data-placement="top" title="Not available in Pilot">
              <option>Pre-Set Account Option 01</option>
            </select>
          </div>
          <div class="form-group">
            <label for="" class="label-control">Legal Name of Business <span class="aestrik">*</span></label>
            <input type="text" class="form-control" name="business_name" id="business_name" placeholder="Legal Name of Business" value="@isset($licensee['business_name']){{$licensee['business_name']}}@endisset" />
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Licensee Holder (First Name) <span class="aestrik">*</span></label>
                  <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="@isset($licensee['first_name']){{ $licensee['first_name']}}@endisset" />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Licensee Holder (Last Name) <span class="aestrik">*</span></label>
                  <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="@isset($licensee['last_name']){{$licensee['last_name'] }}@endisset" />
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Licensee Holder Email Address <span class="aestrik">*</span></label>
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="@isset($licensee['email']){{$licensee['email']}}@endisset" />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Licensee Holder Phone Number</label>
                  <input type="tel" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number" value="@isset($licensee['phone_number']){{ $licensee['phone_number'] }}@endisset" />
                </div>
            </div>
          </div>
        <div class="box-wrapper m-t-20">
          <p>Business Address</p>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Street <span class="aestrik">*</span></label>
                  <input type="text" class="form-control" name="street" id="street" placeholder="Street" value="@isset($licensee['street']){{$licensee['street']}}@endisset" />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Apartment, Suite, etc.</label>
                  <input type="text" class="form-control" name="apartment" id="apartment" placeholder="Apartment" value="@isset($licensee['apartment']){{$licensee['apartment'] }}@endisset" />
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">City <span class="aestrik">*</span></label>
                  <input type="text" class="form-control" name="city" id="city" placeholder="City" value="@isset($licensee['city']){{ $licensee['city'] }}@endisset" />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">State / Territory  <span class="aestrik">*</span></label>
                  <input type="text" class="form-control" name="state" id="state" placeholder="State" value="@isset($licensee['state']){{$licensee['state']}}@endisset" />
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Country <span class="aestrik">*</span></label>
                  <select class="form-control" name="country_id" id="country_id">
                    @foreach($countries as $id => $country)
                    	<option value="{{ $id }}" @if(isset($licensee['country_id']) && $id == $licensee['country_id'])  {{ 'selected' }}@endif>{{ $country }}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                  <label for="" class="label-control">Postal / ZIP Code <span class="aestrik">*</span></label>
                  <input type="tel" class="form-control" name="zip_code" id="zip_code" placeholder="Postal / ZIP Code" value="@isset($licensee['zip_code']){{$licensee['zip_code']}}@endisset" />
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
              <div class="form-group">
                <label for="" class="label-control">Timezone </label>
                <select class="form-control" name="timezone_id" id="timezone_id">
                    @foreach($timezones as $id => $timezone)
                    	<option value="{{ $id }}" @if(isset($licensee['timezone_id']) && $id == $licensee['timezone_id'])  {{ 'selected' }}@endif>{{ $timezone }}</option>
                    @endforeach
                </select>
              </div>
          </div>
          <div class="col-sm-6">
              <div class="form-group">
                <label for="" class="label-control">Website URL</label>
                <input type="text" class="form-control" name="website" id="website" placeholder="Website URL" value="@isset($licensee['website']){{$licensee['website']}}@endisset" />
              </div>
          </div>
        </div>
      </div>

      @isset($licensee['id'])
        <div class="m-t-20 row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <button type="sumbit" name="" class="btn btn-primary btn-block">Save</button>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ route('onboarding.frontend',$licensee['id']) }}" class="btn btn-primary btn-block">Next</a>
                    </div>
                </div>
            </div>
        </div>
      @else
        <div class="m-t-20 row">
            <div class="col-sm-offset-2 col-sm-8">
              <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                  <div class="row">
                    <button type="sumbit" name="" class="btn btn-primary btn-block">Next</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
      @endisset
    </form>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
	jQuery.validator.addMethod('url', function (value, element, param) {
        return this.optional(element)|| /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(value); 
	}, "Url pattern is not Valid");
	
});
    $("#general-form").validate({
        ignore: [],
        rules: {
            business_name: {
                required: true
            },
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            street: {
                required: true
            },
            city: {
                required: true
            },
            state: {
                required: true,
            },
            country_id: {
                required: true
            },
            zip_code:{
                required: true
            },
            website:{
                url:true
			},
        },

        errorPlacement: function (label, element) {
            label.addClass('error_c');
            label.insertAfter($(element).parent('.form-group'));
        },
        submitHandler: function (form) {
            $('.add-form').sumbit();
        }
    });
</script>
@endpush