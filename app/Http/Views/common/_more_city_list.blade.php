@foreach ($oCityList as $aCity)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aCity->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aCity->id;?>" value="<?php echo $aCity->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aCity->name }}
            </a>
        </td>
        <td> {!! ($aCity->optional_city) ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>' !!} </td>
        <td>{{ $aCity->country_name }}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('common.create-city',[ 'nIdCity' => $aCity->id ])}}" class="button success tiny btn-primary btn-sm m-r-10">{{ trans('messages.update_btn') }}</a>
            <input type="button" class="button btn-delete tiny btn-primary btn-sm m-r-10" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('common.delete-city',['nIdCity'=> $aCity->id]) }}','{{ trans('messages.delete_label')}}')">
            </div>
        </td>
    </tr> 
@endforeach