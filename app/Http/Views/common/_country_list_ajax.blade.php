<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCountrySort(this,'c.name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'c.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'r.name');"> {{ trans('messages.region_name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'r.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">Active</th>
        </tr>
    </thead>
    <tbody class="country_list_ajax">
    @if(count($oCountryList) > 0)
        @include('WebView::common._more_country_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oCountryList->count() , 'total'=>$oCountryList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oCountryList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oCountryList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('common/country-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                    callCountryListing(event,'country_list_ajax',pageNumber);
//                else
//                    callCountryListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>