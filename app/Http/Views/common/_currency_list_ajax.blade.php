<table class="table">
    <thead>
        <tr>
            <th width="20px">
                <label class="radio-checkbox label_check" for="checkbox-00" >
                    <input type="checkbox" id="checkbox-00" value="1" onchange = "selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCouponSort(this, 'name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCouponSort(this, 'code');">{{ trans('messages.code') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'code')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>{{ trans('messages.action_head')}}</th>
        </tr>
    </thead>
    <tbody class="coupon_list_ajax">
    @if(count($oCurrencyList) > 0)
        @include('WebView::common._more_currency_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oCurrencyList->count() , 'total'=>$oCurrencyList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oCurrencyList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oCurrencyList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('common/currency-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                    getMoreListing(siteUrl('common/currency-list?page='+pageNumber),event,'coupon_list_ajax');
//                else
//                    getMoreListing(siteUrl('common/currency-list?page='+pageNumber),event,'table_record');
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>