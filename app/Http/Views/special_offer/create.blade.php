@extends( 'layout/mainlayout' )

@section('content')
	<div class="content-container">
        <h1 class="page-title">@empty($special_offer) {{ trans('messages.add_special_offer') }} @else {{ trans('messages.update_special_offer') }} @endisset</h1>
        <div class="row" id="error_msg">
           
        </div>	
        <br>
        <form action="@empty($special_offer) {{ route('special-offer.store') }} @else {{ route('special-offer.update',$special_offer) }} @endisset" method="post" id="special-offer-form">
            {{ csrf_field() }}
            @isset($special_offer['offer_name'])
            	@method('PUT')
            @endisset
            <div class="box-wrapper">
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.offer_name') }}<span class="required">*</span></label>
                    <input type="text" id="offer_name" class="form-control" placeholder="Enter Offer Name" name="offer_name" value="@isset($special_offer['offer_name']){{$special_offer->offer_name}} @endisset"/>
                </div> 

                <div class="form-group m-t-30">
                    <label class="label-control">Offer Type</label><br />

                    <label class="radio-checkbox label_radio" for="value_based_dollar">
                        <input type="radio"  name="offer_type" id="value_based_dollar" value="value_based_dollar"  class="offer_type" @isset($special_offer['offer_type']) @if($special_offer['offer_type'] === 'value_based_dollar'){{ 'checked' }}@endif @else {{ 'checked' }} @endisset> Value Based Offer ( <i class="fa fa-dollar"></i> )
                    </label>

                    <label class="radio-checkbox label_radio" for="value_based_per">
                        <input type="radio" name="offer_type" id="value_based_per" value="value_based_per" class="offer_type"  @isset($special_offer['offer_type']) @if($special_offer['offer_type'] === 'value_based_per'){{ 'checked' }}@endif @endisset> Value Based Offer ( <i class="fa fa-percent"></i> )
                    </label>

                    <label class="radio-checkbox label_radio" for="term_based">
                        <input type="radio" name="offer_type" id="term_based" value="term_based" class="offer_type"  @isset($special_offer['offer_type']) @if($special_offer['offer_type'] === 'term_based'){{ 'checked' }}@endif @endisset> Term Based Offer
                    </label>
                </div>
               	<div class="row">
               		<div class="col-sm-6">
               			<div class="form-group m-t-20">
               			    <label class="label-control">Inventory Type <span class="required">*</span></label>
               			    <select name="inventory_type" id="inventory_type" class="form-control" data-url="{{ route('products') }}">
               			    	<option value="">Select Inventory Type</option>
               			    	<option value="Accommodation" @isset($special_offer['inventory_type']) @if($special_offer['inventory_type'] === 'Accommodation'){{ 'selected' }}@endif @endisset>Accommodation</option>
               			    	<option value="Activity" @isset($special_offer['inventory_type']) @if($special_offer['inventory_type'] === 'Activity'){{ 'selected' }}@endif @endisset>Activity</option>
               			    	<option value="Tour" @isset($special_offer['inventory_type']) @if($special_offer['inventory_type'] === 'Tour'){{ 'selected' }}@endif @endisset>Tour</option>
               			    	<option value="Transport" @isset($special_offer['inventory_type']) @if($special_offer['inventory_type'] === 'Transport'){{ 'selected' }}@endif @endisset>Transport</option>
               			    </select>
               			</div>
               		</div>
               		<div class="col-sm-6">
               			<div class="form-group m-t-20" style="padding-bottom: 6px;">
               			    <label class="label-control">Inventory/Product Name <span class="required">*</span></label>
               			    <select name="product_name" id="product_name" class="form-control" data-url="{{ route('price') }}">
               			    	<option value="">Select Product</option>
               			    	@isset($product_name) 
               			    		@foreach($product_name as $product)
               			    			<option value="{{ $product['id'] }}" @isset($special_offer['product_name']) @if($special_offer['product_name'] == $product['id']){{ 'selected' }}@endif @endisset>{{ $product['name'] }}</option>
               			    		@endforeach
               			    	@endisset
               			    </select>
               			</div>
               		</div>
               	</div>
                <div class="form-group m-t-20">
                    <label class="label-control">Licensee / Client <span class="required">*</span></label>
                    <select name="client[]" multiple id="client" class="form-control">
                    	<option value="All" @isset($special_offer['client']) {{''}} @else {{ 'selected' }}@endisset>All</option>
                    	@foreach($licensee as $l) 
                    		<option value="{{ $l['name'] }}" @isset($special_offer['client']) @if(in_array($l['name'],explode(',',$special_offer['client']))){{ 'selected' }}@endif @endisset>{{ $l['name'] }}</option>
                    	@endforeach
                    </select>
                </div>
                <div class="row">
                	<div class="col-sm-6">
                		<div class="form-group m-t-20">
                		    <label class="label-control">Selling Start Date<span class="required">*</span></label>
                		    <input type="text" id="selling_start_date" class="form-control datepicker" placeholder="Selling Start Date" name="selling_start_date" value="@isset($special_offer['selling_start_date']){{$special_offer->selling_start_date}} @endisset" />
                		</div>
                	</div>
                	<div class="col-sm-6">
                		<div class="form-group m-t-20">
                		    <label class="label-control">Selling End Date<span class="required">*</span></label>
                		    <input type="text" id="selling_end_date" class="form-control datepicker" placeholder="Selling End Date" name="selling_end_date" value="@isset($special_offer['selling_end_date']){{$special_offer->selling_end_date}} @endisset" />
                		</div>
                	</div>
                </div>
                <div class="row">
                	<div class="col-sm-6">
                		<div class="form-group m-t-20">
                		    <label class="label-control">Travel Start Date<span class="required">*</span></label>
                		    <input type="text" id="travel_start_date" class="form-control datepicker" placeholder="Travel Start Date" name="travel_start_date" value="@isset($special_offer['travel_start_date']){{$special_offer->travel_start_date}} @endisset" />
                		</div>
                	</div>
                	<div class="col-sm-6">
                		<div class="form-group m-t-20">
                		    <label class="label-control">Travel End Date<span class="required">*</span></label>
                		    <input type="text" id="travel_end_date" class="form-control datepicker" placeholder="Travel End Date" name="travel_end_date" value="@isset($special_offer['travel_end_date']){{$special_offer->travel_end_date}} @endisset" />
                		</div>
                	</div>
                </div>
                <div id="offer-type-fields" class="@empty($special_offer){{ 'hide' }}@endempty">
                <div id="value-based-field" class="@empty($special_offer) {{'hide'}} @else @if (($special_offer['offer_type'] === 'value_based_per') || ($special_offer['offer_type'] === 'value_based_dollar'))
		    {{''}}
		@else
		    {{'hide'}}
		@endif @endempty">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="form-group m-t-20">
	                		    <label class="label-control">Original Price</label>
	                		    <input type="text" id="original_price" class="form-control" placeholder="Original Price" name="original_price" @isset($special_offer['original_price']){{$special_offer->original_price}} @endisset/>
	                		</div>
	                	</div>
	                	<div class="col-sm-6">
	                		<div class="form-group m-t-20">
		                		<div class="checkbox">
		                		    <label  class="label-control">
		                		      <input  type="checkbox" value="Yes" name="children_discount" id="children_discount" @isset($special_offer['children_discount']) @if($special_offer['children_discount'] === 'Yes'){{ 'checked' }}@endif @endisset> Children Discount
		                		    </label>
		                		</div>
	                		</div>
	                	</div>
	                </div>
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="form-group m-t-20">
		                		<label class="label-control">Discounted Offer</label>
		                		<input type="text" name="discounted_offer" id="discounted_offer" placeholder="Discounted Offer" class="form-control" value="@isset($special_offer['discount_offer']){{$special_offer->discount_offer}} @endisset">
	                		</div>
	                	</div>
	                	<div class="col-sm-6">
	                		<div class="form-group m-t-20">
			            		<label class="label-control">Discounted Price</label>
			            		<input type="text" name="discounted_price" id="discounted_price" placeholder="Discounted Price" class="form-control" value="@isset($special_offer['discount_price']){{$special_offer->discount_price}} @endisset">
			            	</div>
	                	</div>
	                </div>
	                <div id="child-fields" class="@isset($special_offer['children_discount'])@if($special_offer['children_discount'] !== 'Yes'){{'hide'}}@endif @endisset">
		                <div class="row">
		                	<div class="col-sm-6">
		                		<div class="form-group m-t-20">
			                		<label class="label-control">Child Discounted Offer</label>
			                		<input type="text" name="children_discounted_offer" id="children_discounted_offer" placeholder="Child Discounted Offer" class="form-control" value="@isset($special_offer['children_discount_offer']){{$special_offer->children_discount_offer}} @endisset">
		                		</div>
		                	</div>
		                	<div class="col-sm-6">
		                		<div class="form-group m-t-20">
				            		<label class="label-control">Child Discounted Price</label>
				            		<input type="text" name="children_discounted_price" id="children_discounted_price" placeholder="Child Discounted Price" class="form-control" value="@isset($special_offer['children_discount_price']){{$special_offer->children_discount_price}} @endisset">
				            	</div>
		                	</div>
		                </div>
	            	</div>
	                <div class="form-group m-t-20">
	                	<label class="label-control">Offer Description</label>
	                	<textarea name="offer_description" id="offer_description" class="form-control">@isset($special_offer['offer_description']){{$special_offer->offer_description}} @endisset</textarea>
	                </div>
            	</div>
            	<div id="term-based-field" class="@isset($special_offer['offer_type']) @if($special_offer['offer_type'] === 'term_based'){{ '' }} @else {{'hide'}} @endif @endisset">
	                <div class="form-group m-t-20">
	                    <label class="label-control"> Term Based Option <span class="required">*</span></label>
	                    <select name="term_option" id="term_option" class="form-control">
	                    	<option value=""> Select Term Based Option</option>
	                    	<option value="Value Add"  @isset($special_offer['term_option']) @if($special_offer['term_option'] === 'Value Add'){{ 'selected' }}@endif @endisset> Value Add</option>
	                    	<option value="Free Flight" @isset($special_offer['term_option']) @if($special_offer['term_option'] === 'Free Flight'){{ 'selected' }}@endif @endisset>Free Flight</option>
	                    	<option value="Free Upgrade" @isset($special_offer['term_option']) @if($special_offer['term_option'] === 'Free Upgrade'){{ 'selected' }}@endif @endisset>Free Upgrade</option>
	                    	<option value="Buy One Get One Free" @isset($special_offer['term_option']) @if($special_offer['term_option'] === 'Buy One Get One Free'){{ 'selected' }}@endif @endisset>Buy One Get One Free</option>
	                    	<option value="Stay Pay Deal" @isset($special_offer['term_option']) @if($special_offer['term_option'] === 'Stay Pay Deal'){{ 'selected' }}@endif @endisset>Stay Pay Deal</option>
	                    </select>
	                </div>
            	</div>
            	<div id="term-option-field" class="@empty($special_offer['term_option']){{ 'hide' }}@endempty">
	                <div id="stay-deal-field" class="@isset($special_offer['term_option']) @if($special_offer['term_option'] !== 'Stay Pay Deal'){{ 'hide' }}@endif @endisset">
		                <div class="row">
		                	<div class="col-sm-6">
		                		<div class="form-group m-t-20">
			                		<label class="label-control">Cost Per Night</label>
			                		<input type="text" name="cost_per_night" id="cost_per_night" placeholder="Cost Per Night" class="form-control" value="@isset($special_offer['cost_per_night']){{$special_offer->cost_per_night}} @endisset">
		                		</div>
		                	</div>
		                	<div class="col-sm-6">
		                		<div class="form-group m-t-20">
				            		<label class="label-control">Original Number of Nights</label>
				            		<input type="text" name="original_nights" id="original_nights" placeholder="Original Number of Nights" class="form-control" value="@isset($special_offer['original_nights']){{$special_offer->original_nights}} @endisset">
				            	</div>
		                	</div>
		                </div>
		                <div class="row">
		                	<div class="col-sm-6">
		                		<div class="form-group m-t-20">
			                		<label class="label-control"> Additional ‘Free’ Nights</label>
			                		<input type="text" name="free_nights" id="free_nights" placeholder=" Additional ‘Free’ Nights" class="form-control" value="@isset($special_offer['free_nights']){{$special_offer->free_nights}} @endisset">
		                		</div>
		                	</div>
		                	<div class="col-sm-6">
		                		<div class="form-group m-t-20">
				            		<label class="label-control"> Additional Fee (Per Person, Per Night)</label>
				            		<input type="text" name="fee" id="fee" placeholder=" Additional Fee (Per Person, Per Night)" class="form-control" value="@isset($special_offer['fee']){{$special_offer->fee}} @endisset">
				            	</div>
		                	</div>
		                </div>
	            	</div>
	                <div class="form-group m-t-20">
	                	<label class="label-control"> Additional Services <span class="required">*</span></label>
	                	<textarea class="form-control" name="additional_services" id="additional_services">@isset($special_offer['additional_services']){{$special_offer->additional_services}} @endisset</textarea>
	                </div>
	                <div class="form-group m-t-20">
	                	<label class="label-control"> Terms and Conditions <span class="required">*</span></label>
	                	<textarea class="form-control" name="terms_n_conditions" id="terms_n_conditions">@isset($special_offer['terms_n_conditions']){{$special_offer->terms_n_conditions}} @endisset</textarea>
	                </div>
	                
            	</div>
            </div>
            </div>
            <div class="m-t-20">
            	<div class="col-sm-6">
            		<input type="submit" name="sub" value="Save Offer" class="btn btn-primary btn-block">
            	</div>
            	<div class="col-sm-6">
            		<a href="{{ route('special-offer.list') }}" class="btn btn-primary btn-block">CANCEL</a>
            	</div>
            	
            </div>
        </form>
    </div>
@stop

@section('custom-js')

	<script type="text/javascript">
    $(document).ready(function(){
      $("#offer-type-fields").removeClass('hide');
      $("#value-based-field").removeClass('hide');
      $("#term-based-field").addClass('hide');
    })
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

		$('#product_name').selectize({
		    create: true,
		    sortField: 'text'
		});

		$('#client').selectize({
		    maxItems: 10
		});
		$(".datepicker").datepicker({
			format: 'dd/mm/yyyy'
		});

		// $(document).ready(function(){
		// 	$("#offer-type-fields").addClass('hide');
		// });

		$(document).on('change','.offer_type',function() {
			$("#offer-type-fields").removeClass('hide');
			if($(this).val() === 'value_based_per' || $(this).val() === 'value_based_dollar') {
				$("#value-based-field").removeClass('hide');
				$("#term-based-field").addClass('hide');
			}
			if($(this).val() === 'term_based') {
				$("#value-based-field").addClass('hide');
				$("#term-based-field").removeClass('hide');
			}
		}); 

		$(document).on('change','#children_discount',function(){
			if($(this).prop('checked') === true) {
				$("#child-fields").removeClass('hide');
			} else {
				$("#child-fields").addClass('hide');
			}
		});

		$(document).on('change','#term_option',function() {
			if($(this).val() !== "") {
				$("#term-option-field").removeClass('hide');
				if($(this).val() === 'Stay Pay Deal') {
					$("#stay-deal-field").removeClass('hide');
				} else {
					$("#stay-deal-field").addClass('hide');
				}
			} else {
				$("#term-option-field").addClass('hide');
			}
		});

		$(document).on('change','#inventory_type',function() {
			var url = $(this).data('url');
			var inventory = $(this).val();

			$.post(url,{inventory:inventory},function(out) {	
				$("#product_name").html('');
				$("#product_name")[0].selectize.destroy();	
				$("#product_name").html(out);
				$("#product_name").selectize()
			});
		});

		$(document).on('change','#product_name',function() {
			var product_id = $("#product_name option:selected").val();
			var inventory = $("#inventory_type").val();

			var url = $(this).data('url');

			$.post(url,{product_id:product_id,inventory:inventory},function(out) {
				if(out.price) {
					$("#original_price").val(out.price);
				}else {
					$("#original_price").val(0);
				}
			});
		});	
		$(document).on('change','#discounted_offer',function() {
			var price = parseInt($("#original_price").val());
			var discount = parseInt($(this).val());
			var offer_type = $(".offer_type:checked").val();
			var final_price = 0;
			if(offer_type === 'value_based_dollar') {
				final_price = price - discount;
			} else if(offer_type === 'value_based_per') {
				final_price = price - (price * (discount/100));
			}
			$("#discounted_price").val(final_price);
		});

		$(document).on('change','#children_discounted_offer',function() {
			var price = parseInt($("#original_price").val());
			var discount = parseInt($(this).val());
			var offer_type = $(".offer_type:checked").val();
			var final_price = 0;
			if(offer_type === 'value_based_dollar') {
				final_price = price - discount;
			} else if(offer_type === 'value_based_per') {
				final_price = price - (price * (discount/100));
			}
			$("#children_discounted_price").val(final_price);
		});


		$(document).on('submit','#special-offer-form',function(evt) {
			evt.preventDefault();
      $('.text-danger').remove();
			var url = $(this).attr('action');
			var postData = $(this).serialize();
			var form = $(this)[0];
			$.post(url,postData,function(out){
				if(out.result === 0) {
					for(var i in out.errors) {
            $('#'+i).parent('.form-group').find('.text-danger').remove();
						$('#'+i).parent('.form-group').append('<small class="text-danger error">'+out.errors[i]+'</small>');
					}
				}
				if(out.result === 1) {
					//$("#error_msg").append('<div class="small-12 small-centered columns success-box">'+out.msg+'</div>')
            window.location = "{{ route('special-offer.list') }}";
				}
			});
		});
	</script>
	
@stop