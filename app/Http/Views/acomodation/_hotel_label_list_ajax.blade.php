<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getSortData(this,'hc.name');">{{ trans('messages.label_name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'hc.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th class="text-center">{{ trans('messages.action_head') }} </th>
        </tr>
    </thead>
    <tbody class="hotel_list_ajax">
    @if(count($oHotelList) > 0)
        @foreach ($oHotelList as $aHotel)	
            <tr>
                <td>
                    <label class="radio-checkbox label_check" for="checkbox-<?php echo $aHotel->id;?>">
                        <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aHotel->id;?>" value="<?php echo $aHotel->id;?>">&nbsp;
                    </label>
                </td>
                <td>
                    <a href="#">
                        {{ $aHotel->name }}
                    </a>
                </td>
                <td class="text-center">
                    <div class="switch tiny switch_cls">
                        <a href="{{ route('acomodation.hotel-label-create',[ 'nIdLabel' => $aHotel->id  ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                        <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('acomodation.label-delete',['nIdLabel'=> $aHotel->id]) }}','{{ trans('messages.delete_label')}}')">
                    </div>
                </td>
            </tr> 
        @endforeach
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oHotelList->count() , 'total'=>$oHotelList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oHotelList->lastPage() }},
            currentPage: {{ $oHotelList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('acomodation/hotel-label-list?page='+pageNumber),event,'table_record');
                /*if(pageNumber > 1)
                    callHotelListing(event,'hotel_list_ajax',pageNumber);
                else
                    callHotelListing(event,'table_record',pageNumber);*/
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>