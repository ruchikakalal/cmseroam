@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if(isset($oSupplier))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Accomodation Supplier']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Accomodation Supplier']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    <?php //print_r($oSupplier);exit; ?>
    @if(isset($oSupplier))
    {{ Form::model($oSupplier, array('url' => route('acomodation.hotel-supplier-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id' =>'create')) }}
    @else
    {{Form::open(array('url' => 'acomodation/hotel-supplier-create','method'=>'Post','enctype'=>'multipart/form-data' ,'id' =>'create')) }}
    @endif
    <div class="box-wrapper">
        <p>Accommodation Supplier Details</p>
        <div class="form-group m-t-30">
            <label class="label-control">Supplier Name <span class="required">*</span></label>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Supplier Name'])}}
        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif

        <div class="form-group m-t-30">
            <label class="label-control">eRoam Code <span class="required">*</span></label>
            {{Form::text('abbreviation',Input::old('abbreviation'),['id'=>'abbreviation','class'=>'form-control','placeholder'=>'Enter eRoam Code'])}}
        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Supplier Account Type</label>
            <select class="form-control">
                <option>Custom Account (Manual Upload)</option>
            </select>
        </div>
        
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Supplier Login</label>
                    <select class="form-control">
                        <option>CASAdmin</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Supplier Password</label>
                    <input type="Password" class="form-control" value="******" disabled>
                </div>
            </div>
        </div>
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Supplier Commission <span class="required">*</span></label>
                    {{Form::number('percentage',Input::old('percentage'),['id'=>'percentage','class'=>'form-control', 'step' => '0.01', 'min' => '0','placeholder'=>'Enter Supplier Commission'])}}
                </div>
                @if ( $errors->first( 'percentage' ) )
                <small class="error">{{ $errors->first('percentage') }}</small>
                @endif
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Supplier Status</label>
                    <select class="form-control">
                        <option>Pending</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="box-wrapper">
        <p>Accommodation Supplier Description</p>
        <div class="editor-block">
            {{ Form::textarea('description',Input::old('description'), ['placeholder' => 'Description', 'rows' => '5','id'=>'description','class'=>'form-control']) }}
        </div>
    </div>

    <div class="box-wrapper">
        <p>Accommodation Supplier Product / Marketing Contact</p>
        <div class="form-group m-t-30">
            <label class="label-control">Contact Name</label>
            {{Form::text('product_contact_name',Input::old('product_contact_name'),['id'=>'product_contact_name','class'=>'form-control','placeholder'=>'Enter Contact Name'])}}
        </div>
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Email Address</label>
                    {{Form::text('product_contact_email',Input::old('product_contact_email'),['id'=>'product_contact_email','class'=>'form-control','placeholder'=>'Enter Email Address'])}}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Contact Number <span class="required">*</span></label>
                    {{Form::text('product_contact_phone',Input::old('product_contact_phone'),['id'=>'product_contact_phone','class'=>'form-control','placeholder'=>'Enter Contact Number'])}}
                </div>
                @if ( $errors->first( 'product_contact_phone' ) )
                    <small class="error">{{ $errors->first('product_contact_phone') }}</small>
                @endif
            </div>
        </div>
    </div>

    <div class="box-wrapper">
        <p>Accommodation Supplier Reservation Contact</p>
        <div class="form-group m-t-30">
            <label class="label-control">Contact Name</label>
            {{Form::text('reservation_contact_name',Input::old('reservation_contact_name'),['id'=>'reservation_contact_name','class'=>'form-control','placeholder'=>'Enter Contact Name'])}}
            @if ( $errors->first( 'reservation_contact_name' ) )
                <small class="error">{{ $errors->first('reservation_contact_name') }}</small>
            @endif
        </div>
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Email Address</label>
                    {{Form::text('reservation_contact_email',Input::old('reservation_contact_email'),['id'=>'reservation_contact_email','class'=>'form-control','placeholder'=>'Enter Email Address'])}}
                </div>
                @if ( $errors->first( 'reservation_contact_email' ) )
                    <small class="error">{{ $errors->first('reservation_contact_email') }}</small>
                @endif
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Contact Number <span class="required">*</span></label>
                    {{Form::text('reservation_contact_free_phone',Input::old('reservation_contact_free_phone'),['id'=>'reservation_contact_free_phone','class'=>'form-control','placeholder'=>'Enter Contact Number'])}}
                </div>
                @if ( $errors->first( 'reservation_contact_free_phone' ) )
                    <small class="error">{{ $errors->first('reservation_contact_free_phone') }}</small>
                @endif
            </div>
        </div>
    </div>

    <div class="box-wrapper">
        <p>Accommodation Supplier Accounts Contact</p>
        <div class="form-group m-t-30">
            <label class="label-control">Contact Name</label>
            {{Form::text('accounts_contact_name',Input::old('accounts_contact_name'),['id'=>'accounts_contact_name','class'=>'form-control','placeholder'=>'Enter Contact Name'])}}
        </div>
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Email Address</label>
                    {{Form::text('accounts_contact_email',Input::old('accounts_contact_email'),['id'=>'accounts_contact_email','class'=>'form-control','placeholder'=>'Enter Email Address'])}}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Contact Number <span class="required">*</span></label>
                    <?php
                    $attributes = 'form-control';
                    ?>
                    {{Form::text('accounts_contact_phone',Input::old('accounts_contact_phone'),['id'=>'accounts_contact_phone','class'=>'form-control','placeholder'=>'Enter Contact Number'])}}
                </div>
                @if ( $errors->first( 'accounts_contact_phone' ) )
                    <small class="error">{{ $errors->first('accounts_contact_phone') }}</small>
                @endif
            </div>
        </div>
    </div>

    <div class="box-wrapper">
        <p>Agent Notes (Internal Only)</p>
        <div class="editor-block">
            {{ Form::textarea('special_notes',Input::old('special_notes'), ['rows' => '5','id'=>'special_notes','class'=>'form-control']) }}
        </div>
    </div>

    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="col-sm-6">
            {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
        </div>
        <div class="col-sm-6">
            <a href="{{ route('acomodation.hotel-supplier-list') }}" class="btn btn-primary btn-block">Cancel</a>
        </div>
    </div>
    {{ Form::hidden('supplier_id', $nIdSupplier) }}
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>    
    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }

    .success_message{
        color:green !important;
        text-align: center;
    }
</style>
@stop

@section('custom-js')
<script>
    tinymce.init({
        selector: '#description',
        height: 200,
        menubar: false
    });

$(function() {
    $( "#create" ).validate({
        rules: {
            name: 'required',
            abbreviation: 'required',
            percentage: 'required',
            product_contact_phone: 'required',
            reservation_contact_free_phone: 'required',
            accounts_contact_phone: 'required',
        },
        errorPlacement: function(error, element) {
            var placement = $(element).parent();
            if (placement) {
              $(error).insertAfter(placement)
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
                form.submit();
            }
    });
});
</script>
@stop
