@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }

    .address{
        margin-left: 45px;
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
@stop
@section('content')

<div class="content-container" >
    <h1 class="page-title">Payment Summery</h1> 

    @include('WebView::booking.review_booking_menu')
	<form action="{{route('booking.payment',$nBookId)}}" method="post">
	  @csrf
		 <input type="hidden" name="order_id" value="{{$nBookId}}">
		 
		  <div class="col-xs-6 form-group">
			<label for="deposit_money">Deposit Money:</label>
			<input type="text" required class="form-control" name="deposit_money" id="deposit_money">
		  </div>
			<button style="margin-top:15px;margin-left:10px" type="submit" class="btn btn-default">Submit</button>
	</form> 
	<br/>
	<br/>
	@if(Session::has('error_msg'))
	<div class="alert alert-danger">
		{{ Session::get('error_msg') }}
	</div>
	@endif
	@if(Session::has('succ_msg'))
	<div class="alert alert-success">
		{{ Session::get('succ_msg') }}
	</div>
	@endif
	<br/>
  
	<table class="table">
		<thead>
		<tr>
			<th></th>
			<th>Total Amount</th>
			<th>Deposit</th>
			<th>Pending</th>
			<th>Date</th>
		</tr>
		</thead>
		<tbody>
		@if(isset($order_payment) && $order_payment->count() > 0)
			@foreach($order_payment as $key=>$value)
		<tr>
			<td></td>
			<td>{{number_format((float)$value->total, 2, '.', '')}}</td>
			<td>{{number_format((float)$value->deposit, 2, '.', '')}}</td>
			<td>{{number_format((float)$value->pending, 2, '.', '')}}</td>
			<td>{{$value->created_at}}</td>
		</tr>
			@endforeach
		@endif
		</tbody>
	</table>

    <div class="col-sm-offset-2 col-sm-8">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="row">
                     <a href="{{ route('booking.booking-voucher-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Previous</a>
                </div>
            </div>
        </div>
    </div>
    @stop