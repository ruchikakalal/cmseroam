<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getTourSort(this,'id');">{{ trans('messages.booking_id') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'id')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'first_name');">{{ trans('messages.customer_name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'first_name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'i.created_at');">{{ trans('messages.created_date') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.created_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'i.from_date');">{{ trans('messages.departure_date') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.from_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'i.to_date');">{{ trans('messages.return_date') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.to_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'i.created_at');">{{ trans('messages.booking_type') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.created_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            <th>{{ trans('messages.agent_consultant') }}</th>
            </th>
            <th onclick="getTourSort(this,'status');">{{ trans('messages.booking_status') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'status')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">{{ trans('messages.thead_action') }}</th>
        </tr>
    </thead>
    <tbody class="tour_list_ajax">
        @include('WebView::booking._more_active_list')
        
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oItineraries->count() , 'total'=>$oItineraries->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>


<script type="text/javascript">
$(function() {
    $('.pagination').pagination({
        pages: {{ $oItineraries->lastPage() }},
        itemsOnPage: 10,
        currentPage: {{ $oItineraries->currentPage() }},
        displayedPages:2,
        edges:1,
        onPageClick(pageNumber, event){
		getBookingPaginationListing(siteUrl('booking/active/?page='+pageNumber),event,'table_record');
		
//            if(pageNumber > 1)
//                getMoreListing(siteUrl('booking/itenary-list?page='+pageNumber),event,'tour_list_ajax');
//            else
//                getMoreListing(siteUrl('booking/itenary-list?page='+pageNumber),event,'table_record');
            $('#checkbox-00').prop('checked',false);
            setupLabel();
        }
    });
});
</script>