@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if($nIdMarkup != '')
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Activity Markup']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Activity Markup']) }}</h1>
    @endif

    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="small-6 small-centered columns error-box">{{$errors->first()}}</div>
        @endif
    </div>
    <br>
    <div class="box-wrapper">

        <p>{{ trans('messages.activity_markup_info')}}</p>
        @if($nIdMarkup == '')
            {{Form::open(array('url' => 'activity/create-activity-markup','method'=>'Post')) }}
        @else
            {{ Form::model($oActivityMarkup, array('url' => route('activity.create-activity-markup') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.name')}} <span class="required">*</span></label>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=> 'form-control'])}}
        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_description')}} <span class="required">*</span></label>
            {{Form::textarea('description',Input::old('description'),['id'=>'description','class'=> 'form-control','rows'=>"3"])}}
            <!--<textarea name="description" id="description" class="form-control" rows="3">{{Input::old('description')}}</textarea>-->
        </div>
        @if ( $errors->first( 'description' ) )
        <small class="error">{{ $errors->first('description') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.allocation_type')}} <span class="required">*</span></label>
            <select name="allocation_type" id="allocation-type" class="form-control">
                <option selected disabled>Choose</option>
                <option value="all" {{ (isset($oActivityMarkup) && $oActivityMarkup['allocation_type'] == 'all') ? 'selected = "selected"' : '' }}>All</option>
                <option value="activity" {{ (isset($oActivityMarkup) && $oActivityMarkup['allocation_type'] == 'activity') ? 'selected = "selected"' : '' }}>Activity</option>
                <option value="city" {{ (isset($oActivityMarkup) && $oActivityMarkup['allocation_type'] == 'city') ? 'selected = "selected"' : '' }}>City</option>
                <option value="country" {{ (isset($oActivityMarkup) && $oActivityMarkup['allocation_type'] == 'country') ? 'selected = "selected"' : '' }}>Country</option>
            </select>
        </div>
        @if ( $errors->first( 'allocation_type' ) )
        <small class="error">{{ $errors->first('allocation_type') }}</small>
        @endif
        <div class="form-group m-t-30" id="allocation-id-container">
            <label class="label-control" id="allocation-id-label"></label>
            <select id="allocation-id" name="allocation_id" class='form-control' required disabled></select>
        </div>
        <input type="hidden" value="{{ $nIdMarkup }}" name="id_activity" >
        <input type="hidden" value="{{ (isset($oActivityMarkup) && isset($$oActivityMarkup['allocation_id']) ) ? $oActivityMarkup['allocation_id'] : '' }}" id="allocation">

        <div class="row">	
            <div class="form-group col-md-10">
                <label class="label-control">{{ trans('messages.markup_percentage')}} <span class="required">*</span></label>
                {{Form::text('markup_percentage',Input::old('markup_percentage'),['id'=>'markup-percentage','class'=> 'form-control','step'=>"0.01", 'min'=>"0", 'type'=>"number"])}}
                <!--<input type="number" id="markup-percentage" step="0.01" min="0" class='form-control' value="{{Input::old('markup_percentage')}}" name="markup_percentage"/>-->
            </div>
            <div class="form-group col-md-2">
                <label class="label-control"></label>
                <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                      title="The markup % field is the percentage increase on the original selling price of the product.">
                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        @if ( $errors->first( 'markup_percentage' ) )
        <small class="error">{{ $errors->first('markup_percentage') }}</small>
        @endif
        <div class="row">	
            <div class="form-group col-md-10">
                <label class="label-control">{{ trans('messages.supplier_commission_percentage') }} <span class="required">*</span></label>
                {{Form::text('supplier_percentage',Input::old('supplier_percentage'),['id'=>'supplier-percentage','class'=> 'form-control','step'=>"0.01", 'min'=>"0", 'type'=>"number"])}}
                <!--<input type="number" id="supplier-percentage" step="0.01" min="0" class='form-control' value="{{Input::old('supplier_percentage')}}" name="supplier_percentage"/>-->
            </div>
            <div class="form-group col-md-2">
                <label class="label-control"></label>
                <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                      title="The agent % field is the percentage increase on the original selling price of the product.">
                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        @if ( $errors->first( 'supplier_percentage' ) )
        <small class="error">{{ $errors->first('supplier_percentage') }}</small>
        @endif
        <div class="row">
            <div class="form-group col-md-10">
                <label class="label-control">{{ trans('messages.agent_commission_percentage') }} <span class="required">*</span></label>
                {{Form::text('agent_percentage',Input::old('agent_percentage'),['id'=>'agent-percentage','class'=> 'form-control','step'=>"0.01", 'min'=>"0", 'type'=>"number"])}}
                <!--<input type="number" id="agent-percentage" step="0.01" min="0" class="form-control" value="{{Input::old('agent_percentage')}}" name="agent_percentage"/>-->
            </div>
            <div class="form-group col-md-2">
                <label class="label-control"></label>
                <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                      title="The agent % field is the percentage increase on the original selling price of the product.">
                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                </span>
            </div>
        </div>
        @if ( $errors->first( 'agent_percentage' ) )
        <small class="error">{{ $errors->first('agent_percentage') }}</small>
        @endif
        <div class="row">	
            <div class="form-group col-md-10">
                <label for="allocation-type" class="label-control">{{ trans('messages.status') }}</label>
                {{ Form::checkbox('is_active', 1, Input::old('is_active'), ['class' => 'show-on-eroam-btn switch1-state1']) }}
                <!--<input id="is-active" type="checkbox" name="is_active" class="show-on-eroam-btn switch1-state1"/>-->
                <label for="is-active"></label>
            </div>
            <div class="form-group col-md-2">
                <label class="label-control"></label>
                <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                      title="Turn the switch on to activate this markup on all related products after creation.">
                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                </span>
            </div>	
        </div>
        <div class="row" id="is-default-container" style="{{ (isset($oActivityMarkup) && $oActivityMarkup['allocation_type'] != 'all') ? 'display:none' : '' }}">	
            <div class="form-group col-md-10">
                <label for="allocation-type" class="label-control">{{ trans('messages.set_as_default') }}</label>
                {{ Form::checkbox('is_default', 1, Input::old('is_default'), ['class' => 'show-on-eroam-btn switch1-state2','id'=>"is-default"]) }}
                <!--<input id="is-default" type="checkbox" name="is_default" class="show-on-eroam-btn switch1-state2"/>-->
                <label for="is-default"></label>
            </div>	
            <div class="form-group col-md-2">
                <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="height: 34px;vertical-align: sub;"
                      title="Turn the switch on to set this markup as the default markup for hotels with no set markup.">
                    <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                </span>
            </div>	
        </div>
    </div>  

    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('activity.activity-markup-list')}}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
            </div>
        </div>
    </div>	
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>
    .error{
        color:red !important;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    #allocation-id-container{
        display:none;
    }
</style>
@stop

@section('custom-js')
<script>
    $('.switch1-state1').bootstrapSwitch();
    $('.switch1-state2').bootstrapSwitch();
    $('.alert').click(function () {
        var c = confirm("Are you sure you want to delete this record?");
        return c;
    });


    $('select#allocation-type').on('change', function () {
        var values = [];
        var html = '';
        switch ($(this).val()) {
            case 'all':
                $('div#is-default-container').slideDown(200);
                if ($('div#allocation-id-container').is(":visible")) {
                    $('div#allocation-id-container').slideUp({duration: 200, complete: function () {
                            $('select#allocation-id').attr('disabled', 'disabled');
                        }});
                }
                break;

            case 'activity':
                $('div#is-default-container').slideUp(200);
                $('input#is-default').prop('checked', false);
                $.ajax({method: "get", url: siteUrl('activity/all-activity-list'), data: {_token: "<?php echo csrf_token(); ?>"},
                    success: function (response) {
                        console.log(response);
                        values = response;
                        $('label#allocation-id-label').html('Activity');
                        html = '<option selected disabled>Choose a Activity</option>';
                        buildAllocationIdSelect(values, html);
                        if (!$('div#allocation-id-container').is(":visible")) {
                            $('select#allocation-id').removeAttr("disabled");
                            $('div#allocation-id-container').slideDown(200);
                        }
                    },
                    error: function (xhr) {
                        console.log(xhr)
                    }
                });
                break;

            case 'city':
                $('div#is-default-container').slideUp(200);
                $('input#is-default').prop('checked', false);
                $.ajax({method: "get", url: siteUrl('common/all-city-list'), data: {_token: "<?php echo csrf_token(); ?>"},
                    success: function (response) {
                        console.log(response);
                        values = response;
                        $('label#allocation-id-label').html('City');
                        html = '<option selected disabled>Choose a City</option>';
                        buildAllocationIdSelect(values, html);
                        if (!$('div#allocation-id-container').is(":visible")) {
                            $('select#allocation-id').removeAttr("disabled");
                            $('div#allocation-id-container').slideDown(200);
                        }
                    },
                    error: function (xhr) {
                        console.log(xhr)
                    }
                });
                break;

            case 'country':
                $('div#is-default-container').slideUp(200);
                $('input#is-default').prop('checked', false);
                $.ajax({method: "get", url: siteUrl('/common/all-country-list'), data: {_token: "<?php echo csrf_token(); ?>"},
                    success: function (response) {
                        console.log(response);
                        values = response;
                        $('label#allocation-id-label').html('Country');
                        html = '<option selected disabled>Choose a Country</option>';
                        buildAllocationIdSelect(values, html);
                        if (!$('div#allocation-id-container').is(":visible")) {
                            $('select#allocation-id').removeAttr("disabled");
                            $('div#allocation-id-container').slideDown(200);
                        }
                    },
                    error: function (xhr) {
                        console.log(xhr)
                    }
                });
                break;

        }
    });

    // function to create the options for the select#allocation-id input field;
    function buildAllocationIdSelect(values, html) {
     var selected = $('#allocation').val();
        if (values) {
            //console.log(values);
            $.each(values, function (index, value) {
                var select='';
                if(value.id == selected)
                    var select = "selected";
                html = html + '<option value="' + value.id + '" '+select+'>' + value.name + '</option>';
            });
            $('select#allocation-id').html(html);
        }
    }

    // function to confirm if user wants to set the markup as the default markup
    $('input#is-default').on('click', function (e) {
        var msg = '';
        if ($("input#is-active").is(':checked')) {

            if ($("input#is-default").is(':checked')) {

                e.preventDefault();
                msg = "Would you like to set this makrup as the default generic markup for all activities? By doing so, the existing default markup will no longer be used.";
                customConfirm('Set as Default ', msg, function () {
                    $('input#is-default').prop('checked', true);
                }, function () {
                    $('input#is-default').prop('checked', false);
                });
            }
        }
    });

    // function to show/hide the set as default field
    $('input#is-active').on('click', function (e) {
        var msg = '';
        if ($("input#is-active").is(':checked')) {
            if ($('select#allocation-type').val() == 'All') {
                $('div#is-default-container').slideDown(200);
            } else {
                $('div#is-default-container').slideUp(200);
            }
        } else {
            if ($('select#allocation-type').val() == 'All') {
                e.preventDefault();
                msg = "You've currently set this markup as the default. Changing the status to inactive will not make this the default markup anymore. Do you wish to continue?";
                customConfirm('Set as inactive', msg, function () {
                    $('input#is-default').prop('checked', false);
                    $('input#is-active').prop('checked', false);
                    $('div#is-default-container').slideUp(200);
                }, null);
            } else {
                $('input#is-default').prop('checked', false);
            }
        }
    });

</script>
@stop
