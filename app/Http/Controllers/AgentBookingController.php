<?php

//priya

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use PDF;
use Auth;
use Validator;
use View;
use App\UserDomain;
use App\Domain;
use App\User;
use App\Customer;
use App\ItenaryOrder;
use App\LegDetail;
use App\PassengerInformation;
use App\BookingRequest;
use App\ItenaryLeg;
use App\SavedTrips;
use App\AgeGroup;
use App\BookingAmount;
use App\Country;
use DB;
use Session;

class AgentBookingController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function createCustomer() {
        $agent_id = Auth::user()->id;
        if(session()->has('new_agent')) {
            $agent_id = session()->get('new_agent')['id'];
        }
        $agent_domains = UserDomain::where('user_id',$agent_id)->with('domains')->get();
        return view('WebView::agent.create-customer',compact('agent_domains'));
    }

    public function storeCustomer(Request $request) {
        $rules = [
            'name' => 'required',
            'family_name' => 'required',
            'gender' => 'required',
            'contact_no' => 'required',
            'email' => 'required|email',
            'contact_method' => 'required',
            'domain_id' => 'required',
            'account_id' => 'required|unique:users'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return ['result' => 0, 'errors' => $validator->messages()];
        }

        $login_user = Auth::user();
        $user = User::Create([
                    'account_id' => $request->account_id,
                    'username' => $request->email,
                    'name' => $request->name,
                    'family_name' => $request->family_name,
                    'password' => '',
                    'type' => 'customer',
                    'active' => 1,
                    'contact_no' => $request->contact_no,
                    'gender' => $request->gender,
                    'user_id' => (session()->has('new_agent'))? session()->get('new_agent')['id'] :Auth::user()->id,
                    'licensee_id' => Auth::user()->licensee_id,
                    'domain_id' => $request->domain_id,
                    'created_at' => date("Y-m-d H:i:s")
        ]);

        if($request->freq_used_products == ''){$request->freq_used_products = [];}
        if($request->seat_type == ''){$request->seat_type = [];}
        if($request->meal_type == ''){$request->meal_type = [];}
        if($request->interests == ''){$request->interests = [];}
        $name = explode(' ', $request->name);
        $customer = Customer::Create([
                    'first_name' => $name[0],
                    'last_name' => $name[1],
                    'user_id' => $user->id,
                    'is_confirmed' => 1,
                    'email' => $request->email,
                    'currency' => '',
                    'contact_no' => $request->contact_no,
                    'pref_contact_method' => $request->contact_method,
                    'freq_used_products' => implode(',', $request->freq_used_products),
                    'pref_hotel_categories' => $request->accommodation_type,
                    'pref_hotel_room_types' => $request->room_type,
                    'pref_transport_types' => $request->transport_type,
                    'pref_cabin_class' => $request->cabin_class,
                    'pref_seat_type' => implode(',', $request->seat_type),
                    'pref_meal_type' => implode(',', $request->meal_type),
                    'interests' => implode(',', $request->interests),
                    'additional_info' => $request->additional_info
        ]);

        return ['result' => 1, 'msg' => 'Customer created successfully', 'url' => route('agent.redirect-page', $user->id)];
    }

    public function redirectPage($user_id,$trip_id=null) {
        session()->put('redirect_user_id',$user_id);
        $agent = (session()->has('new_agent')) ? json_encode(session()->get('new_agent')) : json_encode(Auth::user());
        $user = User::where('id', $user_id)->first();
        $customer = Customer::where('user_id',$user_id)->first();
        if(isset($customer->pref_age_group_id)) {
            $age_group = AgeGroup::where('id',$customer->pref_age_group_id)->first();
        } else {
            $age_group['name'] = '';
        }
        $interests = array(
                      15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                      5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                      13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                      29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                      11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                      20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                    );
        $cust_pref = [];
        $pref = explode(',', $customer->interests);
        foreach ($pref as $value) {
            $cust_pref[] = $interests[$value];
        }
        $prefrences[] = ['cabin_class' => $customer->pref_cabin_class,
                         'accommodation' => [$customer->pref_hotel_categories],
                         'room' => [$customer->pref_hotel_room_types],
                         'transport' => [$customer->pref_transport_types],
                         'age_group' => $age_group['name'],
                         'nationality' => $customer->pref_nationality_id,
                         'gender' => $customer->gender,
                         'interestLists' => implode(',', $cust_pref),
                         'interestListIds' => $pref];
        $prefrences = json_encode($prefrences);

        $saved_trip = [];
        if(!empty($trip_id)) {
            $saved_trip = SavedTrips::where('id',$trip_id)->first();
        }
        $domain = Domain::where('id',$user['domain_id'])->first()['name'];
        return view('WebView::agent.redirect-page', compact('agent', 'user','domain','saved_trip','customer','prefrences'));
    }

    public function listCustomers() {
        return view('WebView::agent.list-customers');
    }

    public function list() {
        $user_id = Auth::user()->id;
        if(session()->has('new_agent')) {
            $user_id = session()->get('new_agent')['id'];
        }
        $licensee_id = Auth::user()->licensee_id;
        $domain_id = session()->get('domain_id');
        $where = ['licensee_id' => $licensee_id, 'type' => 'customer','domain_id'=> $domain_id,'user_id'=>$user_id];
        if (request()->search_by && request()->search_text) {
            if (request()->search_by == 'name') {
                $customers = User::where($where)->where(request()->search_by, 'like', '%' . request()->search_text . '%')->get();
            } else {
                $customers = User::where($where)->where(request()->search_by, request()->search_text)->get();
            }
        } else {
            $customers = User::where($where)->get();
        }

        $view = View::make('WebView::agent._list-customer', ['customers' => $customers]);
        $content = $view->render();

        return ['content' => $content];
    }
	
	public function pendingBooking(Request $oRequest)
    {
		$login_user = Auth::user();
        if(session()->has('new_agent')) {
            $login_user = session()->get('new_agent');
        }
		$domain_id = $login_user['domain_id'];
		$licensee_id = $login_user['licensee_id'];
        if (isset($oRequest->type)) {
            $type = $oRequest->type;

            if ($type == 'all') {
                $agent_id = '';
            } else {
                $agent_id = $login_user['id'];
            }
        } else {
            $type = 'personal';
            $agent_id = $login_user['id'];
        }
        //remove session when it comes from sidebar
        if (session('heading_name') != 'itenary_booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('itenary_booking');

			session(['page_name' => 'itenary_booking']);
			$aData = session('itenary_booking') ? session('itenary_booking') : array();

			$oRequest->session()->forget('itenary_booking'); 
		
			if (session('page_name') != 'pending_booking' || $oRequest->query('isreset') == 1)
				$oRequest->session()->forget('pending_booking');

			session(['page_name' => 'pending_booking']);
			$aData = session('pending_booking') ? session('pending_booking') : array();

			$oRequest->session()->forget('pending_booking');
        
			$nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
			$sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
			$sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
			$sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
			$nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
			if (count($aData) && $sSearchStr != $aData['search_str'])
				$nPage = 1;

			Paginator::currentPageResolver(function () use ($nPage) {
				return $nPage;
			});
        $oItineraries = SavedTrips::saveItenaryAgentWise($sSearchStr, $sOrderField, $sOrderBy, $nShowRecord,$agent_id, $type,$domain_id);
        //echo "<pre>";print_r($oItineraries);exit;
        setSession($sSearchStr, '', $sOrderField, $sOrderBy, $nShowRecord, $oItineraries->currentPage(), 'itenary_booking');

        if ($oRequest->page > 1)
            $oViewName = 'WebView::booking._booking_pending_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::booking.pending_list' : 'WebView::booking._booking_pending_ajax';

        return \View::make($oViewName, compact('oItineraries', 'sSearchStr', 'sOrderField', 'sOrderBy', 'nShowRecord', 'status', 'type'));
    }
	
	public function activeBooking(Request $oRequest)
    {
		$login_user = Auth::user();
        if(session()->has('new_agent')) {
            $login_user = session()->get('new_agent');
        }
		$domain_id = $login_user['domain_id'];
		$licensee_id = $login_user['licensee_id'];
        if (isset($oRequest->type)) {
            $type = $oRequest->type;

            if ($type == 'all') {
                $agent_id = '';
            } else {
                $agent_id = $login_user['id'];
            }
        } else {
            $type = 'personal';
            $agent_id = $login_user['id'];
        }
        //remove session when it comes from sidebar
        if (session('heading_name') != 'itenary_booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('itenary_booking');

			session(['page_name' => 'itenary_booking']);
			$aData = session('itenary_booking') ? session('itenary_booking') : array();

			$oRequest->session()->forget('itenary_booking'); 
		
			if (session('page_name') != 'active_booking' || $oRequest->query('isreset') == 1)
				$oRequest->session()->forget('active_booking');

			session(['page_name' => 'active_booking']);
			$aData = session('active_booking') ? session('active_booking') : array();

			$oRequest->session()->forget('active_booking');
        
			$nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
			$sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
			$sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
			$sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
			$nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
			if (count($aData) && $sSearchStr != $aData['search_str'])
				$nPage = 1;

			Paginator::currentPageResolver(function () use ($nPage) {
				return $nPage;
			});
        $oItineraries = ItenaryOrder::activeBooking($sSearchStr, $sOrderField, $sOrderBy, $nShowRecord,$agent_id, $type,$domain_id);
        //echo "<pre>";print_r($oItineraries);exit;
        setSession($sSearchStr, '', $sOrderField, $sOrderBy, $nShowRecord, $oItineraries->currentPage(), 'itenary_booking');

        if ($oRequest->page > 1)
            $oViewName = 'WebView::booking._booking_active_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::booking.active_list' : 'WebView::booking._booking_active_ajax';

        return \View::make($oViewName, compact('oItineraries', 'sSearchStr', 'sOrderField', 'sOrderBy', 'nShowRecord', 'status', 'type'));
    }
	
	
	public function archivedBooking(Request $oRequest)
    {
		$login_user = Auth::user();
        if(session()->has('new_agent')) {
            $login_user = session()->get('new_agent');
        }
		$domain_id = $login_user['domain_id'];
		$licensee_id = $login_user['licensee_id'];
        if (isset($oRequest->type)) {
            $type = $oRequest->type;

            if ($type == 'all') {
                $agent_id = '';
            } else {
                $agent_id = $login_user['id'];
            }
        } else {
            $type = 'personal';
            $agent_id = $login_user['id'];
        }
        //remove session when it comes from sidebar
        if (session('heading_name') != 'itenary_booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('itenary_booking');

			session(['page_name' => 'itenary_booking']);
			$aData = session('itenary_booking') ? session('itenary_booking') : array();

			$oRequest->session()->forget('itenary_booking'); 
		
			if (session('page_name') != 'archived_booking' || $oRequest->query('isreset') == 1)
				$oRequest->session()->forget('archived_booking');

			session(['page_name' => 'archived_booking']);
			$aData = session('archived_booking') ? session('archived_booking') : array();

			$oRequest->session()->forget('archived_booking');
        
			$nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
			$sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
			$sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
			$sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
			$nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
			if (count($aData) && $sSearchStr != $aData['search_str'])
				$nPage = 1;

			Paginator::currentPageResolver(function () use ($nPage) {
				return $nPage;
			});
        $oItineraries = ItenaryOrder::archivedBooking($sSearchStr, $sOrderField, $sOrderBy, $nShowRecord,$agent_id, $type,$domain_id);
        //echo "<pre>";print_r($oItineraries);exit;
        setSession($sSearchStr, '', $sOrderField, $sOrderBy, $nShowRecord, $oItineraries->currentPage(), 'itenary_booking');

        if ($oRequest->page > 1)
            $oViewName = 'WebView::booking._booking_archived_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::booking.archived_list' : 'WebView::booking._booking_archived_ajax';

        return \View::make($oViewName, compact('oItineraries', 'sSearchStr', 'sOrderField', 'sOrderBy', 'nShowRecord', 'status', 'type'));
    }
	

    public function statusWiseBooking($status, Request $oRequest) {
        $login_user = Auth::user();
        if(session()->has('new_agent')) {
            $login_user = session()->get('new_agent');
        }
		$domain_id = $login_user['domain_id'];
		$licensee_id = $login_user['licensee_id'];
        $status = ucfirst($status);
        if (isset($oRequest->type)) {
            $type = $oRequest->type;

            if ($type == 'all') {
                $agent_id = '';
            } else {
                $agent_id = $login_user['id'];
            }
        } else {
            $type = 'personal';
            $agent_id = $login_user['id'];
        }
		
		if($status == "Pending"){
			
			if (session('heading_name') != 'itenary_booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('itenary_booking');

			session(['page_name' => 'itenary_booking']);
			$aData = session('itenary_booking') ? session('itenary_booking') : array();

			$oRequest->session()->forget('itenary_booking'); 
		
			if (session('page_name') != 'pending_booking' || $oRequest->query('isreset') == 1)
				$oRequest->session()->forget('pending_booking');

			session(['page_name' => 'pending_booking']);
			$aData = session('pending_booking') ? session('pending_booking') : array();

			$oRequest->session()->forget('pending_booking');
		}elseif($status == "Active"){
			if (session('heading_name') != 'itenary_booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('itenary_booking');

			session(['page_name' => 'itenary_booking']);
			$aData = session('itenary_booking') ? session('itenary_booking') : array();

			$oRequest->session()->forget('itenary_booking'); 
			if (session('page_name') != 'active_booking' || $oRequest->query('isreset') == 1)
				$oRequest->session()->forget('active_booking');

			session(['page_name' => 'active_booking']);
			$aData = session('active_booking') ? session('active_booking') : array();

			$oRequest->session()->forget('active_booking');
			
		}else{
			if (session('heading_name') != 'itenary_booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('itenary_booking');

			session(['page_name' => 'itenary_booking']);
			$aData = session('itenary_booking') ? session('itenary_booking') : array();

			$oRequest->session()->forget('itenary_booking'); 
			if (session('page_name') != 'archived_booking' || $oRequest->query('isreset') == 1)
				$oRequest->session()->forget('archived_booking');

			session(['page_name' => 'archived_booking']);
			$aData = session('archived_booking') ? session('archived_booking') : array();

			$oRequest->session()->forget('archived_booking');
		}


        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'order_id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if (count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;

        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        $oItineraries = ItenaryOrder::getItenaryListByStatus($sSearchStr, $sOrderField, $sOrderBy, $nShowRecord, $status, $agent_id, $type,$domain_id);
        setSession($sSearchStr, '', $sOrderField, $sOrderBy, $nShowRecord, $oItineraries->currentPage(), 'itenary_booking');

        if ($oRequest->page > 1)
            $oViewName = 'WebView::booking._booking_listing_status_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::booking.booking_listing' : 'WebView::booking._booking_listing_status_ajax';

        return \View::make($oViewName, compact('oItineraries', 'sSearchStr', 'sOrderField', 'sOrderBy', 'nShowRecord', 'status', 'type'));
    }
	
	public function tourBooking(Request $oRequest) {
		$login_user = Auth::user();
        if(session()->has('new_agent')) {
            $login_user = session()->get('new_agent');
        }
		$domain_id = $login_user['domain_id'];
		$licensee_id = $login_user['licensee_id'];
       
        if (isset($oRequest->type)) {
            $type = $oRequest->type;

            if ($type == 'all') {
                $agent_id = '';
            } else {
                $agent_id = $login_user['id'];
            }
        } else {
            $type = 'personal';
            $agent_id = $login_user['id'];
        }
		
		if (session('heading_name') != 'tour-booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('tour-booking');

			session(['page_name' => 'tour-booking']);
			$aData = session('tour-booking') ? session('tour-booking') : array();

			$oRequest->session()->forget('tour-booking'); 
	
		if (session('page_name') != 'all_tour_booking' || $oRequest->query('isreset') == 1)
			$oRequest->session()->forget('all_tour_booking');

			session(['page_name' => 'all_tour_booking']);
			$aData = session('all_tour_booking') ? session('all_tour_booking') : array();

			$oRequest->session()->forget('all_tour_booking');
		$nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'request_id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        $oBookingRequest = BookingRequest::getTourBookingByDomain($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord,$agent_id, $type,$domain_id);
		//pr($oBookingRequest);die;
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oBookingRequest->currentPage(),'all_tour_booking');

        if($oRequest->page > 1)
            $oViewName =  'WebView::booking._tour_status_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::booking.tour_status_list' : 'WebView::booking._tour_status_list_ajax';
        return \View::make($oViewName, compact('oBookingRequest', 'sSearchStr', 'sOrderField', 'sOrderBy','sSearchBy', 'nShowRecord','type'));
		
       // return \View::make($oViewName, compact('oTourList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
	
	public function tourPaxDetail($request_id) {
		$page_name= 'tour_pax_details';
		$oleadPaxDetials = BookingRequest::from('tblbookingrequests as b')
                                ->where('b.request_id',$request_id)
                                ->first();
								
		$oAdditionalPaxDetials = BookingRequest::from('tblbookingrequests as b')
                                ->select('b.request_id','b.tour_id','b.tour_title','b.Title','b.user_id','b.FName','b.SurName','b.gender','b.DOB','b.email','b.address','b.nationality','b.contact','b.notes','ap.type','ap.id as person_id','ap.type','ap.title as person_title','ap.first_name','ap.family_name','ap.email as additional_email','ap.contact as additional_contact')
                                ->join('tblbookingrequestsadditionalpersons as ap', 'ap.request_id', '=', 'b.request_id')
								->where('b.request_id',$request_id)
                                ->get();
								
		return \View::make('WebView::booking.tour_pax_detail', compact('page_name','request_id','oAdditionalPaxDetials','oleadPaxDetials'));
	}
	
	public function tourSupplierDetail($request_id){
        $page_name= 'tour_supplier_details';
        $getRquestDetails = BookingRequest::where('request_id',$request_id)->with('getProviders')->first();
        if(empty($getRquestDetails)){
            return back()->with('error', 'No data found.');
        }
        $getRquestDetails = $getRquestDetails->toArray();
		return \View::make('WebView::booking.tour_supplier_details', compact('page_name','request_id','getRquestDetails'));
    }
	
	public function tourProductDetail($request_id) {
		$page_name= 'tour_product_details';
		$productDetials = BookingRequest::from('tblbookingrequests as b')
                                ->select('b.request_id','b.tour_id','b.tour_title','b.FName','b.SurName','b.gender','b.DOB','b.email','b.address','b.nationality','b.contact','b.notes','b.tour_notes','b.provider','bkc.reseller_cost', 'bkc.licensee_benefit', 'bkc.licensee_rrp as sellprice','t.tour_title','t.no_of_days','durationType','t.short_description')
                                ->join('booking_commissions as bkc', 'b.request_id', '=', 'bkc.booking_id')
								->join('tbltours as t', 't.tour_id', '=', 'b.tour_id')
								->where('b.request_id',$request_id)
                                ->first();
        return \View::make('WebView::booking.tour_product_details', compact('page_name','request_id','productDetials'));
	}
		
	public function reviewBooking($nBookId) {
        $page_name = 'pax_details';
        $oItinerary = ItenaryOrder::with('Passenger')
                ->select('*')
                ->where('order_id', $nBookId)
                ->get();
				//pr($oItinerary[0]['Passenger']);die;
		if(!empty($oItinerary)){
            foreach ($oItinerary[0]['Passenger'] as $passanger) {
				if ($passanger['is_lead'] == 'Yes') {
					$lead['passenger_information_id'] = $passanger['passenger_information_id'];
                    $lead['itenary_order_id'] = $passanger['itenary_order_id'];
                    $lead['first_name'] = $passanger['first_name'];
                    $lead['last_name'] = $passanger['last_name'];
                    $lead['email'] = $passanger['email'];
                    $lead['title'] = $passanger['title'] != '' ? $passanger['title'] : 'N/A';
                    $lead['gender'] = $passanger['gender'] != '' ? $passanger['gender'] : 'N/A';
                    $lead['country'] = $passanger['country'];
                    $lead['contact'] = $passanger['contact'] != '' ? $passanger['contact'] : 'N/A';
                    $lead['dob'] = $passanger['dob'];
                    $lead['passport_expiry_date'] = $passanger['passport_expiry_date'];
                    $lead['passport_num'] = $passanger['passport_num'];
                    $lead['address_one'] = $passanger['address_one'];
                    $lead['address_two'] = $passanger['address_two'] != '' ? $passanger['address_two'] : 'N/A';
                    $lead['suburb'] = $passanger['suburb'] != '' ? $passanger['suburb'] : 'N/A';
                    $lead['state'] = $passanger['state'] != '' ? $passanger['state'] : 'N/A';
                    $lead['zip'] = $passanger['zip'] != '' ? $passanger['zip'] : 'N/A';
                    $lead['is_lead'] = $passanger['is_lead'];
                    $lead['created_at'] = $passanger['created_at'];
                    $lead['updated_at'] = $passanger['updated_at'];
                    $lead['is_billing'] = $passanger['is_billing'];
                } else {
					$additional['passenger_information_id'] = $passanger['passenger_information_id'];
                    $additional['itenary_order_id'] = $passanger['itenary_order_id'];
                    $additional['first_name'] = $passanger['first_name'];
                    $additional['last_name'] = $passanger['last_name'];
                    $additional['email'] = $passanger['email'];
                    $additional['title'] = $passanger['title'] != '' ? $passanger['title'] : 'N/A';
                    $additional['gender'] = $passanger['gender'] != '' ? $passanger['gender'] : 'N/A';
                    $additional['country'] = $passanger['country'];
                    $additional['contact'] = $passanger['contact'] != '' ? $passanger['contact'] : 'N/A';
                    $additional['dob'] = $passanger['dob'];
                    $additional['passport_expiry_date'] = $passanger['passport_expiry_date'];
                    $additional['passport_num'] = $passanger['passport_num'];
                    $additional['address_one'] = $passanger['address_one'] != '' ? $passanger['address_one'] : 'N/A';
                    $additional['address_two'] = $passanger['address_two'] != '' ? $passanger['address_two'] : 'N/A';
                    $additional['suburb'] = $passanger['suburb'] != '' ? $passanger['suburb'] : 'N/A';
                    $additional['state'] = $passanger['state'] != '' ? $passanger['state'] : 'N/A';
                    $additional['zip'] = $passanger['zip'] != '' ? $passanger['zip'] : 'N/A';
                    $additional['is_lead'] = $passanger['is_lead'];
                    $additional['created_at'] = $passanger['created_at'];
                    $additional['updated_at'] = $passanger['updated_at'];
                    $additional['is_billing'] = $passanger['is_billing'];
                    $additional_arr[] = $additional;
        			
                }
            }
		}
        
        return \View::make('WebView::booking.booking_review_detail', compact('page_name', 'oItinerary', 'additional_arr', 'lead', 'nBookId'));
    }
	
	/* Booking Payment List */
	
	public function twoDecimalFloat($number)
	{
		return number_format((float)$number, 2, '.', '');
	}	
	
	public function bookingPayment(Request $request,$nBookId)
	{
		$page_name = 'payment_details';
		
		if ($request->isMethod('post'))
		{
			if(is_numeric($request->deposit_money)){
						
				$last_rec = BookingAmount::where("order_id",$request->order_id)->orderBy("id",'desc')->first();
						if(is_object($last_rec)){
							
							$pending = $this->twoDecimalFloat($last_rec->pending);
							if($pending==0.00){
								Session::flash('error_msg', 'No pending amount for deposit.');
									return redirect()->route('booking.payment',$request->order_id)->with("error_msg ","Invalid deposit. Please sure payment is correct.");
							}
							$deposit_money = $this->twoDecimalFloat($request->deposit_money);
								if($deposit_money > $pending){
									Session::flash('error_msg', 'Invalid deposit. Please sure payment is correct.');
									return redirect()->route('booking.payment',$request->order_id)->with("error_msg ","Invalid deposit. Please sure payment is correct.");
								}else{
									$total_pending = $pending - $deposit_money;
									$deposit = $deposit_money;
									
									$DepositArray = array(
									'order_id' => $request->order_id,
									'total' => $last_rec->total,
									'deposit' => $deposit,
									'pending' => $total_pending
									);
									BookingAmount::create($DepositArray);
									ItenaryOrder::where("order_id",$request->order_id)->update(['deposit_amount'=> $deposit, 'pending_amount' => $total_pending]);
									Session::flash('succ_msg', 'Successfully deposited amount.');
									
								}
							
							
							
						}else{
								Session::flash('error_msg', 'No recods found.');
								return redirect()->route('booking.payment',$request->order_id)->with("error_msg ","Invalid deposit. Please sure payment is correct.");
						}	
				
			}else{
					Session::flash('error_msg', 'Please Enter Valid Amount.');
					return redirect()->route('booking.payment',$request->order_id)->with("error_msg ","Invalid deposit. Please sure payment is correct.");
				
			}
		
		}
		
		$order_payment = BookingAmount::where('order_id',$nBookId)->orderBy("id",'desc')->get();
		
		return \View::make('WebView::booking.booking_payment',compact('nBookId','order_payment','page_name'));
	}

    public function locationDetails($nBookId) {
        $page_name = 'location_details';
        $oItinerary = ItenaryOrder::with('ItenaryLegs', 'LegDetails')
                ->select('*')
                ->where('order_id', $nBookId)
                ->first();

        echo "<pre>";
        print_r($oItinerary);
        die;
        return \View::make('WebView::booking.location_detail', compact('page_name', 'nBookId', 'oItinerary'));
        die('location details');
    }

    public function productDetails($nBookId) {
        $page_name = 'product_details';
        $passanger = PassengerInformation::where('itenary_order_id', $nBookId)->get();
        $total_travller = count($passanger);
        $types = array('hotel', 'activities', 'transport');
        $city = ItenaryLeg::from('itenarylegs')
                ->select(
                        'itenarylegs.from_city_name'
                )
                ->where('itenarylegs.itenary_order_id', $nBookId)
                ->groupBy('itenarylegs.itenary_leg_id')
                ->get();

        $allcities = $city->toArray();
        $cities_arr = array_column($allcities, 'from_city_name');
       	$oItinerary = LegDetail::from('legdetails')
                ->leftjoin('itenarylegs', 'itenarylegs.itenary_leg_id', '=', 'legdetails.itenary_leg_id')
                ->join('booking_commissions', 'itenarylegs.itenary_order_id', '=', 'booking_commissions.booking_id')
                ->select(
                        'legdetails.*', 'itenarylegs.from_city_name as from_city', 'itenarylegs.to_city_name as to_city', 'itenarylegs.from_date as city_from_date', 'itenarylegs.to_date as city_to_date', 'itenarylegs.itenary_leg_id as leg_detail_ids', 'itenarylegs.itenary_order_id as leg_detail_order_id', 'booking_commissions.reseller_cost', 'booking_commissions.licensee_benefit', 'booking_commissions.licensee_rrp as sellprice'
                )
                ->where('legdetails.itenary_order_id', $nBookId)
                ->get();
        foreach ($cities_arr as $key => $val) {
            foreach ($oItinerary as $details) {
                foreach ($types as $type) {
                    if ($details['from_city'] == $val && $details['leg_type'] == $type) {
                        $product_details['leg_detail_id'] = $details['leg_detail_id'];
                        $product_details['leg_detail_ids'] = $details['leg_detail_ids'];
                        $product_details['itenary_leg_id'] = $details['itenary_leg_id'];
                        $product_details['leg_detail_order_id'] = $details['leg_detail_order_id'];
                        $product_details['city_to_date'] = $details['city_to_date'];
                        $product_details['city_from_date'] = $details['city_from_date'];
                        $product_details['from_city'] = $details['from_city'];
                        $product_details['to_city'] = $details['to_city'];
                        $product_details['from_city'] = $details['from_city'];
                        $product_details['reseller_cost'] = $details['reseller_cost'];
                        $product_details['licensee_benefit'] = $details['licensee_benefit'];
                        $product_details['sellprice'] = $details['sellprice'];
                        $product_details['booking_id'] = $details['booking_id'];
                        $product_details['leg_name'] = $details['leg_name'];
                        $product_details['leg_type'] = $details['leg_type'];
                        $product_details['nights'] = $details['nights'];
                        $product_details['price'] = $details['price'];
                        $product_details['hotel_room_type_name'] = $details['hotel_room_type_name'];
                        $product_details['provider_booking_status'] = $details['provider_booking_status'];
                        $product_details['notes'] = $details['notes'];
                        $product_details['supplier_id'] = $details['supplier_id'];
                        $product_details['provider'] = $details['provider'];
                        $product_details_all[$val][$type] = $product_details;
                    }
                }
            }
        }
		return View::make('WebView::booking.product_details', compact('page_name', 'nBookId', 'oItinerary', 'total_travller', 'product_details_all', 'cities_arr', 'types'));
    }

    public function supplierDetails($nBookId) {
        $page_name = 'supplier_details';
		$passanger = PassengerInformation::where('itenary_order_id', $nBookId)->get();
        $total_travller = count($passanger);
		$types = array('hotel', 'activities', 'transport');
        $city = ItenaryLeg::from('itenarylegs')
                ->select(
                        'itenarylegs.from_city_name'
                )
                ->where('itenarylegs.itenary_order_id', $nBookId)
                ->groupBy('itenarylegs.itenary_leg_id')
                ->get();

        $allcities = $city->toArray();
        $cities_arr = array_column($allcities, 'from_city_name');
       	$oItinerary = LegDetail::from('legdetails')
                ->leftjoin('itenarylegs', 'itenarylegs.itenary_leg_id', '=', 'legdetails.itenary_leg_id')
                ->select(
						'legdetails.leg_detail_id',
                        'legdetails.itenary_leg_id',
                        'legdetails.leg_name',
                        'legdetails.leg_name',
                        'legdetails.leg_type',
                        'legdetails.supplier_id',
                        'legdetails.notes',
						'itenarylegs.from_city_name as from_city',
						'itenarylegs.itenary_leg_id as leg_detail_ids',
						'itenarylegs.itenary_order_id as leg_detail_order_id',
						DB::raw('(SELECT name FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_name'),
						DB::raw('(SELECT reservation_contact_name FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_contact_name'),
						DB::raw('(SELECT reservation_contact_email FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_contact_email'),
						DB::raw('(SELECT reservation_contact_landline FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_contact_landline'),
						DB::raw('(SELECT reservation_contact_free_phone FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_contact_free_phone'),
						DB::raw('(SELECT special_notes FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_special_notes'),
						DB::raw('(SELECT remarks FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_remarks'),
						DB::raw('(SELECT percentage FROM ztransportsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_percentage'),
						DB::raw('(SELECT name FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_hotel_name'),
						DB::raw('(SELECT reservation_contact_name FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_hotel_name'),
						DB::raw('(SELECT reservation_contact_email FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_hotel_email'),
						DB::raw('(SELECT reservation_contact_landline FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_hotel_landline'),
						DB::raw('(SELECT reservation_contact_free_phone FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_hotel_reservation_contact_free_phone'),
						DB::raw('(SELECT special_notes FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_hotel_special_notes'),
						DB::raw('(SELECT remarks FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_hotel_remarks'),
						DB::raw('(SELECT percentage FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_hotel_percentage'),
						DB::raw('(SELECT cancellation_formula FROM zhotelsuppliers WHERE id = legdetails.supplier_id) as supplier_hotel_percentage'),
						DB::raw('(SELECT name FROM zactivitysuppliers WHERE id = legdetails.supplier_id) as supplier_activity_name'),
						DB::raw('(SELECT reservation_contact_name FROM zactivitysuppliers WHERE id = legdetails.supplier_id) as supplier_activity_reservation_contact_name'),
						DB::raw('(SELECT reservation_contact_email FROM zactivitysuppliers WHERE id = legdetails.supplier_id) as supplier_activity_reservation_contact_email'),
						DB::raw('(SELECT reservation_contact_landline FROM zactivitysuppliers WHERE id = legdetails.supplier_id) as supplier_activity_reservation_contact_landline'),
						DB::raw('(SELECT reservation_contact_free_phone FROM zactivitysuppliers WHERE id = legdetails.supplier_id) as supplier_transport_reservation_contact_free_phone'),
						DB::raw('(SELECT special_notes FROM zactivitysuppliers WHERE id = legdetails.supplier_id) as supplier_activity_special_notes'),
						DB::raw('(SELECT remarks FROM zactivitysuppliers WHERE id = legdetails.supplier_id) as supplier_activity_remarks')
				)
                ->where('legdetails.itenary_order_id', $nBookId)
                ->get();
		foreach ($cities_arr as $key => $val) {
            foreach ($oItinerary as $details) {
                foreach ($types as $type) {
                    if ($details['from_city'] == $val && $details['leg_type'] == $type) {
                        $supplier_details['leg_detail_id'] = $details['leg_detail_id'];
                        $supplier_details['leg_detail_ids'] = $details['leg_detail_ids'];
                        $supplier_details['itenary_leg_id'] = $details['itenary_leg_id'];
                        $supplier_details['leg_detail_order_id'] = $details['leg_detail_order_id'];
                        $supplier_details['from_city'] = $details['from_city'];
                        $supplier_details['booking_id'] = $details['booking_id'];
                        $supplier_details['leg_name'] = $details['leg_name'];
                        $supplier_details['leg_type'] = $details['leg_type'];
                        $supplier_details['hotel_room_type_name'] = $details['hotel_room_type_name'];
                        $supplier_details['provider_booking_status'] = $details['provider_booking_status'];
                        $supplier_details['notes'] = $details['notes'];
                        $supplier_details['supplier_id'] = $details['supplier_id'];
                        $supplier_details['provider'] = $details['provider'];
						$supplier_details['supplier_hotel_name'] =$details['supplier_hotel_name'] ;
						$supplier_details['supplier_hotel_reservation_name'] = $details['supplier_transport_reservation_hotel_name'];
						$supplier_details['supplier_hotel_reservation_email'] =  $details['supplier_transport_reservation_hotel_email'];
						$supplier_details['supplier_hotel_reservation_landline'] =$details['supplier_transport_reservation_hotel_landline'];
						$supplier_details['supplier_hotel_reservation_contact_free_phone'] =$details['supplier_hotel_reservation_contact_free_phone'];
						$supplier_details['supplier_hotel_special_notes'] =$details['supplier_hotel_special_notes'];
						$supplier_details['supplier_hotel_remarks'] =$details['supplier_hotel_remarks'];
						$supplier_details['supplier_hotel_percentage'] =$details['supplier_hotel_percentage'];
						$supplier_details['supplier_activity_name'] =$details['supplier_activity_name'];
						$supplier_details['supplier_activity_reservation_contact_name'] =$details['supplier_activity_reservation_contact_name'];
						$supplier_details['supplier_activity_reservation_contact_email'] = $details['supplier_activity_reservation_contact_email'];
						$supplier_details['supplier_activity_reservation_contact_landline'] =$details['supplier_activity_reservation_contact_landline'];
						$supplier_details['supplier_activity_special_notes'] =$details['supplier_activity_special_notes'];
						$supplier_details['supplier_activity_remarks'] =$details['supplier_activity_remarks'];
						$supplier_details['supplier_transport_name'] =$details['supplier_transport_name'];
						$supplier_details['supplier_transport_reservation_contact_name'] =$details['supplier_transport_reservation_contact_name'];
                        $supplier_details['supplier_transport_reservation_contact_email'] =$details['supplier_transport_reservation_contact_name'];
                        $supplier_details['supplier_transport_reservation_contact_landline'] =$details['supplier_transport_reservation_contact_landline'];
                        $supplier_details['supplier_transport_reservation_contact_free_phone'] =$details['supplier_transport_reservation_contact_free_phone'];
						$supplier_details['supplier_transport_special_notes'] =$details['supplier_transport_special_notes'];
						$supplier_details['supplier_transport_remarks'] = $details['supplier_transport_remarks'];
						$supplier_details['supplier_transport_percentage'] =$details['supplier_transport_percentage'];
						$supplier_details_all[$val][$type] = $details;
                    }
                }
            }
        }
		//pr($supplier_details_all);die;
		return View::make('WebView::booking.supplier_details', compact('page_name', 'nBookId', 'oItinerary', 'supplier_details_all', 'cities_arr', 'types','total_travller'));
    }

    public function itenaryDetails($nBookId) {
        $page_name = 'itenary_details';
        return View::make('WebView::booking.itenary_details', compact('page_name', 'nBookId'));
    }

    public function voucherDetails($nBookId) {
        $page_name = 'voucher_details';
        $types = array('hotel', 'activities', 'transport');
        $city = ItenaryLeg::from('itenarylegs')
                ->select(
                        'itenarylegs.from_city_name'
                )
                ->where('itenarylegs.itenary_order_id', $nBookId)
                ->groupBy('itenarylegs.itenary_leg_id')
                ->get();

        $allcities = $city->toArray();
        $cities_arr = array_column($allcities, 'from_city_name');
        // echo "<pre>";print_r($cities_arr);die;					
        // die;		
        $oItinerary = LegDetail::from('legdetails')
                ->leftjoin('itenarylegs', 'itenarylegs.itenary_leg_id', '=', 'legdetails.itenary_leg_id')
                ->select(
                        'legdetails.leg_detail_id',
						'legdetails.itenary_leg_id',
						'legdetails.itenary_order_id',
						'legdetails.leg_id',
						'legdetails.leg_name',
						'legdetails.leg_type',
						'legdetails.provider_booking_status',
						'legdetails.voucher_key',
						'legdetails.voucher_url',
						'legdetails.voucher',
						'legdetails.voucher_not_required',
						'legdetails.notes',
						'itenarylegs.from_city_name as from_city',
						'itenarylegs.itenary_leg_id as leg_detail_ids',
						'itenarylegs.itenary_order_id as leg_detail_order_id'
						
                )
                ->where('legdetails.itenary_order_id', $nBookId)
                ->get();
        foreach ($cities_arr as $key => $val) {
            foreach ($oItinerary as $details) {
                foreach ($types as $type) {
                    if ($details['from_city'] == $val && $details['leg_type'] == $type) {
                        $vouchers['leg_detail_id'] = $details['leg_detail_id'];
                        $vouchers['leg_detail_ids'] = $details['leg_detail_ids'];
                        $vouchers['itenary_leg_id'] = $details['itenary_leg_id'];
                        $vouchers['itenary_order_id'] = $details['itenary_order_id'];
                        $vouchers['voucher_url'] = $details['voucher_url'];
                        $vouchers['voucher_key'] = $details['voucher_key'];
                        $vouchers['from_city'] = $details['from_city'];
                        $vouchers['voucher_not_required'] = $details['voucher_not_required'];
                        $vouchers['voucher'] = $details['voucher'];
                        $vouchers_details[$val][$type] = $vouchers;
                    }
                }
            }
        }
        return View::make('WebView::booking.voucher_details', compact('page_name', 'nBookId', 'oItinerary', 'vouchers_details', 'cities_arr', 'types'));
    }

    public function updateVoucherDetails(Request $request, $legDetailId) {
        $data['voucher_not_required'] = $request->input('not_required');
        $voucher = $_FILES['file_data']['name'];
        //$data['voucher']=  Input::file('file_data');
        $extension = Input::file('file_data')->getClientOriginalExtension();
        $filename = rand(11111111, 99999999) . '.' . $extension;
        Input::file('file_data')->move(
                base_path() . '/public/uploads/upload', $filename
        );
        $fullPath = '/public/uploads/upload' . $filename;
        $data['voucher'] = $voucher;
        $legDetailUpdate = LegDetail::where('leg_detail_id', $legDetailId)->update($data);
        if ($legDetailUpdate == 1) {
            $message = "successfully updated";
        }
    }

    public function editCustomer($user_id = null) {
        $countriesList = Country::select('id','name')->orderBy('name','asc')->get();
        $userDetails = Customer::where('zcustomers.user_id', $user_id)
                        ->leftJoin('users', 'users.id', '=', 'zcustomers.user_id')->first();
        return \view('WebView::agent.update_customer', compact('userDetails','countriesList'));
    }

    public function updateCustomer(Request $request) {
        $userid = Input::get('id');
        $customerId = Input::get('user_id');
        $rules = [
            'name' => 'required',
            'family_name' => 'required',
            'gender' => 'required',
            'contact_no' => 'required',
            'email' => 'required|email',
            'contact_method' => 'required',
            //'account_id' => 'required|unique:users'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return ['result' => 0, 'errors' => $validator->messages()];
        }

        $login_user = Auth::user();
        if(session()->has('new_agent')) {
            $login_user = session()->get('new_agent');
        }
        $obj = User::find($userid);
        $obj->username = $request->email;
        $obj->name = $request->name . " " . $request->family_name;
        $obj->contact_no = $request->contact_no;
        $obj->gender = $request->gender;
        $obj->user_id = $login_user['id'];
        $obj->licensee_id = $login_user['licensee_id'];
        $obj->save();

        DB::table('zcustomers')
                ->where('user_id', $userid)
                ->update([
                    'first_name' => $request->name,
                    'last_name' => $request->family_name,
                    'user_id' => $userid,
                    'email' => $request->email,
                    'contact_no' => !empty($request->contact_no) ? $request->contact_no : '',
                    'pref_contact_method' => !empty($request->contact_method) ? $request->contact_method : '',
                    'freq_used_products' => !empty($request->freq_used_products) ? implode(',', $request->freq_used_products) : '',
                    'pref_hotel_categories' => !empty($request->accommodation_type) ? $request->accommodation_type : '',
                    'pref_hotel_room_types' => !empty($request->room_type) ? $request->room_type : '',
                    'pref_transport_types' => !empty($request->transport_type) ? $request->transport_type : '',
                    'pref_cabin_class' => !empty($request->cabin_class) ? $request->cabin_class : '',
                    'pref_seat_type' => !empty($request->seat_type) ? implode(',', $request->seat_type) : '',
                    'pref_meal_type' => !empty($request->meal_type) ? implode(',', $request->meal_type) : '',
                    'interests' => !empty($request->interests) ? implode(',', $request->interests) : '',
                    'additional_info' => !empty($request->additional_info) ? $request->additional_info : ''
                ]);

        $usrDetails = array(
                    'account_id' => !empty($request->account_id) ? $request->account_id : '',
                    'other_name' => !empty($request->other_name) ? $request->other_name : '',
                    'gender' => !empty($request->gender) ? $request->gender : '',
                    'birth_year' => !empty($request->birth_year) ? $request->birth_year : '',
                    'nationality' => !empty($request->nationality) ? $request->nationality : '',
                    'passport_number' => !empty($request->passport_number) ? $request->passport_number : '',
                    'passport_expiry_date' => !empty($request->passport_expiry_date) ? $request->passport_expiry_date : '',
                    'passport_issue_date' => !empty($request->passport_issue_date) ? $request->passport_issue_date : '',
                    'address_one' => !empty($request->address_one) ? $request->address_one : '',
                    'address_two' => !empty($request->address_two) ? $request->address_two : '',
                    'city' => !empty($request->city) ? $request->city : '',
                    'state' => !empty($request->state) ? $request->state : '',
                    'zip' => !empty($request->zip) ? $request->zip : '',
                    'country_id' => !empty($request->country_id) ? $request->country_id : '');

        $userUpdate = User::where('id', $userid)->update($usrDetails);

        return ['result' => 1, 'msg' => 'Customer Updated successfully', 'url' => 'http://eroamlocal.com/'];
    }

    public function savedTrips($user_id) {
        $trips = SavedTrips::where('user_id',$user_id)->get();
        return view('WebView::agent.saved-trips',compact('trips'));
    }
}
