<?php
//priya
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;

use App\Label;
use App\Region;
use App\Country;
use App\City;
use App\CitiesLatLong;
use App\Timezone;
use App\HBDestination;
use App\AECity;
use App\AOTLocation;
use App\AOTMapSupplierCity;
use App\HBDestinationsMapped;
use App\HBZone;
use App\HBDestinationMapped;
use App\AECitiesMapped;
use App\ViatorLocationsMapped;
use App\CityImage;
use App\Coupon;
use App\Suburb;
use App\Currency;
use App\UserDomain;
use App\DomainExtras;
use App\DomainLocation;
use App\Domain;
use App\Licensee;

use Session;
use Auth;
use File;
use Image;
use DB;

class CommonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callLabelList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'label' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('label');

        session(['page_name' => 'label']);
        $aData = session('label') ? session('label') : array();
        $oRequest->session()->forget('label');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'Name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;   
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oLabelList = Label::geLabelList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oLabelList->currentPage(),'label');

        if($oRequest->page > 1)
            $oViewName =  'WebView::common._label_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.label_list' : 'WebView::common._label_list_ajax';
        
        return \View::make($oViewName, compact('oLabelList','sSearchStr','sOrderField','sOrderBy','nShowRecord'));
    }
    
    public function callLabelCreate(Request $oRequest,$nIdLabel='') 
    {
        session(['page_name' => 'label']);
        
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name'=> 'required|unique:zlabels,name,'.$oRequest->label_id 
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $data=array();
            $data['name'] = $oRequest->name;
            $oLabel = Label::updateOrCreate(['id' => $oRequest->label_id],$data);
            DB::table('cache_reload')->update(['is_reload' => 1]);
            if($oRequest->label_id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            return Redirect::back();
        }
        $oLabel = Label::find($nIdLabel);

        return \View::make('WebView::common.label_create',compact('oLabel'));
    }
    
    public function callLabelDelete($nIdLabel)
    {
        $oLabel = Label::find($nIdLabel);
        $oLabel->delete();
    }
    
    public function callRegionList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
		//echo	Auth::user()->id; die;
		$user_domain = UserDomain::where('user_id',Auth::user()->id)->get();
		$licensee = Licensee::orderBy('business_name','asc')->pluck('business_name','id');
		$regions = Region::pluck('name','id')->toArray();
        if(session('page_name') != 'region' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('region');

        session(['page_name' => 'region']);
        $aData = session('region') ? session('region') : array();
        $oRequest->session()->forget('region');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oRegionList = Region::geRegionList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,'','region');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.region_list' : 'WebView::common._region_list_ajax';
        
        return \View::make($oViewName, compact('oRegionList','sSearchStr','sOrderField','sOrderBy','nShowRecord','user_domain','regions','licensee')); 
    }
	
	public function callRegionLists(Request $oRequest) 
	{
		//echo "Hello";die;
        if(session('page_name') != 'region' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('region');

        session(['page_name' => 'region']);
        $aData = session('region') ? session('region') : array();
        $oRequest->session()->forget('region');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oRegionList = Region::geRegionList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,'','region');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.region_lists' : 'WebView::common._region_lists_ajax';
        
        return \View::make($oViewName, compact('oRegionList','sSearchStr','sOrderField','sOrderBy','nShowRecord')); 
		
	}
	
	public function setDomainGeoLink(Request $oRequest)
	{
		$domain_id = $oRequest->domain_id;
		$url =  $oRequest->url;
		return response()->json([
			'url' => $url.'?domain_id='.$domain_id
		]);
	}
	
	public function callUserRegion(Request $oRequest)
	{
		$domain_id = "";
		$user_region = [];
		if($oRequest->domain_id){
			$domain_id = $oRequest->domain_id;
		}

		if($domain_id == ""){
			return redirect()->route('common.manage_user_geo_data').'?isreset=1';
		}
		
		if(Auth::user()->type=='eroamProduct'){
			
			$check_domain_region = DomainExtras::where([['domain_id',$domain_id],['type','region']])->count();
			
			if($check_domain_region >0){
			
				$UserRegion = DomainLocation::select('region_id')->where('domain_id',$domain_id)->get();
				
				$license = DomainLocation::select('licensee_id')->where('domain_id',$domain_id)->first();
				
				if(isset($UserRegion) && count($UserRegion) > 0){
					foreach($UserRegion as $key=>$value){
						$user_region['regions'][$key] = $value->region_id;
					}
				}
				
				$regions = Region::pluck('name','id')->toArray();
			
				return \View::make('WebView::common.user_regions', compact('user_region','regions','domain_id','license'));
				
			}else{
				return redirect()->route('common.manage_user_geo_data').'?isreset=1';
			}	
				
		}else{
			
			$check_domainid = UserDomain::where('user_id',Auth::user()->id)->where('domain_id',$domain_id)->count();
			$check_domain_region = DomainExtras::where([['domain_id',$domain_id],['type','region']])->count();
		
			if($check_domainid > 0 && $check_domain_region >0){
				
				$UserRegion = DomainLocation::select('region_id')->where('domain_id',$domain_id)->get();
				if(isset($UserRegion) && count($UserRegion) > 0){
					foreach($UserRegion as $key=>$value){
						$user_region['regions'][$key] = $value->region_id;
					}
				}
				
				$regions = Region::pluck('name','id')->toArray();
			
				return \View::make('WebView::common.user_regions', compact('user_region','regions','domain_id'));
				
			}else{
				return redirect()->route('common.manage_user_geo_data').'?isreset=1';
			}
			
		}
		
	}
	
	public function addUserRegion(Request $oRequest)
	{
		$domain_id = $oRequest->domain_id;
		
		if(Auth::user()->type=='eroamProduct'){
			$licensee_id = $oRequest->licensee_id;
		}else{
			$licensee_id = Auth::user()->licensee_id;
		}
		
		$regionArray = array();
		
		DomainLocation::where('domain_id',$domain_id)->delete();
		
		if(count($oRequest->region) > 0){
			foreach($oRequest->region as $key=>$value){
				$regionArray = array(
				'licensee_id' => $licensee_id,
				'domain_id' => $domain_id,
				'region_id' => $value,
				'created_by' => Auth::user()->id
				);
				DomainLocation::create($regionArray);
			}
		}
		
		return Redirect::to('region-geo?domain_id='.$domain_id);
		
	}
	
	public function callUserCountry(Request $oRequest)
	{
		$domain_id = "";
		$user_country = [];
		$countries =	[];
		$region = [];
		$fullcountries = [];
		if($oRequest->domain_id){
			$domain_id = $oRequest->domain_id;
		}

		if($domain_id == ""){
			return redirect()->route('common.manage_user_geo_data').'?isreset=1';
		}
		
		if(Auth::user()->type=='eroamProduct'){
			
				$check_domain_country = DomainExtras::where([['domain_id',$domain_id],['type','country']])->count();
				$check_locations = DomainLocation::where('domain_id',$domain_id)->count();
				
				if($check_domain_country > 0 && $check_locations >0){
			
					$UserCountry = DomainLocation::where('domain_id',$domain_id)->groupBy('region_id')->get();
					
					$license = DomainLocation::select('licensee_id')->where('domain_id',$domain_id)->first();
					
					//pr($license);die;
					
					if($UserCountry->count() == 0){
						$fullcountries =  Region::pluck('name','id')->toArray();
					}
					
					$countryObj = DomainLocation::where('domain_id',$domain_id)->groupBy('country_id')->get();
					
					if(is_object($countryObj)){
						foreach($countryObj as $key1=>$value1){
							$user_country['countries'][$key1] = $value1->country_id;
						}
					}
					
					if(isset($UserCountry) && count($UserCountry) > 0){
						foreach($UserCountry as $key=>$value){
							$region['region'][$key] = $value->region_id;
						}
					}
					return \View::make('WebView::common.user_countries', compact('region','user_country','domain_id','fullcountries','license'));
					
				}else{
					return redirect()->route('common.manage_user_geo_data').'?isreset=1';
				}	
		}else{
				$check_domainid = UserDomain::where('user_id',Auth::user()->id)->where('domain_id',$domain_id)->count();
				$check_domain_country = DomainExtras::where([['domain_id',$domain_id],['type','country']])->count();
				
				if($check_domainid > 0 && $check_domain_country >0 ){
					
					$UserCountry = DomainLocation::where('domain_id',$domain_id)->groupBy('region_id')->get();
					
					if($UserCountry->count() == 0){
						$fullcountries =  Region::pluck('name','id')->toArray();
						//echo "<pre>"; print_r($fullcountries);die;
					}
					
					$countryObj = DomainLocation::where('domain_id',$domain_id)->groupBy('country_id')->get();
					
					if(is_object($countryObj)){
						foreach($countryObj as $key1=>$value1){
							$user_country['countries'][$key1] = $value1->country_id;
						}
					}
					
					if(isset($UserCountry) && count($UserCountry) > 0){
						foreach($UserCountry as $key=>$value){
							$region['region'][$key] = $value->region_id;
						}
					}
					
					return \View::make('WebView::common.user_countries', compact('region','user_country','domain_id','fullcountries'));
				}else{
					return redirect()->route('common.manage_user_geo_data').'?isreset=1';
				}
		}
		
	}
	
	public function addUserCountry(Request $oRequest)
	{
		$country_id = [];
		$domain_id = $oRequest->domain_id;
		if(Auth::user()->type=='eroamProduct'){
			$licensee_id = $oRequest->licensee_id;
		}else{
			$licensee_id = Auth::user()->licensee_id;
		}
		
		if($oRequest->country_id){
			$country_id = $oRequest->country_id;
		}
		
		DomainLocation::where('domain_id',$domain_id)->delete();
		if(count($country_id) > 0){
			for($i=0; $i<count($country_id); $i++){
				$explArry = explode('|',$country_id[$i]);
				$countryArray = array(
				'licensee_id' => $licensee_id,
				'domain_id' => $domain_id,
				'region_id' => $explArry[1],
				'country_id' => $explArry[0],
				'created_by' => Auth::user()->id
				);
				DomainLocation::create($countryArray);
			}
		}
		
		return Redirect::to('country-geo?domain_id='.$domain_id);
		
	}
	
	public function callUserCity(Request $oRequest)
	{
		set_time_limit(2000);
		
		$domain_id = "";
		$user_country = [];
		$countries =	[];
		$region = [];
		$city = [];
		$fullcountries = [];
		if($oRequest->domain_id){
			$domain_id = $oRequest->domain_id;
		}

		if($domain_id == ""){
			return redirect()->route('common.manage_user_geo_data').'?isreset=1';
		}
		
		if(Auth::user()->type=='eroamProduct'){
			
				$check_domain_country = DomainExtras::where([['domain_id',$domain_id],['type','city']])->count();
				
				if($check_domain_country > 0){
					
					$license = DomainLocation::select('licensee_id')->where('domain_id',$domain_id)->first();
			
						$UserCountry = DomainLocation::where('domain_id',$domain_id)->groupBy('country_id')->get();
						
						if($UserCountry->count() == 0){
							$fullcountries =  Country::pluck('name','id')->toArray();
						}
						
						$cityObj = DomainLocation::select('city_id')->where('domain_id',$domain_id)->get();
						
						if(is_object($cityObj)){
							foreach($cityObj as $key1 => $value1 ){
								$city['city'][$key1] = $value1->city_id;
							}
						}
						
						if(isset($UserCountry) && count($UserCountry) > 0)	{
							foreach($UserCountry as $key=>$value){
								$user_country['countries'][$key] = $value->country_id;
								$region['region'][$key] = $value->region_id;
							}
						}
						
					return \View::make('WebView::common.user_cities', compact('region','user_country','city','domain_id','fullcountries','license'));
				
				}else{
					return redirect()->route('common.manage_user_geo_data').'?isreset=1';
				}
		
		}else{
					$check_domainid = UserDomain::where('user_id',Auth::user()->id)->where('domain_id',$domain_id)->count();
					$check_domain_country = DomainExtras::where([['domain_id',$domain_id],['type','city']])->count();
		
					if($check_domainid > 0 && $check_domain_country > 0){
						
						$UserCountry = DomainLocation::where('domain_id',$domain_id)->groupBy('country_id')->get();
						
						if($UserCountry->count() == 0){
							$fullcountries =  Country::pluck('name','id')->toArray();
							//echo "<pre>"; print_r($fullcountries);die;
						}
						
						$cityObj = DomainLocation::select('city_id')->where('domain_id',$domain_id)->get();
						
						if(is_object($cityObj)){
							foreach($cityObj as $key1 => $value1 ){
								$city['city'][$key1] = $value1->city_id;
							}
						}
						
						if(isset($UserCountry) && count($UserCountry) > 0)	{
							foreach($UserCountry as $key=>$value){
								$user_country['countries'][$key] = $value->country_id;
								$region['region'][$key] = $value->region_id;
							}
						}
						
						return \View::make('WebView::common.user_cities', compact('region','user_country','city','domain_id','fullcountries'));
						
					}else{
						return redirect()->route('common.manage_user_geo_data').'?isreset=1';
					}
		}
		
	}
	
	public function addUserCity(Request $oRequest)
	{
		$cityid = [];
		$domain_id = $oRequest->domain_id;
		if(Auth::user()->type=='eroamProduct'){
			$licensee_id = $oRequest->licensee_id;
		}else{
			$licensee_id = Auth::user()->licensee_id;
		}
		if($oRequest->cityid){
			$cityid = $oRequest->cityid;
		}
		DomainLocation::where('domain_id',$domain_id)->delete();
		if(count($cityid) > 0){
			for($i=0; $i<count($cityid); $i++){
				$explArry = explode('|',$cityid[$i]);
				$cityArray = array(
				'licensee_id' => $licensee_id,
				'domain_id' => $domain_id,
				'region_id' => $explArry[2],
				'country_id' => $explArry[1],
				'city_id' => $explArry[0],
				'created_by' => Auth::user()->id
				);
				DomainLocation::create($cityArray);
			}
		}
		
		return Redirect::to('city-geo?domain_id='.$domain_id);
	}
	
	public function getGioDataUserWise(Request $oRequest)
	{
		$status = 400;
		$msg = "Error in code";
		$data	= array();
		
		$domain_id = '';
		$flag	= '';
		
		if($oRequest->domain_id)
		{
			$domain_id = $oRequest->domain_id;
		}
		
		if(!empty($domain_id))	{
			
			$getType = DomainExtras::where('domain_id',$domain_id)->get();
			
			if(is_object($getType) && $getType->count() >	0)	 {
				$status = 200;
				$msg = "success.";
				$data['type'] = $getType;
			}else{
				$status = 500;
				$msg = "Not found.";
			}
			
		}		
		return response()->json([
				'data' => $data,
				'flag' => $flag,
				'msg' => $msg,
				'status' => $status
			]);
		
	}
    
    public function callRegionSwitch(Request $oRequest,$nRegionId) 
    {
        session(['page_name' => 'region']);
        if ($oRequest->isMethod('post') && $nRegionId !='')
    	{
            $oRegion = Region::find($nRegionId );
            $oRegion->show_on_eroam = $oRequest->show;
            $oRegion->update();
            DB::table('cache_reload')->update(['is_reload' => 1]);
            Country::where('region_id',$nRegionId)->update(['show_on_eroam'=>$oRequest->show]);
            $oCountry = Country::where('region_id',$nRegionId)->get();
            foreach($oCountry as $aCountry){
                City::where('country_id',$aCountry->id)->update(['is_disabled'=> !$oRequest->show ]);
            } 
            
            
        }
    }
    
    public function callCountryUpadetRegion(Request $oRequest)
    {
        $oCountry = Country::find($oRequest->country_id);
        $oCountry->region_id = $oRequest->region_id;
        $oCountry->update();
        $oRegion = Region::find($oRequest->region_id);
        return $oRegion->name;
    }

    public function callCountryList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'country' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('country');

        session(['page_name' => 'country']);
        $aData = session('country') ? session('country') : array();
        $oRequest->session()->forget('country'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'c.name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oCountryList = Country::geCountryList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oCountryList->currentPage(),'country');
        if($oRequest->page > 1)
            $oViewName =  'WebView::common._country_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.country_list' : 'WebView::common._country_list_ajax';
        
        return \View::make($oViewName, compact('oCountryList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }

    
    public function callCountrySwitch(Request $oRequest,$nCountryId) 
    {
        session(['page_name' => 'country']);
        if ($oRequest->isMethod('post') && $nCountryId !='')
    	{
            $oCountry = Country::find($nCountryId);
            $oCountry->show_on_eroam = $oRequest->show;
            $oCountry->update();
            DB::table('cache_reload')->update(['is_reload' => 1]);
            City::where('country_id',$nCountryId)->update(['is_disabled'=> !$oRequest->show ]);
        }
    }
    
    public function callCityList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'city' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('city');

        session(['page_name' => 'city']);
        $aData = session('city') ? session('city') : array();
        $oRequest->session()->forget('city'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'c.name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oCityList = City::geCityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oCityList->currentPage(),'city');

        if($oRequest->page > 1)
            $oViewName =  'WebView::common._city_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.city_list' : 'WebView::common._city_list_ajax';
        
        return \View::make($oViewName, compact('oCityList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callCityCreate(Request $oRequest,$nIdCity='') 
    {
        session(['page_name' => 'city']);
        if ($oRequest->isMethod('post'))
    	{
            //echo "<pre>";var_dump(json_decode($oRequest->aot_location_codes));exit;
            $oCity = City::firstOrNew(['id' => $oRequest->city_id]);
            $aValidationRules = [
                                    'name'=>'required',
                                    'optional_city'=>'required',
                                    'default_nights'=>'required|numeric',
                                    'country_id' =>'required',
                                    'lat' => 'required',
                                    'lng' => 'required'
                                    ];
            if($oRequest->city_id!='' && $oCity->country_id != '' )
                $aValidationRules['timezone_id'] = 'required';
            
           //print_r($oRequest->enabled);exit;
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRules);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            //$oCity = City::firstOrNew(['id' => $oRequest->city_id]);
            DB::table('cache_reload')->update(['is_reload' => 1]);
            $oCity->name = $oRequest->name;
            $oCity->description = $oRequest->description;
            $oCity->default_nights = $oRequest->default_nights;
            $oCity->optional_city = $oRequest->optional_city;
            $oCity->country_id = $oRequest->country_id;
            $oCity->is_disabled =  ($oRequest->enabled ) ? FALSE : TRUE;
            $oCity->region_id =  0;
            if($oRequest->city_id != '')
                $oCity->timezone_id = $oRequest->timezone_id;
            $oCity->save();
            
            $oLatlong = CitiesLatLong::firstOrNew(['city_id' => $oCity->id]);
            $oLatlong->city_id = $oCity->id;
            $oLatlong->lat = $oRequest->lat; 
            $oLatlong->lng = $oRequest->lng; 
            $oLatlong->save();
            
            if($oRequest->city_id != '')
            {
                //Hbdestination mapped table entry
                //there is no data for mapped with country id this insert is not in use
                $aHbDestinations = json_decode($oRequest->hb_destination_codes);
                HBDestinationsMapped::where('city_id', $oRequest->city_id)->delete();
                if($aHbDestinations && count($aHbDestinations)>0)
                {
                    foreach( $aHbDestinations as $aHb )
                    {
                        $aHbDestination = HBDestinationsMapped::withTrashed()
                                                            ->firstOrNew([ 'city_id'=> $oRequest->city_id,
                                                                            'hb_destination_id' => $aHb
                                                                        ]);
                        if ($aHbDestination->trashed()) 
                            $aHbDestination->restore();
                        else
                            $aHbDestination->save();
                    }
                }
                //AotSupplier mapped table entry
                $aAotSupplier = json_decode($oRequest->aot_location_codes);
                AOTMapSupplierCity::where('city_id', $oRequest->city_id)->delete();
                if($aAotSupplier && count($aAotSupplier)>0)
                {
                    foreach( $aAotSupplier as $aAot )
                    {
                        $oAotSupplier = AOTMapSupplierCity::withTrashed()
                                                        ->firstOrNew([ 'city_id'=> $oRequest->city_id,
                                                                        'location_id' => $aAot
                                                                    ]);
                        if ($oAotSupplier->trashed()) 
                            $oAotSupplier->restore();
                        else
                            $oAotSupplier->save();
                    }
                }
                //viator destination mapped table entry
                //there is no data for mapped with country id this insert is not in use
                $aViatorDestinations = json_decode($oRequest->viator_destination_ids);
                ViatorLocationsMapped::where('city_id', $oRequest->city_id)->delete();
                if($aViatorDestinations && count($aViatorDestinations)>0)
                {
                    foreach( $aViatorDestinations as $aVd )
                    {
                        $aViatorDestination = ViatorLocationsMapped::withTrashed()
                                                                    ->firstOrNew([ 'city_id'=> $oRequest->city_id,
                                                                                    'viator_destination_id' => $aVd
                                                                                ]);
                        if ($aViatorDestination->trashed()) 
                            $aViatorDestination->restore();
                        else
                            $aViatorDestination->save();
                    }
                }
                //Ae City mapped table entry
                $aAeCities = json_decode($oRequest->ae_region_ids);
                //print_r($aAeCities);exit;
                AECitiesMapped::where('eroam_city_id', $oRequest->city_id)->delete();
                if($aAeCities && count($aAeCities)>0)
                {
                    foreach( $aAeCities as $aAe )
                    {
                        $oAeCity = AECitiesMapped::withTrashed()
                                                ->firstOrNew([ 'eroam_city_id'=> $oRequest->city_id,
                                                                'ae_city_id' => $aAe
                                                            ]);
                        if ($oAeCity->trashed()) 
                            $oAeCity->restore();
                        else
                            $oAeCity->save();
                    }
                }
            }
                        
            if($oRequest->city_id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            return Redirect::route('common.city-list');
        }
        
        if($nIdCity != '')
        {
            $oCity = City::where('id', $nIdCity)
                        ->with([
                                'country','image',
                                'latlong', 
                                'timezone','hb_destinations_mapped',
                                'aot_supplier_locations_mapped' => function( $aot_mapped_q ){
                                    $aot_mapped_q->with(['locations' => function( $aot_location_q ){
                                            $aot_location_q->addSelect('id', 'LocationCode', 'LocationName', 'LocationType');
                                    }]);
                                },
                                'viator_locations_mapped' => function( $viator_mapped_q ){
                                    $viator_mapped_q->with(['viator_location' => function( $viator_destination_q ){
                                            $viator_destination_q->addSelect('id', 'destinationId', 'destinationName', 'destinationType');
                                    }]);
                                },
                                'ae_city_mapped' => function( $ae_mapped_q ){
                                    $ae_mapped_q->with(['ae_city' => function( $ae_city_q ){
                                            $ae_city_q->addSelect('id', 'RegionId', 'Name');
                                    }]);
                                }
                            ])
                        ->first();
            // AOT
            $selected_aot_suppier_location = $oCity->aot_supplier_locations_mapped->toArray();
            $selected_aot_suppier_locations = array_column($selected_aot_suppier_location, 'locations');
            
            $aAeCityMapSelected = $oCity->ae_city_mapped->toArray();
            $aAeCityMapSelected = array_column($aAeCityMapSelected, 'ae_city');
            $countryCode = ($oCity->country_id != '') ? $oCity->country->code : '';

                $oTimezone = Timezone::where('country_code',$countryCode)->select('name','id')->get();
                //there is no use of hb destination coz no country code addess
                //$oHBDestinations = HBDestination::getHbDestination($oCity->country->code);
                $oRegion = AECity::getAECity($countryCode)->toArray();
                $oRegion = json_encode($oRegion);
            
        }
        $oCountry = Country::orderBy('name','asc')->get();
        
        return \View::make('WebView::common.city_create',compact('oCountry','nIdCity','oTimezone','oHBDestinations','oRegion','oCity','aAeCityMapSelected','selected_aot_suppier_locations'));
    }
    
    public function callCityDelete($nIdCity)
    {
        $oCity = City::find($nIdCity);
        $oCity->delete();
    }
    
    public function callSuburbList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'suburb' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('suburb');

        session(['page_name' => 'suburb']);
        $aData = session('suburb') ? session('suburb') : array();
        $oRequest->session()->forget('suburb'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 's.name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oSuburbList = Suburb::geSuburbList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oSuburbList->currentPage(),'suburb');

        if($oRequest->page > 1)
            $oViewName =  'WebView::common._suburb_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.suburb_list' : 'WebView::common._suburb_list_ajax';
        
        return \View::make($oViewName, compact('oSuburbList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callSuburbCreate(Request $oRequest, $nIdSuburb = '')
    {
        session(['page_name' => 'suburb']);
        
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name'=> 'required',
                                    'city_id'=> 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            //$data=array();
            $data = $oRequest->except(['_token','country_id']);
            //print_r($data);exit;
            $oSuburb = Suburb::updateOrCreate(['id' => $oRequest->id],$data);
            
            if($oRequest->id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::back();
        }
        $nIdCountry='';
        if($nIdSuburb != ''){
        $oSuburb = Suburb::find($nIdSuburb);
        $oCountry = City::find($oSuburb->city_id);
        $nIdCountry = $oCountry->country_id;
        //echo "<pre>";print_r($oCountry);exit;
        }
        $countries = ['' => 'Select Country'] + Country::orderBy('name','asc')->pluck('name','id')->toArray();                                   
        return \View::make('WebView::common.suburb_create',compact('oSuburb','nIdSuburb','countries','nIdCountry'));
    }
    
    public function getAotLocations($sType) 
    {
        $oAotLocation = AOTLocation::getAotlocations($sType);
        return $oAotLocation;
    }
    
    public function callCityImageUpload(Request $oRequest) 
    {
        
        $success = FALSE;
    	$data    = array();
    	$error   = array();    
        if(Input::hasFile('city_image'))
        {

            $nCityId        = $oRequest->city_id;
            $image           = Input::file('city_image');
            $image_validator = image_validator($image);
            // check if image is valid
            if( $image_validator['fails'] )
            {
                $error = ['message' => $image_validator['message']];
            }
            else
            {
                $imageName = $nCityId.'_'.date('YmdHis').'.'.$image->getClientOriginalExtension(); 
                $filePath = 'cities/' . $imageName;
                $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');
                $imageNamePath = \Storage::disk('s3')->url($filePath);
                // Update TO DATABASE
                $cityUpdate=City::where('id',$nCityId)->update(['small_image'=>$imageName]);
                if( $cityUpdate )
                {  
                        $success = TRUE;
                        $data    = ['message' => 'Successfully added image.','nCityId'=>$nCityId,'imageNamePath'=>$imageNamePath];
                    }
                    else
                    {
                        $error   = ['message' => 'Failed to add image.'];
                    }
            }			
        }
        else
        {
                $error = ['message' => 'You have not chosen a file.'];
        }		
        return response_format($success, $data, $error);
    }
    
    public function callCityImageDelete($nIdCityImage) 
    {
       
        $data    = array();
        $success = TRUE;
        $image = City::where(['id' => $nIdCityImage])->first();
        if( $image )
        {
            $error = array();
         
            $update = City::where('id',$nIdCityImage)->update(['small_image'=>""]);
            if(!$update){
                    $success = FALSE;
                    $error = ['message' => 'An error has occured while trying to delete the image.'];
            }else
            {
                $data = ['message' => 'Successfully deleted image.'];
                
            }
        }else{
                $error = ['message' => 'An error has occured. Image not found.'];
        }
        return response_format($success, $data, $error);
        
    }
    
    public function callSetCityImagePrimary($nIdCityImage) 
    {
        $data    = array();
        $success = FALSE;
        $oCityImages = CityImage::where(['id' => $nIdCityImage])->first();
        if( $oCityImages ){

            $error = array();
            // get the current primary picture of the city and change "is_primary" to false;
            $images = CityImage::where(['city_id' => $oCityImages->city_id, 'is_primary' => TRUE])->get()->toArray();
            if( $images ){
                foreach($images as $image){
                    CityImage::where(['id' => $image['id']])->update(['is_primary' => FALSE]);
                }
            }
            $result = CityImage::where(['id' => $nIdCityImage])->update(['is_primary' => TRUE]);
            if($result){
                $success = TRUE;
                $data    = ['message' => 'Successfully updated primary picture.'];
            }
            else
                $error = ['message' => 'An error has occured. Failed to udpate primary picture.'];

        }else{
            $error = ['message' => 'An error has occured. Image not found.'];
        }
        return response_format($success, $data, $error);
        
    }
    
    public function callCouponList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'coupon' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('coupon');

        session(['page_name' => 'coupon']);
        $aData = session('coupon') ? session('coupon') : array();
        $oRequest->session()->forget('coupon'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'title');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
      
        $oCouponList = Coupon::geCouponList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,'','','coupon');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.coupon_list' : 'WebView::common._more_coupon_list';
        
        return \View::make($oViewName, compact('oCouponList','sSearchStr','sOrderField','sOrderBy','sSearchBy'));
    }
    
    public function callCouponCreate(Request $oRequest,$nIdCoupon='') 
    {
        session(['page_name' => 'coupon']);
        if($nIdCoupon != '')
            $oCoupon = Coupon::find($nIdCoupon);
        if ($oRequest->isMethod('post'))
    	{
            $aValidation = [
                                'title'=>'required',
                                'code'=>'required|unique:zcoupons,code,'.$oRequest->coupon_id,
                                'start_date'=>'required',
                                'end_date'=>'required',
                                'coupon_type'=>'required',
                            ];
            
            if($oRequest->coupon_type == 'flat'){
                $aValidation ['coupon_currency']= 'required';
                $aValidation ['amount']= 'required|numeric';
            } else{
                $aValidation ['amount']= 'required|numeric|between:0,100';
            }
            $oValidator = Validator::make($oRequest->all(), $aValidation);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aCouponData = $oRequest->all();
            unset($aCouponData['_token'],  $aCouponData['btnAddCoupon'],$aCouponData['coupon_id']);
            
            $aCouponData['is_active'] = 1;
            $aCouponData['is_deleted']= 0;
            $oCoupon = Coupon::updateOrCreate(['id' => $oRequest->coupon_id],$aCouponData);
            //$oCoupon->update($aCouponData);
            
            if($oRequest->coupon_id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::back();
        }
        $oCurrencies  = [''=>'Select Currency'] + Currency::orderBy('code','asc')->pluck('code','id')->toArray();
        return \View::make('WebView::common.coupon_create',compact('oCoupon','oCurrencies','nIdCoupon'));
    }
    
    public function callManageAction(){
        
        session(['page_name' => 'coupon']);
        $aCoupons = Input::get('coupons');
        $sAction = Input::get('action');

        switch ($sAction) {
            case 'delete':
                if ($aCoupons) {
                    foreach ($aCoupons as $coupon) {
                        $update_data = array('is_deleted' => 1, 'deleted_by' => 'Admin');
                        Coupon::where('id', $coupon)->update($update_data);
                    }
                }
                $msg = 'Coupon Deleted Successfully.';
                break;

            case 'activate':
                if ($aCoupons) {
                    foreach ($aCoupons as $coupon) {
                        $update_data = array('is_active' => 1);
                        Coupon::where('id', $coupon)->update($update_data);
                    }
                }
                $msg = 'Coupon Activated Successfully.';
                break;
            case 'deactivate':
                if ($aCoupons) {
                    foreach ($aCoupons as $coupon) {
                        $update_data = array('is_active' => 0);
                        Coupon::where('id', $coupon)->update($update_data);
                    }
                }
                $msg = 'Coupon De-activated Successfully.';
                break;
            default:
                break;
        }
        Session::flash('message', $msg);
        return 'success';
    }
    
    public function getCityCountryWise()
    {
        $country_id = Input::get('country_id');	
        return City::select('id', 'name', 'country_id')->where('country_id', $country_id)->orderBy('name')->get();
    }
    public function getCountryRegionWise()
    {
        $region_id = Input::has( 'region_id' ) ? Input::get( 'region_id' ) : null;
        return Country::where( ['region_id' => $region_id] )->orderBy('name', 'asc')->get();
    }
    
    public function callCurrencyList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'currency' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('currency');

        session(['page_name' => 'currency']);
        $aData = session('currency') ? session('currency') : array();
        $oRequest->session()->forget('currency'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
      
        $oCurrencyList = Currency::geCurrencyList($sSearchStr,$sOrderField,$sOrderBy);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,'','','currency');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::common._more_currency_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.currency_list' : 'WebView::common._currency_list_ajax';
       
        
        return \View::make($oViewName, compact('oCurrencyList','sSearchStr','sOrderField','sOrderBy'));
    }
    
    public function callCurrencyCreate(Request $oRequest,$nIdCurrency='')
    {
        session(['page_name' => 'currency']);
        if($nIdCurrency != '')
            $oCurrency = Currency::find($nIdCurrency);
        if ($oRequest->isMethod('post'))
    	{
            $aValidation = [
                            'name'=>'required|unique:zcurrencies,name,'.$oRequest->currency_id,
                            'code'=>'required|unique:zcurrencies,code,'.$oRequest->currency_id,
                            ];

            $oValidator = Validator::make($oRequest->all(), $aValidation);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aCurrencyData['name'] = $oRequest->name;
            $aCurrencyData['code'] = $oRequest->code;

            $oCoupon = Currency::updateOrCreate(['id' => $oRequest->currency_id],$aCurrencyData);

            if($oRequest->currency_id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::route('common.currency-list');
        }
        return \View::make('WebView::common.currency_create',compact('oCurrency','nIdCurrency','nIdCurrency'));
    }
    public function callCurrencyDelete($nIdCurrency)
    {
        $oCurrency = Currency::find($nIdCurrency);
        $oCurrency->delete();
    }
    public function getAllCountry(){
        return Country::select('id', 'name')->orderBy('name', 'asc')->get();
    }
    public function getAllCity(){
        return City::select('id', 'name')->orderBy('name', 'asc')->get();
    }
    public function getHbZoneCityWise(Request $oRequest){
        $zone_ids = [];
        $hb_destinations_mapped = HBDestinationsMapped::where(['city_id' => $oRequest->city_id ])->with([
			'hbDestination' => function($query){
				$query->with('hbZones');
		}])->get();

		foreach( $hb_destinations_mapped as $key => $value )
		{
			// echo json_encode($value);die;
			if( count( $value->hbDestination->hbZones ) > 0 )
			{
				foreach( $value->hbDestination->hbZones as $zone )
				{
					array_push( $zone_ids, $zone->id );
				}
			}
		}

		return HBZone::whereIn( 'id', $zone_ids )->orderBy('zone_name')->get();
       // return HBZone::select('id', 'name')->orderBy('name', 'asc')->get();
    }

    public function getDomainLicenseeWise()
    {
        $licensee_id = Input::get('licensee_id');	
        return \App\Domain::select('id', 'name')->where('licensee_id', $licensee_id)->orderBy('name')->get();
    }
	
	public function getDomainsByLicenseeId(Request $oRequest)
	{
		$domains = Domain::select('id', 'name')->where('licensee_id',$oRequest->licensee_id)->get();
		if(is_object($domains)){
			foreach($domains as $key=>$value){
				echo '<option value='.$value->id.' id='.$value->id.'>'.$value->name.'</option>';
			}
		}
	}

    public function downloadAwsImage(Request $oRequest)
    {
        
    }

}
