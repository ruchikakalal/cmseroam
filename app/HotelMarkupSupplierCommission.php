<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelMarkupSupplierCommission extends Model
{
    protected $table = 'zhotelmarkupsuppliercommissions';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
