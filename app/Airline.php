<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Airline extends Model {

	protected $table = 'zairlines';

    protected $guarded = array('id');

    public $timestamps = false;
 	
 	public static function getTransportAirlineList ($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return TransportMarkup::from('zairlines')
                ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                    $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                })
            ->orderBy($sOrderField, $sOrderBy)
            ->paginate($nShowRecord);
    }
}

?>