<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class ItenaryOrder extends Model
{
    protected $guarded = [ ];
    protected $table = 'itenaryorders';
    protected $primaryKey = 'order_id';
    
    public function Passenger()
    {
        return $this->hasMany('App\PassengerInformation', 'itenary_order_id', 'order_id');
    }
    public function Customer()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
    public function ItenaryLegs()
    {
        return $this->hasMany('App\ItenaryLeg', 'itenary_order_id', 'order_id');
    }
    public function LegDetails()
    {
        return $this->hasMany('App\LegDetail', 'itenary_order_id', 'order_id');
    }
    public static function getItenaryList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10){
        $oItenaryOrder = ItenaryOrder::from('itenaryorders as i')
                                    ->select('invoice_no','order_id','i.created_at','status','total_amount'
                                            ,'currency','voucher','first_name','last_name','from_date','to_date','is_lead')
                                    ->leftJoin('passengerinformations as p', function($query){
                                        $query->on('p.itenary_order_id', '=', 'i.order_id')
                                              ->where('is_lead','yes'); 
                                        })        
                                    ->when($sSearchStr, function($query) use($sSearchStr) {
                                                $query->where('invoice_no','LIKE', '%'.$sSearchStr.'%');
                                            })
                                    ->orderBy($sOrderField, $sOrderBy)
                                    ->groupBy('order_id')
                                    ->paginate($nShowRecord);
        return $oItenaryOrder;
    }
	
	public static function getItenaryListByStatus($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10,$status,$agent_id,$type,$domain_id){
		
		$firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00.000000';
		$endDayOfyear = Carbon::now()->endOfYear()->format('Y-m-d').' 23:59:59.000000';
		$oItenaryOrder = ItenaryOrder::from('itenaryorders as i')
                                    ->select('invoice_no','i.user_id','order_id','i.created_at','agent_id','status','total_amount'
                                            ,'currency','voucher','first_name','last_name','from_date','to_date','is_lead','u.domain_id','u.id')
                                    ->leftJoin('passengerinformations as p', function($query){
                                        $query->on('p.itenary_order_id', '=', 'i.order_id')
                                              ->where('is_lead','yes'); 
                                        })
									->leftjoin('users as u','u.id','=','agent_id')
									->when($sSearchStr, function($query) use($sSearchStr) {
                                                $query->where('invoice_no','LIKE', '%'.$sSearchStr.'%');
                                            })
									->where('status',$status)
									->whereBetween('i.created_at', [$firstDayOfMonth, $endDayOfyear])
									->when($type, function($query) use($type,$agent_id,$domain_id) {
												if($type == 'personal'){
													$query->where('u.domain_id',$domain_id);
													$query->where('agent_id',$agent_id);
												}else{
													$query->where('u.domain_id',$domain_id);
												}
                                            })
									->orderBy($sOrderField, $sOrderBy)
                                    ->groupBy('order_id')
                                    ->paginate($nShowRecord);
		return $oItenaryOrder;
    }

    public static function getPendingBooking($userIds,$sOrderField,$sOrderBy,$status){
       $oItenaryOrder =  ItenaryOrder::leftjoin('users','users.id','=','itenaryorders.user_id')
                                            ->select('itenaryorders.*',
                                                    'users.name as username',
                                                    DB::raw('(SELECT COUNT(passenger_information_id) FROM passengerinformations WHERE itenary_order_id =itenaryorders.order_id) as pax_count')
                                                    )
                                            ->whereIn('itenaryorders.user_id',$userIds)
                                            ->where('itenaryorders.status',$status)
                                            ->orderBy($sOrderField, $sOrderBy)
                                            ->limit(5)
                                            ->get();
        return $oItenaryOrder;
    }
	
	public static function activeBooking($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10,$agent_id,$type,$domain_id){
		
		$firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00';
		$endDayOfyear = Carbon::now()->endOfYear()->format('Y-m-d').' 23:59:59';
		$now = Carbon::now()->format('Y-m-d').' 00:00:00';
		
		$oItenaryOrder = ItenaryOrder::from('itenaryorders as i')
                                    ->select('invoice_no','i.user_id','order_id','i.created_at','agent_id','status','total_amount'
                                            ,'currency','voucher','first_name','last_name','from_date','to_date','is_lead','u.domain_id','u.id')
                                    ->leftJoin('passengerinformations as p', function($query){
                                        $query->on('p.itenary_order_id', '=', 'i.order_id')
                                              ->where('is_lead','yes'); 
                                        })
									->leftjoin('users as u','u.id','=','agent_id')
									->when($sSearchStr, function($query) use($sSearchStr) {
                                                $query->where('invoice_no','LIKE', '%'.$sSearchStr.'%');
                                            })
									->where('i.created_at', '>=', $now)
									->when($type, function($query) use($type,$agent_id,$domain_id) {
												if($type == 'personal'){
													$query->where('u.domain_id',$domain_id);
													$query->where('agent_id',$agent_id);
												}else{
													$query->where('u.domain_id',$domain_id);
												}
                                            })
									->orderBy($sOrderField, $sOrderBy)
                                    ->groupBy('order_id')
                                    ->paginate($nShowRecord);
		return $oItenaryOrder;
    }
	
	public static function archivedBooking($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10,$agent_id,$type,$domain_id){
		
		$firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00';
		$endDayOfyear = Carbon::now()->endOfYear()->format('Y-m-d').' 23:59:59';
		$now = Carbon::now()->format('Y-m-d');
		
		$oItenaryOrder = ItenaryOrder::from('itenaryorders as i')
                                    ->select('invoice_no','i.user_id','order_id','i.created_at','agent_id','status','total_amount'
                                            ,'currency','voucher','first_name','last_name','from_date','to_date','is_lead','u.domain_id','u.id')
                                    ->leftJoin('passengerinformations as p', function($query){
                                        $query->on('p.itenary_order_id', '=', 'i.order_id')
                                              ->where('is_lead','yes'); 
                                        })
									->leftjoin('users as u','u.id','=','agent_id')
									->when($sSearchStr, function($query) use($sSearchStr) {
                                                $query->where('invoice_no','LIKE', '%'.$sSearchStr.'%');
                                            })
									->where('i.to_date', '<=', $now)
									->when($type, function($query) use($type,$agent_id,$domain_id) {
												if($type == 'personal'){
													$query->where('u.domain_id',$domain_id);
													$query->where('agent_id',$agent_id);
												}else{
													$query->where('u.domain_id',$domain_id);
												}
                                            })
									->orderBy($sOrderField, $sOrderBy)
                                    ->groupBy('order_id')
                                    ->paginate($nShowRecord);
		return $oItenaryOrder;
    }
	
	
	
	
	
}
