<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRequestsAdditionalPerson extends Model
{
    protected $guarded = [];
    protected $table = 'tblbookingrequestsadditionalpersons';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
