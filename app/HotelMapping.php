<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HotelMapping extends Model
{
    protected $guarded = [ ];
    protected $table = 'enhotelmapping';
    protected $primaryKey = 'hm_id';  
    public $timestamps = false;  
}
