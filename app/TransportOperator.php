<?php
namespace App;

use Eloquent;

class TransportOperator extends Eloquent {

	protected $table = 'ztransportoperators';

    protected $guarded = [];
    protected $primaryKey = 'id';

    public static function getTransportOperatorList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return Transport::from('ztransportoperators as t')
                        ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                                    $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                                })
                        ->select(
                            't.*'
                            )
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);  
    }

}

?>