<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backend extends Model
{
    protected $table = 'backend';

    public function config() {
    	return $this->belongsTo('App\BackendConfig','config_id');
    }
}
