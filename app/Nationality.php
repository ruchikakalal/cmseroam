<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Nationality extends Model
{

	protected $table = 'znationalities';

    protected $guarded = array('id');

    use SoftDeletes;

    protected $dates = ['deleted_at'];     

    public function country(){
    	return $this->hasOne('Country','id','country_id');
    }

}
 
?>