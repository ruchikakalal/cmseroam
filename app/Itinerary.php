<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Itinerary extends Model
{
     protected $fillable = [
                            'is_booked', 'customer_id','agent_id','reference_no','title','to_city_id','from_city_id','num_of_travellers',
                            'travel_date', 'is_deleted','total_itinerary_legs','total_amount','currency','total_days','total_per_person','total_refundable',
                            'is_saved_itinerary', 'ae_api_response','search_session'
                        ];
    protected $table = 'zitineraries';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function agent()
    {
        return $this->hasOne('App\Agent', 'id', 'agent_id');   
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');   
    }    

	public function itinerary_legs()
    {
        return $this->hasMany('App\ItineraryLeg', 'itinerary_id', 'id');
    }

	public function itinerary_pax_information()
    {
        return $this->hasMany('App\ItineraryPaxInformation');
    }

    public function lead_pax()
    {
        return $this->hasOne('App\ItineraryPaxInformation')->where('lead_person','yes');
    }

	public function itinerary_fees_and_charges()
    {
        return $this->hasOne('App\ItineraryFeesAndCharges');
    }

    public function itinerary_legs_data()
    {
        return $this->hasMany('App\ItineraryLeg')
            ->with(['itinerary_leg_activities',
                    'itinerary_leg_hotel',
                    'itinerary_leg_hotelbeds',
                    'itinerary_leg_aothotels',
                    'itinerary_leg_transport',
                    'itinerary_leg_aotactivity',
                    'itinerary_leg_aehotels',
                    'itinerary_leg_aeactivity',
                    'from_city' => function($query)
                    {
                        $query->with('image');
                    },
                    'to_city' => function($query)
                    {
                        $query->with('image');
                    }
                ]);
    }

    public function from_city(){
        return $this->hasOne('App\city','id','from_city_id');
    }

    public function to_city(){
        return $this->hasOne('App\city','id','to_city_id');
    }


    public function scopefirstName($query,$pax){
        return $query->whereHas('lead_pax',function($q) use($pax){
            $q->where("first_name", 'LIKE', '%'.$pax.'%');
        });
    }

    public function scopelastName($query,$pax){
        return $query->whereHas('lead_pax',function($q) use($pax){
            $q->where("last_name", 'LIKE', '%'.$pax.'%');
        });
    }
}
