<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class RoutePlan extends Model
{
     protected $fillable = [
        'name','from_city_id','to_city_id'
    ];
    protected $table = 'zrouteplans';
    protected $primaryKey = 'id';
   
    public function routes() 
    {
        return $this->hasMany('App\Route', 'route_plan_id', 'id')->with('route_legs')->orderBy('is_default', 'desc');
    }
    public function fromcity() 
    {
        return $this->hasOne('App\City','id','from_city_id')->with('country');
    }
    public function tocity() 
    {
        return $this->hasOne('App\City','id','to_city_id')->with('country');
    }
    
    public static function getRouteList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord,$nStart,$nFinish) 
    {
        $oRouteList = RoutePlan::from('zrouteplans as r')
                                ->leftJoin('zcities as cf', 'cf.id', '=', 'r.from_city_id')
                                ->leftJoin('zcities as ct', 'ct.id', '=', 'r.to_city_id')
                                ->leftJoin('zcountries as cot', 'cot.id', '=', 'ct.country_id')
                                ->leftJoin('zcountries as cof', 'cof.id', '=', 'cf.country_id')
                                ->leftJoin('zroutes as routes', 'routes.route_plan_id', '=', 'r.id')
                                ->when($nFinish, function($query) use($nFinish) {
                                        $query->where('to_city_id', $nFinish);
                                    })
                                ->when($nStart, function($query) use($nStart) {
                                        $query->where('from_city_id',$nStart);
                                    })
                                ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                                        $query->where('r.'.$sSearchBy,'like','%'.$sSearchStr.'%');
                                    })
                                ->select(
                                    'r.id as id',
                                    'r.name as name',
                                    'cf.name as from_city',
                                    'ct.name as to_city',
                                    'cot.name as to_country',
                                    'cof.name as from_country',
                                    DB::raw('COUNT(routes.route_plan_id) AS route_count')
                                    )
                                ->groupBy('routes.route_plan_id')
                               ->orderBy($sOrderField, $sOrderBy)->paginate($nShowRecord);
                               
                                
        return $oRouteList;
    }
}
