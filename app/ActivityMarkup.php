<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityMarkup extends Model
{
    protected $table = 'zactivitymarkups';
    protected $primaryKey = 'id';
    protected $fillable = ['name','description','allocation_type','allocation_id','is_default','is_active','activity_markup_percentage_id'
                            ,'activity_markup_agent_commission_id','activity_markup_supplier_commission_id'];
    
    public function markup_percentage(){
        return $this->hasOne('App\ActivityMarkupPercentage','activity_markup_id','id');
    }       

    public function agent_commission(){
        return $this->hasOne('App\ActivityMarkupAgentCommission','activity_markup_id','id');
    }
    
    public function supplier_commission(){
        return $this->hasOne('App\ActivityMarkupSupplierCommission','activity_markup_id','id');
    }
    
    public static function getActivityMarkupList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord) 
    {
        $oQuery = ActivityMarkup::from('zactivitymarkups as am')
                                ->leftjoin('zactivitymarkuppercentages as amp','amp.activity_markup_id','=','am.id')
                                ->leftjoin('zactivitymarkupagentcommissions as ama','ama.activity_markup_id','=','am.id')
                                ->select('am.id as id','am.name as name','amp.percentage as percentage','am.allocation_type as allocation_type',
                                          'amp.percentage as mark_percentage','ama.percentage as agent_percentage','allocation_id as allocation_id')
                                ->orderBy($sOrderField, $sOrderBy);
        if($sSearchStr != ''){                    
            if($sSearchBy == 'name')
                $oQuery->where('name', 'like', '%' . $sSearchStr . '%');
            elseif($sSearchBy == 'activity')
            {
                $aIdList    = Activity::where( 'name', 'like', '%' . $sSearchStr . '%' )->pluck('id');
                $oQuery->whereIn('allocation_id', $aIdList)
                        ->orWhere(['allocation_type' => 'activity']);
            }
            elseif($sSearchBy == 'city')
            {
                $aIdList    = City::where( 'name', 'like', '%' . $sSearchStr . '%' )->pluck('id');
                $oQuery->whereIn('allocation_id', $aIdList)
                        ->orWhere(['allocation_type' => 'city']);
            }
            elseif($sSearchBy == 'country')
            {
                $aIdList    = Country::where( 'name', 'like', '%' . $sSearchStr . '%' )->pluck('id');
                $oQuery->whereIn('allocation_id', $aIdList)
                        ->orWhere(['allocation_type' => 'country']);
            }
        }
                
        $oQuery->groupBy('am.id');
        return $oQuery->get();
    }
}
