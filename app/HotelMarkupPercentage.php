<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelMarkupPercentage extends Model
{
    protected $table = 'zhotelmarkuppercentages';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
