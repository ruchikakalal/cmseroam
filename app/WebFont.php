<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebFont extends Model
{
    protected $table = "webfonts";
}
