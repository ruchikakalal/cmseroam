<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class SavedTrips extends Model
{
    protected $table = 'saved_trips';
	
	
	public static function saveItenaryAgentWise($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10,$agent_id,$type,$domain_id)
	{
		
	$firstDayOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d').' 00:00:00.000000';
	$endDayOfyear = Carbon::now()->endOfYear()->format('Y-m-d').' 23:59:59.000000';
	$now = Carbon::now()->format('Y-m-d').' 00:00:00';
	
	$saveTrip = SavedTrips::from('saved_trips as i')
                                    ->select('i.*',
                                                    'u.name as username')
                                   ->leftjoin('users as u','u.id','=','i.user_id')
									->when($sSearchStr, function($query) use($sSearchStr) {
                                                $query->where('id','LIKE', '%'.$sSearchStr.'%');
                                            })
									
									->where('i.created_at', '>=', $now)
									->when($type, function($query) use($type,$agent_id,$domain_id) {
												if($type == 'personal'){
													$query->where('u.domain_id',$domain_id);
													$query->where('i.user_id',$agent_id);
												}else{
													$query->where('u.domain_id',$domain_id);
												}
                                            })
									->orderBy($sOrderField, $sOrderBy)
                                    ->groupBy('i.id')
                                    ->paginate($nShowRecord);
		return $saveTrip;
	}
	
}
