-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2018 at 01:44 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eroamv1`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_ip_details`
--

CREATE TABLE `user_ip_details` (
  `id` int(11) NOT NULL,
  `search_type` varchar(20) NOT NULL,
  `ip_address` longtext NOT NULL,
  `city` varchar(60) NOT NULL,
  `country_code` varchar(20) NOT NULL,
  `country_name` varchar(60) NOT NULL,
  `interestlist` text NOT NULL,
  `nationality` int(11) NOT NULL,
  `latitude` varchar(60) NOT NULL,
  `longitude` varchar(60) NOT NULL,
  `region` varchar(100) NOT NULL,
  `accommodation` int(11) NOT NULL,
  `room` varchar(11) NOT NULL,
  `transport` int(11) NOT NULL,
  `age_group` varchar(40) NOT NULL,
  `gender` varchar(40) NOT NULL,
  `interest_list_id` varchar(10) NOT NULL,
  `tour_type` varchar(60) DEFAULT NULL,
  `project` varchar(100) DEFAULT NULL,
  `type_option` varchar(20) DEFAULT NULL,
  `package_option` varchar(20) DEFAULT NULL,
  `start_location` varchar(100) DEFAULT NULL,
  `end_location` varchar(100) DEFAULT NULL,
  `start_date` varchar(60) DEFAULT NULL,
  `rooms` int(11) DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `departure_date` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_ip_details`
--

INSERT INTO `user_ip_details` (`id`, `search_type`, `ip_address`, `city`, `country_code`, `country_name`, `interestlist`, `nationality`, `latitude`, `longitude`, `region`, `accommodation`, `room`, `transport`, `age_group`, `gender`, `interest_list_id`, `tour_type`, `project`, `type_option`, `package_option`, `start_location`, `end_location`, `start_date`, `rooms`, `adults`, `children`, `departure_date`, `created_at`, `updated_at`) VALUES
(1, 'tours', '192.168.1.217', '', 'IN', 'India', 'City tour', 10, '28.667', '77.216', 'National Capital', 5, 'NaN', 1, 'Under 18 years old', 'Female', '9,14', 'single,multi', 'Melbourne', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-20 06:46:57', '2018-06-20 06:46:57'),
(2, 'tours', '111.93.58.10', '', 'IN', 'India', 'City Tour, Walking', 10, '28.6667', '77.2167', 'National Capital Territory of Delhi', 5, 'NaN', 1, 'Under 18 years old', 'female', '9,14', 'single,multi', 'Melbourne', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-20 06:47:08', '2018-06-20 06:47:08'),
(3, 'itenerary', '111.93.58.10', 'Delhi', 'IN', 'India', 'City Tour, Walking', 10, '28.6667', '77.2167', 'National Capital Territory of Delhi', 5, 'NaN', 1, 'Under 18 years old', 'female', '9,14', NULL, NULL, 'manual', 'search-box5', NULL, 'Melbourne, Australia', '2018-06-21', 1, 1, 0, NULL, '2018-06-20 06:47:48', '2018-06-20 06:47:48'),
(4, 'tours', '192.168.1.217', 'Delhi', 'IN', 'India', 'City tour', 10, '28.667', '77.216', 'National Capital', 5, 'NaN', 1, 'Under 18 years old', 'Female', '9,14', 'single,multi', 'Melbourne', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-20 06:47:50', '2018-06-20 06:47:50'),
(5, 'itenerary', '111.93.58.10', 'Delhi', 'IN', 'India', 'City Tour, Walking', 10, '28.6667', '77.2167', 'National Capital Territory of Delhi', 5, 'NaN', 1, 'Under 18 years old', 'female', '9,14', NULL, NULL, 'auto', 'search-box5', 'Melbourne, Australia', 'Perth, Australia', '2018-06-21', 1, 1, 0, NULL, '2018-06-20 06:49:10', '2018-06-20 06:49:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_ip_details`
--
ALTER TABLE `user_ip_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_ip_details`
--
ALTER TABLE `user_ip_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
