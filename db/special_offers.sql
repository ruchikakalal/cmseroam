-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2018 at 11:03 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eroam`
--

-- --------------------------------------------------------

--
-- Table structure for table `special_offers`
--

CREATE TABLE `special_offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inventory_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selling_start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selling_end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `travel_start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `travel_end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_price` double(8,2) DEFAULT NULL,
  `children_discount` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_offer` double(8,2) DEFAULT NULL,
  `discount_price` double(8,2) DEFAULT NULL,
  `children_discount_offer` double(8,2) DEFAULT NULL,
  `children_discount_price` double(8,2) DEFAULT NULL,
  `offer_description` text COLLATE utf8mb4_unicode_ci,
  `term_option` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_services` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms_n_conditions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost_per_night` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_nights` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_nights` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Pending','Active','Expired','Deleted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `special_offers`
--

INSERT INTO `special_offers` (`id`, `offer_name`, `offer_type`, `inventory_type`, `product_name`, `client`, `selling_start_date`, `selling_end_date`, `travel_start_date`, `travel_end_date`, `original_price`, `children_discount`, `discount_offer`, `discount_price`, `children_discount_offer`, `children_discount_price`, `offer_description`, `term_option`, `additional_services`, `terms_n_conditions`, `cost_per_night`, `original_nights`, `free_nights`, `fee`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ABC', 'value_based_dollar', 'Accommodation', '2', 'localhost,test 123', '04/06/2018', '26/06/2018', '01/07/2018', '26/07/2018', NULL, 'No', 20.00, 180.00, 0.00, 0.00, 'dsfds dsfsd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Deleted', '2018-06-14 00:20:30', '2018-06-15 03:31:23'),
(2, 'xyz', 'value_based_dollar', 'Accommodation', '4', '', '05/06/2018', '26/06/2018', '01/07/2018', '18/07/2018', 150.00, 'Yes', 20.00, 130.00, NULL, 130.00, 'desc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', '2018-06-14 00:33:20', '2018-06-15 02:00:41'),
(3, 'PQR', 'term_based', 'Activity', '2', 'All,localhost', '06/06/2018', '26/06/2018', '01/07/2018', '26/07/2018', NULL, 'No', NULL, NULL, NULL, NULL, NULL, 'Value Add', 'asdsa fdsfsd', 'asds fds', NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:35:08', '2018-06-15 01:23:29'),
(4, 'dsfds', 'value_based_dollar', 'Activity', '2', 'priya', '03/06/2018', '26/06/2018', '30/06/2018', '18/07/2018', 220.00, 'No', 20.00, 200.00, NULL, NULL, 'safds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:39:39', '2018-06-14 00:39:39'),
(5, 'dd', 'value_based_per', 'Activity', '3', 'Air Asia,irfan,priya', '03/06/2018', '19/06/2018', '01/07/2018', '31/07/2018', 412.88, 'No', 10.00, 41.20, NULL, NULL, 'descr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 01:42:59', '2018-06-14 01:42:59'),
(6, 'ABC', 'value_based_dollar', 'Accommodation', '2', '', '04/06/2018', '26/06/2018', '01/07/2018', '26/07/2018', 200.00, 'No', 20.00, 180.00, 0.00, 0.00, 'dsfds', '', '', '', '', '', '', '', 'Pending', '2018-06-14 00:20:30', '2018-06-14 00:20:30'),
(7, 'xyz', 'value_based_dollar', 'Accommodation', '4', '', '05/06/2018', '26/06/2018', '01/07/2018', '18/07/2018', 150.00, 'Yes', 20.00, 130.00, NULL, 130.00, 'desc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:33:20', '2018-06-14 00:33:20'),
(8, 'PQR', 'term_based', 'Activity', '2', 'localhost', '06/06/2018', '26/06/2018', '01/07/2018', '26/07/2018', 220.00, 'No', NULL, NULL, NULL, NULL, NULL, 'Value Add', 'asdsa', 'asds', NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:35:08', '2018-06-14 00:35:08'),
(9, 'dsfds', 'value_based_dollar', 'Activity', '2', 'priya', '03/06/2018', '26/06/2018', '30/06/2018', '18/07/2018', 220.00, 'No', 20.00, 200.00, NULL, NULL, 'safds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:39:39', '2018-06-14 00:39:39'),
(10, 'dd', 'value_based_per', 'Activity', '3', 'Air Asia,irfan,priya', '03/06/2018', '19/06/2018', '01/07/2018', '31/07/2018', 412.88, 'No', 10.00, 41.20, NULL, NULL, 'descr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 01:42:59', '2018-06-14 01:42:59'),
(11, 'ABC', 'value_based_dollar', 'Accommodation', '2', '', '04/06/2018', '26/06/2018', '01/07/2018', '26/07/2018', 200.00, 'No', 20.00, 180.00, 0.00, 0.00, 'dsfds', '', '', '', '', '', '', '', 'Pending', '2018-06-14 00:20:30', '2018-06-14 00:20:30'),
(12, 'xyz', 'value_based_dollar', 'Accommodation', '4', '', '05/06/2018', '26/06/2018', '01/07/2018', '18/07/2018', 150.00, 'Yes', 20.00, 130.00, NULL, 130.00, 'desc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:33:20', '2018-06-14 00:33:20'),
(13, 'PQR', 'term_based', 'Activity', '2', 'localhost', '06/06/2018', '26/06/2018', '01/07/2018', '26/07/2018', 220.00, 'No', NULL, NULL, NULL, NULL, NULL, 'Value Add', 'asdsa', 'asds', NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:35:08', '2018-06-14 00:35:08'),
(14, 'dsfds', 'value_based_dollar', 'Activity', '2', 'priya', '03/06/2018', '26/06/2018', '30/06/2018', '18/07/2018', 220.00, 'No', 20.00, 200.00, NULL, NULL, 'safds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 00:39:39', '2018-06-14 00:39:39'),
(15, 'dd', 'value_based_per', 'Activity', '3', 'Air Asia,irfan,priya', '03/06/2018', '19/06/2018', '01/07/2018', '31/07/2018', 412.88, 'No', 10.00, 41.20, NULL, NULL, 'descr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2018-06-14 01:42:59', '2018-06-14 01:42:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `special_offers`
--
ALTER TABLE `special_offers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `special_offers`
--
ALTER TABLE `special_offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
