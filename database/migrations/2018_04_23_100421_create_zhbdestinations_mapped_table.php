<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZHBDestinationsMappedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhbdestinationsmapped'))
        {
        Schema::create('zhbdestinationsmapped', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->bigInteger('hb_destination_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('city_id')->references('id')->on('zcities');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhbdestinationsmapped');
    }
}
