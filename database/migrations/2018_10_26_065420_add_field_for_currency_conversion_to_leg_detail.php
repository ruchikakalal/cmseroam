<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldForCurrencyConversionToLegDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legdetails', function($table) {
            $table->string('customer_currency')->default('AUD')->after('currency');
            $table->decimal('conversion_rate',8,2)->default('1')->after('customer_currency')->comment('conversion rate = customer_currency_rate/supplier_currency_rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('legdetails', function($table) {
            $table->dropColumn('customer_currency');
            $table->dropColumn('conversion_rate');
        });
    }
}
