<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelimages'))
        {
        Schema::create('zhotelimages', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('hotel_id')->nullable();
			$table->text('path')->nullable();
            $table->timestamps();
			$table->tinyInteger('is_primary')->default(0);
			$table->text('small')->nullable();
			$table->text('medium')->nullable();
			$table->text('large')->nullable();
			$table->text('original')->nullable();
			$table->text('thumbnail')->nullable();
			$table->integer('random_hotel_id')->nullable();
			$table->integer('sort_order')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelimages');
    }
}
