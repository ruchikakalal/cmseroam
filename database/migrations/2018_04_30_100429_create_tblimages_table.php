<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tblimages'))
        {
        Schema::create('tblimages', function (Blueprint $table) {
            $table->increments('image_id');
            $table->text('image_name')->nullable();
            $table->text('image_thumb')->nullable();
            $table->integer('tour_id')->nullable();
            $table->text('image_url')->nullable();
            $table->text('title')->nullable();
            $table->integer('user_id')->nullable();
            $table->tinyInteger('is_deleted')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->tinyInteger('is_canceled')->nullable();
            $table->string('random_tour_id')->nullable();
            $table->text('image_small')->nullable();
            $table->text('image_medium')->nullable();
            $table->text('image_large')->nullable();
            $table->integer('sort_order')->nullable();
            $table->integer('is_primary')->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblimages');
    }
}
