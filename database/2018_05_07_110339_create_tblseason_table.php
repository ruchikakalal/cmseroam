<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblseasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblseason', function (Blueprint $table) {
            $table->increments('season_id');
            $table->integer('tour_id');
            $table->dateTime('StartDate');
            $table->dateTime('EndDate');
            $table->decimal('AdultPrice',16,4)->nullable();
            $table->decimal('ChildPrice',16,4)->nullable();
            $table->integer('Availability')->nullable();
            $table->integer('adv_purchase')->nullable();
            $table->integer('is_mon')->nullable();
            $table->integer('is_tue')->nullable();
            $table->integer('is_wed')->nullable();
            $table->integer('is_thu')->nullable();
            $table->integer('is_fri')->nullable();
            $table->integer('is_sat')->nullable();
            $table->integer('is_sun')->nullable();
            $table->string('SeasonName')->nullable();
            $table->decimal('AdultPriceSingle',16,4)->nullable();
            $table->decimal('ChildPriceSingle',16,4)->nullable();
            $table->decimal('AdultSupplement',16,4)->nullable();
            $table->decimal('ChildSupplement',16,4)->nullable();
            $table->integer('flight_currency_id')->nullable();
            $table->decimal('flightPrice',16,4)->nullable();
            $table->text('flightDescription')->nullable();
            $table->text('flightReturn')->nullable();
            $table->text('flightDepart')->nullable();
            $table->decimal('sumPrice',16,4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblseason');
    }
}
