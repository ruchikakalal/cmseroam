<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblbookingrequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbookingrequests', function (Blueprint $table) {
            $table->increments('request_id');
            $table->integer('tour_id')->nullable();
            $table->string('tour_title')->nullable();
            $table->string('Title')->nullable();
            $table->string('FName')->nullable();
            $table->string('SurName')->nullable();
            $table->string('gender')->nullable();
            $table->dateTime('DOB')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('nationality')->nullable();
            $table->dateTime('departure_date')->nullable();
            $table->dateTime('return_date')->nullable();
            $table->integer('provider')->nullable();
            $table->dateTime('request_date')->nullable();
            $table->integer('status')->nullable();
            $table->decimal('singletotal',10,4)->nullable();
            $table->decimal('twintotal',10,4)->nullable();
            $table->decimal('price',10,4)->nullable();
            $table->integer('Agent_id')->nullable();
            $table->string('code')->nullable();
            $table->string('contact')->nullable();
            $table->longText('RequestTextXML')->nullable();
            $table->longText('RequestTextReisebazaar')->nullable();
            $table->string('BookingType')->nullable();
            $table->tinyInteger('is_specialOffer')->nullable();
            $table->integer('payment_method')->nullable();
            $table->string('credit_card_type')->nullable();
            $table->string('credit_card_no')->nullable();
            $table->string('CVV_no')->nullable();
            $table->string('card_holder_name')->nullable();
            $table->string('card_holder_address')->nullable();
            $table->date('card_validity')->nullable();
            $table->tinyInteger('is_ch_travling')->nullable();
            $table->string('eway_result')->nullable();
            $table->string('eway_authcode')->nullable();
            $table->string('eway_error')->nullable();
            $table->string('eway_transaction_no')->nullable();
            $table->string('eway_txn_number')->nullable();
            $table->string('eway_option1')->nullable();
            $table->string('eway_option2')->nullable();
            $table->string('eway_option3')->nullable();
            $table->string('passport_pic')->nullable();
            $table->string('credircard_pic1')->nullable();
            $table->string('credircard_pic2')->nullable();
            $table->string('sub_total')->nullable();
            $table->string('addon_total_price')->nullable();
            $table->string('credit_card_fee')->nullable();
            $table->string('grand_total')->nullable();
            $table->string('booking_currency')->nullable();
            $table->tinyInteger('voucher_sent')->nullable();
            $table->integer('domain_id')->nullable();
            $table->string('voucherNo')->nullable();
            $table->string('refferenceNo')->nullable();
            $table->longText('joiningInstruction')->nullable();
            $table->longText('notes')->nullable();
            $table->string('postal_code')->nullable();
            $table->longText('supplier_notes')->nullable();
            $table->longText('tour_notes')->nullable();
            $table->longText('custom_notes')->nullable();
            $table->dateTime('commission_date')->nullable();
            $table->tinyInteger('is_commission_paid')->nullable();
            $table->tinyInteger('supplier_paid')->nullable();
            $table->string('manual_booking_id')->nullable();
            $table->string('domain')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbookingrequests');
    }
}
